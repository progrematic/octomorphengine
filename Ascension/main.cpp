#include <Octomorph/Managers/ViewManager.h>
#include <Octomorph/Managers/EventManager.h>

#include <Managers/ViewModelManager.h>
#include <ViewModels/MainViewModel.h>

#include <Octomorph/Input/Mouse.h>
#include <Octomorph/Input/Keyboard.h>

using namespace Octomorph::Managers;
using namespace Octomorph::Input;

int main()
{
	int screenWidth = 800;
	int screenHeight = 600;
	ViewManager::GetInstance()->Initialize((char*)"Ascension", screenWidth, screenHeight, 32, sf::Style::Titlebar);
	sf::RenderWindow& mWindow = ViewManager::GetInstance()->GetWindow();

	sf::Clock dtClock;
	float curTime = dtClock.restart().asSeconds();
	float dt = 0.f;

	std::shared_ptr<ViewModel> mainVM = std::make_shared<MainViewModel>();
	ViewModelManager::GetInstance()->PushViewModel(mainVM);

	while (mWindow.isOpen())
	{
		Mouse::Update();
		Keyboard::Update();
		EventManager::GetInstance()->Update();

		sf::Event e;
		while (mWindow.pollEvent(e))
		{
			if (e.type == sf::Event::EventType::Closed)
			{
				mWindow.close();
			}

			if (e.type == sf::Event::MouseMoved)
			{
				Mouse::MousePosCallback(sf::Mouse::getPosition(ViewManager::GetInstance()->GetWindow()).x, sf::Mouse::getPosition(ViewManager::GetInstance()->GetWindow()).y);
			}

			if (e.type == sf::Event::MouseButtonPressed || e.type == sf::Event::MouseButtonReleased)
			{
				Mouse::MouseButtonCallback(e.mouseButton.button);
			}

			if (e.type == sf::Event::KeyPressed || e.type == sf::Event::KeyReleased)
			{
				Keyboard::KeyCallback(e.key.code);
			}

			EventManager::GetInstance()->HandleEvent(e);
		}

		float newTime = dtClock.getElapsedTime().asSeconds();
		float dt = std::max(0.f, newTime - curTime);
		curTime = newTime;

		/////////////////////////////////////////////////// UPDATE
		ViewModelManager::GetInstance()->Update(dt);
		ViewManager::GetInstance()->Update(dt);

		/////////////////////////////////////////////////// DRAW
		mWindow.clear(sf::Color(130, 130, 255));
		ViewModelManager::GetInstance()->Draw(dt);
		mWindow.display();
	}

	return 0;
}