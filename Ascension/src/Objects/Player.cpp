#include <Objects/Player.h>
#include <Globals.h>

#include <Octomorph/Components/InputComponent.h>

using namespace Octomorph::Components;

Player::Player() : Mob()
{
	std::shared_ptr<InputComponent> input = GetComponent<InputComponent>();

	input->SetInputType(Octomorph::Enums::INPUT_TYPE::KEYBOARD);
	input->SetKeyboardDigitalInput(ETOI(InputDigital::MoveLeft), sf::Keyboard::A);
	input->SetKeyboardDigitalInput(ETOI(InputDigital::MoveRight), sf::Keyboard::D);
	input->SetKeyboardDigitalInput(ETOI(InputDigital::Jump), sf::Keyboard::Space);
	input->SetKeyboardDigitalInput(ETOI(InputDigital::Crouch), sf::Keyboard::S);

	mSize.x = 16;
	mSize.y = 32;
	mShape.setSize(mSize);
}

void Player::Update(float dt)
{
	Mob::Update(dt);
}

void Player::Draw(float dt)
{
	Mob::Draw(dt);
}
