#include <Objects/Mob.h>

#include <Globals.h>

#include <Octomorph/Globals.h>
#include <Octomorph/Managers/ViewManager.h>
#include <Octomorph/Input/Keyboard.h>
#include <Octomorph/Components/InputComponent.h>
#include <Octomorph/Components/StaticTransformComponent.h>

using namespace Octomorph::Components;

Mob::Mob() : Object()
{
	AttachComponent<StaticTransformComponent>();
	AttachComponent<InputComponent>();

	for (int i = ETOI(MobStat::INVALID) + 1; i < ETOI(MobStat::COUNT); i++)
	{
		mStatsList.insert(std::pair<MobStat, float>(ITOE(MobStat, i), 300.0f));
	}
	mStatsList.at(MobStat::PICKUP_RADIUS) = 2;

	mSize.x = Tile::TileSize;
	mSize.y = Tile::TileSize;

	mShape.setFillColor(sf::Color::Red);
	mShape.setSize(mSize);

	mStatsList[MobStat::MOV_SPEED] = 400;
	mGravity = 500;
	mStatsList[MobStat::MOV_AIR_SPEED] = mGravity;	// This ensures we can't wallcling->jump up a single wall
	mInitialJumpMod = 1.5f;				// Gravity multiplier when jumping
	mJumpMod = 1;						// Current gravity multiplier
	mJumpDeltaMod = 5;					// dt multiplier when adding to mJumpMod - bigger number means reach jump peak faster
	mJumpDescentMod = 3;				// acceleration multiplier when peak of jump has been reached - adds feeling of weight
	mWallClingSlideMod = 0.25f;			// Gravity multiplier when sliding off of a wall cling
	mWallClingHopMod = 0.5f;			// Gravity multiplier when hopping off of a wall cling
	mWallClingJumpInputLock = 0.3f;		// Seconds of locked movement input after a wallcling -> jump
	mWallClingTime = 0.5f;				// Seconds you can wall cling before starting to slide off
	SetJumpState(JumpState::Grounded);
}

void Mob::Initialize(std::shared_ptr<Map> map)
{
	mMap = map;
}

void Mob::Update(float dt)
{
	// Handle Movement
	HandleMovement(dt);

	// Update jump variables
	mTimeInJumpState += dt;

	float jumpDelta = dt * mJumpDeltaMod;
	if (mJumpMod >= 0)
		jumpDelta *= mJumpDescentMod;	// Fall accelerate at mJumpDescentMod times the speed of ascent
	mJumpMod += jumpDelta;
	// Ensure we always max out at gravity descent
	if (mJumpMod > 1)
		mJumpMod = 1;

	mShape.setPosition(GetComponent<StaticTransformComponent>()->GetPos());
}

void Mob::Draw(float dt)
{
	Octomorph::Managers::ViewManager::GetInstance()->GetWindow().draw(mShape);
}

float Mob::GetStat(MobStat stat)
{
	return mStatsList.at(stat);
}

sf::Vector2f Mob::GetPos()
{
	return GetComponent<StaticTransformComponent>()->GetPos();
}

void Mob::SetPos(sf::Vector2f pos)
{
	GetComponent<StaticTransformComponent>()->SetPos(pos);
}

// PROTECTED
void Mob::SetJumpState(Mob::JumpState jumpState)
{
	mLastJumpState = mJumpState;
	// Leaving state
	switch (mJumpState)
	{
	default:
		break;
	}

	mJumpState = jumpState;
	mTimeInJumpState = 0;

	// Entering state
	switch (mJumpState)
	{
	default:
		break;
	}
}

bool Mob::IsWallClinging()
{
	return mJumpState == JumpState::WallClingingLeft || mJumpState == JumpState::WallClingingRight;
}

bool Mob::WasWallClinging()
{
	return mLastJumpState == JumpState::WallClingingLeft || mLastJumpState == JumpState::WallClingingRight;
}

void Mob::HandleMovement(float dt)
{
	// Calculate intended movement based on input
	sf::Vector2f inputVel;
	////////////////////////////////////////////////////////////////////////////////////////////// HORIZONTAL INPUT
	if (GetComponent<InputComponent>()->GetDigital(ETOI(InputDigital::MoveLeft)))
	{
		if (mJumpState == JumpState::WallClingingRight)
		{
			// Clinging to a wall on the right, so moving left should make us fall again
			SetJumpState(JumpState::Jumping);
			// Set jump mod to mWallClingHopMod so we push off just a bit before plummeting
			mJumpMod = -mWallClingHopMod;
		}

		if (mJumpState == JumpState::Grounded)
		{
			inputVel.x = -mStatsList[MobStat::MOV_SPEED];
		}
		else
		{
			inputVel.x = -mStatsList[MobStat::MOV_AIR_SPEED];
		}
	}

	if (GetComponent<InputComponent>()->GetDigital(ETOI(InputDigital::MoveRight)))
	{
		if (mJumpState == JumpState::WallClingingLeft)
		{
			// Clinging to a wall on the left, so moving right should make us fall again
			SetJumpState(JumpState::Jumping);
			// Set jump mod to mWallClingHopMod so we push off just a bit before plummeting
			mJumpMod = -mWallClingHopMod;
		}

		if (mJumpState == JumpState::Grounded)
		{
			inputVel.x = mStatsList[MobStat::MOV_SPEED];
		}
		else
		{
			inputVel.x = mStatsList[MobStat::MOV_AIR_SPEED];
		}
	}

	////////////////////////////////////////////////////////////////////////////////////////////// VERTICAL INPUT
	if (GetComponent<InputComponent>()->GetDigitalDown(ETOI(InputDigital::Jump)))
	{
		if (mJumpState == JumpState::Grounded || IsWallClinging())
		{
			SetJumpState(JumpState::Jumping);
			mJumpMod = -mInitialJumpMod;
		}
	}

	// Allow for early jump exit
	if (GetComponent<InputComponent>()->GetDigitalUp(ETOI(InputDigital::Jump)))
		if (mJumpState == JumpState::Jumping && mJumpMod < 0)
			mJumpMod = 0;

	////////////////////////////////////////////////////////////////////////////////////////////// STATE CHECKS
	// Check if we have slid off of a wall
	if (IsWallClinging())
	{
		// We think we're wallclinging.. let's check again since we might have slid off of a wall
		float side = 1;
		if (mJumpState == JumpState::WallClingingLeft)
		{
			side = -1;
		}

		// Check the tile to our side
		std::shared_ptr<Tile> colTile = mMap->GetTileAtWorldPos(GetPos() + sf::Vector2f(mSize.x * side, 0));

		if (colTile == nullptr)
		{
			// No longer beside a wall on that side
			SetJumpState(JumpState::Jumping);
			// We also need to do this so we don't add the horizontal force from the jump off of wall cling check below
			mTimeInJumpState = mWallClingJumpInputLock;
		}
	}

	// Apply a velocity in the opposite direction if we jump off of a wall cling
	if (WasWallClinging() && mJumpState == JumpState::Jumping && mTimeInJumpState < mWallClingJumpInputLock)
	{
		// If we're here, then we just jumped off of a wallcling less than mWallClingJumpInputLock seconds ago
		if (mLastJumpState == JumpState::WallClingingLeft)
		{
			inputVel.x = mStatsList[MobStat::MOV_AIR_SPEED];
		}
		else if (mLastJumpState == JumpState::WallClingingRight)
		{
			inputVel.x = -mStatsList[MobStat::MOV_AIR_SPEED];
		}
	}

	// Apply gravity as needed based on jumpstate
	if (!IsWallClinging())
	{
		inputVel.y = mGravity * mJumpMod;
	}
	else if (mTimeInJumpState >= mWallClingTime)
	{
		inputVel.y = mGravity * mWallClingSlideMod;
	}

	// input section complete

	// Cap our x velocity before continuing
	if (inputVel.x < 0)
		inputVel.x = max(inputVel.x, -mStatsList[MobStat::MOV_SPEED]);
	if (inputVel.x > 0)
		inputVel.x = min(inputVel.x, mStatsList[MobStat::MOV_SPEED]);

	// Times deltatime before any collision checking
	inputVel *= dt;

	// Handle collisions
	sf::Vector2f newPos = GetPos();

	inputVel.x = HandleHorizontalCollisions(inputVel.x);
	inputVel.y = HandleVerticalCollisions(inputVel.y);

	// Apply the calculated velocities
	newPos += inputVel;

	// Round the position so we're tied to 1px
	newPos.x = Octomorph::OctomorphMath::Round(newPos.x);
	newPos.y = Octomorph::OctomorphMath::Round(newPos.y);
	SetPos(newPos);
}

float Mob::HandleHorizontalCollisions(float vel)
{
	float ret = vel;
	sf::Vector2f newPos = GetPos(); newPos.y += (mSize.y / 2);

	if (vel < 0)
	{
		// Moving left
		newPos.x += vel;
		std::shared_ptr<Tile> colTile = mMap->GetTileAtWorldPos(newPos);

		if (colTile != nullptr)
		{
			// Hitting something, so we need to override ret
			ret = -(GetPos().x - (colTile->GetWorldPos().x + Tile::TileSize));

			// We check mJumpMod >= 0 to ensure we only cling on our way down from a jump
			if (!IsWallClinging() && mJumpMod >= 0)
				SetJumpState(JumpState::WallClingingLeft);
		}
	}
	else if (vel > 0)
	{
		// Moving right
		newPos.x += mSize.x + vel;
		std::shared_ptr<Tile> colTile = mMap->GetTileAtWorldPos(newPos);

		if (colTile != nullptr)
		{
			// Hitting something, so we need to override ret
			ret = colTile->GetWorldPos().x - (GetPos().x + mSize.x);

			// We check mJumpMod >= 0 to ensure we only cling on our way down from a jump
			if (!IsWallClinging() && mJumpMod >= 0)
				SetJumpState(JumpState::WallClingingRight);
		}
	}

	return ret;
}

float Mob::HandleVerticalCollisions(float vel)
{
	float ret = vel;
	sf::Vector2f newPos = GetPos(); newPos.x += (mSize.x / 2);
	if (vel < 0)
	{
		// Moving up
		newPos.y += vel;
		std::shared_ptr<Tile> colTile = mMap->GetTileAtWorldPos(newPos);

		if (colTile != nullptr)
		{
			// Hitting something, so we need to override ret
			ret = -(GetPos().y - (colTile->GetWorldPos().y + Tile::TileSize));
			// Also, we're moving upwards and hit a world tile, so let's end our jump for now
			//  until we implement one-way platforms
			mJumpMod = 0;
		}
	}
	else if (vel > 0)
	{
		// Moving down
		newPos.y += mSize.y + vel;
		std::shared_ptr<Tile> colTile = mMap->GetTileAtWorldPos(newPos);

		if (colTile != nullptr)
		{
			// Hitting something, so we need to override ret
			ret = colTile->GetWorldPos().y - (GetPos().y + mSize.y);
			// Also, we're moving downwards and hit a world tile, so let's say we're grounded for now
			//  until we implement im/passable tiles
			SetJumpState(JumpState::Grounded);

		}
	}

	return ret;
}
