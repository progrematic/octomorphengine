#include <Managers/ViewModelManager.h>
#include <Octomorph/Logging/LogManager.h>

ViewModelManager* ViewModelManager::mInstance = nullptr;

ViewModelManager::ViewModelManager() {}

ViewModelManager* ViewModelManager::GetInstance()
{
	if (!mInstance)
	{
		mInstance = new ViewModelManager();
	}

	return mInstance;
}

void ViewModelManager::PushViewModel(std::shared_ptr<ViewModel> vm)
{
	std::string msg = "ViewModelManager::PushViewModel - Pushing ";
	msg.append(vm->GetName());
	msg.append(" from ");
	msg.append(mViewModels.size() > 0 ? mViewModels.back()->GetName() : "NONE");
	Octomorph::Logging::LogManager::GetInstance()->Log(Octomorph::Logging::LogManager::LogLevel::INFO, msg.c_str());

	if (mViewModels.size() >= 1)
		mViewModels.back()->Disable();
	mViewModels.push_back(vm);
}

void ViewModelManager::PopViewModel()
{
	// Only pop if we can guarantee we have something to show after the pop
	if (mViewModels.size() > 1)
	{
		std::string msg = "ViewModelManager::PushViewModel - Popping ";
		msg.append(mViewModels.back()->GetName());
		msg.append(" to ");

		mViewModels.pop_back();

		msg.append(mViewModels.back()->GetName());
		Octomorph::Logging::LogManager::GetInstance()->Log(Octomorph::Logging::LogManager::LogLevel::INFO, msg.c_str());

		mViewModels.back()->Enable();
	}
	else
	{
		std::string msg = "ViewModelManager::PushViewModel - Can't pop last ViewModel - ";
		msg.append(mViewModels.back()->GetName());
		Octomorph::Logging::LogManager::GetInstance()->Log(Octomorph::Logging::LogManager::LogLevel::WARN, msg.c_str());
	}
}

void ViewModelManager::Update(float dt)
{
	mViewModels.back()->Update(dt);
}

void ViewModelManager::Draw(float dt)
{
	mViewModels.back()->Draw(dt);
}
