#include <World/Map.h>

#include <Octomorph/Managers/ViewManager.h>
#include <Octomorph/IO/json.h>
#include <Octomorph/Logging/LogManager.h>

Map::Map() {}

void Map::LoadMapFile(std::string mapPath)
{
	Json::Value data;
	std::ifstream file;
	file.open(mapPath);

	if (file.is_open())
	{
		file >> data;

		sf::Vector2i minPos = sf::Vector2i(999, 999);
		sf::Vector2i maxPos = sf::Vector2i(-999, -999);

		mMapName = data["name"].asString();
		Clear();
		for (Json::Value::iterator it = data["tiles"].begin(); it != data["tiles"].end(); it++)
		{
			std::shared_ptr<Tile> tile = std::make_shared<Tile>();

			tile->SetTileType(ITOE(Tile::TileType, (*it)["type"].asInt()));
			sf::Vector2i pos;
			pos.x = (*it)["x"].asInt();
			pos.y = (*it)["y"].asInt();
			minPos.x = Octomorph::OctomorphMath::Min(minPos.x, pos.x);
			minPos.y = Octomorph::OctomorphMath::Min(minPos.y, pos.y);
			maxPos.x = Octomorph::OctomorphMath::Max(maxPos.x, pos.x);
			maxPos.y = Octomorph::OctomorphMath::Max(maxPos.y, pos.y);
			tile->SetGridPos(sf::Vector2f(pos));

			mTiles.push_back(tile);
		}
		
		sf::Vector2f viewpointPos;
		viewpointPos.x = (((((float)maxPos.x - (float)minPos.x) / 2) + (float)minPos.x) * Tile::TileSize) + (Tile::TileSize / 2);
		viewpointPos.y = (((((float)maxPos.y - (float)minPos.y) / 2) + (float)minPos.y) * Tile::TileSize) + (Tile::TileSize / 2);
		Octomorph::Managers::ViewManager::GetInstance()->SetGameViewpoint(Octomorph::Enums::VIEW::MAIN, viewpointPos);

		std::string msg = "Map::LoadMapFile - Successfully loaded map from file: ";
		msg.append(mapPath);
		Octomorph::Logging::LogManager::GetInstance()->Log(Octomorph::Logging::LogManager::LogLevel::INFO, msg.c_str());

	}
	else
	{
		std::string msg = "Map::LoadMapFile - Could not load file: ";
		msg.append(mapPath);
		Octomorph::Logging::LogManager::GetInstance()->Log(Octomorph::Logging::LogManager::LogLevel::ERROR, msg.c_str());
	}
}

void Map::SaveMap(std::string mapPath)
{
	Json::Value tileArray(Json::arrayValue);
	for (std::shared_ptr<Tile> tile : mTiles)
	{
		Json::Value tileValue;
		tileValue["type"] = ETOI(tile->GetTileType());
		tileValue["x"] = tile->GetGridPos().x;
		tileValue["y"] = tile->GetGridPos().y;
		tileArray.append(tileValue);
	}

	Json::Value data;
	data["name"] = mMapName;
	data["tiles"] = tileArray;

	std::ofstream file;
	file.open(mapPath);
	if (file.is_open())
	{
		file << data;
		file.close();
		std::string msg = "Map::SaveMap - Map successfully saved to file: ";
		msg.append(mapPath);
		Octomorph::Logging::LogManager::GetInstance()->Log(Octomorph::Logging::LogManager::LogLevel::INFO, msg.c_str());
	}
	else
	{
		std::string msg = "Map::SaveMap - Could not save to file: ";
		msg.append(mapPath);
		Octomorph::Logging::LogManager::GetInstance()->Log(Octomorph::Logging::LogManager::LogLevel::ERROR, msg.c_str());
	}
}

void Map::Clear()
{
	mTiles.clear();
}

void Map::SetName(const std::string& name)
{
	mMapName = name;
}

void Map::AddTile(std::shared_ptr<Tile> tile)
{
	mTiles.push_back(tile);
}

void Map::DeleteTile(std::shared_ptr<Tile> tile)
{
	mTiles.erase(std::remove(mTiles.begin(), mTiles.end(), tile), mTiles.end());
}

std::shared_ptr<Tile> Map::GetTileAtWorldPos(sf::Vector2f pos)
{
	std::shared_ptr<Tile> ret = nullptr;
	for (std::shared_ptr<Tile> tile : mTiles)
	{
		sf::FloatRect rc;
		rc.left = tile->GetWorldPos().x;
		rc.top = tile->GetWorldPos().y;
		rc.width = Tile::TileSize;
		rc.height = Tile::TileSize;
		if (Octomorph::OctomorphMath::IsPointInRect(pos, rc))
		{
			ret = tile;
			break;
		}
	}

	return ret;
}

void Map::Update(float dt)
{
	for (std::shared_ptr<Tile> tile : mTiles)
	{
		tile->Update(dt);
	}
}

void Map::Draw(float dt)
{
	for (std::shared_ptr<Tile> tile : mTiles)
	{
		tile->Draw(dt);
	}
}