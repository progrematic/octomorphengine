#include <World/Tile.h>

#include <Octomorph/Managers/ViewManager.h>
#include <Octomorph/Components/SpritesheetComponent.h>

int Tile::TileSize = 16;

Tile::Tile()
	: mPos(sf::Vector2f(0, 0))
{
	SetTileType(TileType::Ground);
	mShape.setSize(sf::Vector2f((float)TileSize, (float)TileSize));
}

void Tile::SetWorldPos(sf::Vector2f pos)
{
	mShape.setPosition(pos);
	pos.x = std::floor(pos.x / TileSize);
	pos.y = std::floor(pos.y / TileSize);
	mPos = pos;
}

void Tile::SetGridPos(sf::Vector2f pos)
{
	mPos = pos;
	sf::Vector2f shapePos = mPos;
	shapePos.x *= TileSize;
	shapePos.y *= TileSize;
	mShape.setPosition(shapePos);
}

void Tile::SetTileType(TileType type)
{
	mTileType = type;
	switch (mTileType)
	{
	case TileType::Ground:
	{
		mShape.setFillColor(sf::Color::Green);
	}
	break;
	case TileType::Water:
	{
		mShape.setFillColor(sf::Color::Blue);
	}
	break;
	default:
		break;
	}
}

void Tile::Update(float dt)
{

}

void Tile::Draw(float dt)
{
	if (!IsInvalid())
		Octomorph::Managers::ViewManager::GetInstance()->GetWindow().draw(mShape);
}

sf::Vector2f Tile::GetWorldPos()
{
	return mShape.getPosition();
}

sf::Vector2i Tile::GetGridPos()
{
	const sf::Vector2f pos = mShape.getPosition();
	return sf::Vector2i((int)(pos.x / TileSize), (int)(pos.y / TileSize));
}

Tile::TileType Tile::GetTileType()
{
	return mTileType;
}

// PRIVATE

bool Tile::IsInvalid()
{
	return mTileType == TileType::Invalid;
}