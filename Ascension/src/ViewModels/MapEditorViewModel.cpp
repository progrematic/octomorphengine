#include <ViewModels/MapEditorViewModel.h>
#include <ViewModels/GameplayViewModel.h>
#include <Managers/ViewModelManager.h>

#include <Globals.h>

#include <Octomorph/Managers/ViewManager.h>
#include <Octomorph/Managers/EventManager.h>
#include <Octomorph/GUI/Widgets/Input.h>
#include <Octomorph/Input/Mouse.h>
#include <Octomorph/Input/Keyboard.h>
#include <Octomorph/GUI/Widgets/Button.h>
#include <Octomorph/GUI/Widgets/Input.h>
#include <Octomorph/GUI/Widgets/TextLabel.h>
#include <Octomorph/Logging/LogManager.h>

using namespace Octomorph::Managers;
using namespace Octomorph::Input;
using namespace Octomorph::GUI::Widgets;

MapEditorViewModel::MapEditorViewModel()
{
	mName = "MapEditorViewModel";
	// Not setting mLoaded to true since this viewmodel will trak its own GUIs
	//mLoaded = true;
	CreateToolGui();
	CreateOpenGui();
	CreateSaveGui();
	CreateClearGui();

	mRectToolRect.setOutlineColor(sf::Color::Black);
	mRectToolRect.setOutlineThickness(2);

	sf::Vector2f windowSize = sf::Vector2f(ViewManager::GetInstance()->GetWindow().getSize());
	sf::Color highlightColor = sf::Color::White;
	highlightColor.a = 127;

	mHorTileHighlight.setPosition(sf::Vector2f(0, 0));
	mHorTileHighlight.setSize(sf::Vector2f(windowSize.x, Tile::TileSize));
	mHorTileHighlight.setFillColor(highlightColor);

	mVerTileHighlight.setPosition(sf::Vector2f(0, 0));
	mVerTileHighlight.setSize(sf::Vector2f(Tile::TileSize, windowSize.y));
	mVerTileHighlight.setFillColor(highlightColor);

	mMap = std::make_shared<Map>();
	mSelectorShape = sf::RectangleShape(sf::Vector2f(Tile::TileSize, Tile::TileSize));
	mTool = Tool::Single;
	SetTileType(Tile::TileType::Ground);
	mIgnoreMouse = false;
	mDrawRectToolRect = false;
}

void MapEditorViewModel::Enable()
{
	mToolGUI.Enable();
}

void MapEditorViewModel::Disable()
{
	mToolGUI.Disable();
	mOpenGUI.Disable();
	mSaveGUI.Disable();
	mClearGUI.Disable();
}

void MapEditorViewModel::Update(float dt)
{
	ViewModel::Update(dt);
	sf::Vector2f selectorPos;
	selectorPos.x = std::floor(Mouse::GetMouseX() / Tile::TileSize) * Tile::TileSize;
	selectorPos.y = std::floor(Mouse::GetMouseY() / Tile::TileSize) * Tile::TileSize;
	mSelectorShape.setPosition(selectorPos);
	
	sf::Vector2f horPos = mHorTileHighlight.getPosition();
	horPos.y = mSelectorShape.getPosition().y;
	mHorTileHighlight.setPosition(horPos);
	sf::Vector2f verPos = mVerTileHighlight.getPosition();
	verPos.x = mSelectorShape.getPosition().x;
	mVerTileHighlight.setPosition(verPos);

	HandleKeyboard(dt);
	HandleMouse(dt);

	mMap->Update(dt);

	mToolGUI.Update(dt);
	mOpenGUI.Update(dt);
	mSaveGUI.Update(dt);
	mClearGUI.Update(dt);

	// This lives here and not in HandleKeyboard because popping this viewmodel invalidates the rest of the flow
	// Therefore, it must be the last thing we do in Update()
	if (Keyboard::KeyDown(sf::Keyboard::Escape))
	{
		if (mOpenGUI.IsEnabled())
		{
			mOpenGUI.Disable();
		}
		else if (mSaveGUI.IsEnabled())
		{
			mSaveGUI.Disable();
		}
		else if (mClearGUI.IsEnabled())
		{
			mClearGUI.Disable();
		}
		else
		{
			ViewModelManager::GetInstance()->PopViewModel();
		}
	}
}

void MapEditorViewModel::Draw(float dt)
{
	ViewModel::Draw(dt);

	mMap->Draw(dt);
	ViewManager::GetInstance()->GetWindow().draw(mSelectorShape);
	ViewManager::GetInstance()->GetWindow().draw(mHorTileHighlight);
	ViewManager::GetInstance()->GetWindow().draw(mVerTileHighlight);
	if (mDrawRectToolRect)
		ViewManager::GetInstance()->GetWindow().draw(mRectToolRect);

	mToolGUI.Draw(dt);
	mOpenGUI.Draw(dt);
	mSaveGUI.Draw(dt);
	mClearGUI.Draw(dt);
}

// PRIVATE

void MapEditorViewModel::SetTileType(Tile::TileType tileType)
{
	mTileType = tileType;
	switch (mTileType)
	{
	case Tile::TileType::Ground:
	{
		mSelectorShape.setFillColor(sf::Color(0, 255, 0, 127));
	}
	break;
	case Tile::TileType::Water:
	{
		mSelectorShape.setFillColor(sf::Color(0, 0, 255, 127));
	}
	break;
	}

	mRectToolRect.setFillColor(mSelectorShape.getFillColor());
}

void MapEditorViewModel::HandleKeyboard(float dt)
{
	if (Keyboard::KeyDown(sf::Keyboard::O))
		if (Keyboard::Key(sf::Keyboard::LControl))
			mOpenGUI.Enable();
	if (Keyboard::KeyDown(sf::Keyboard::S))
		if (Keyboard::Key(sf::Keyboard::LControl))
			mSaveGUI.Enable();
	if (Keyboard::KeyDown(sf::Keyboard::P))
		if (Keyboard::Key(sf::Keyboard::LControl))
		{
			std::shared_ptr<GameplayViewModel> gameplay = std::make_shared<GameplayViewModel>();
			gameplay->SetMap(mMap);
			ViewModelManager::GetInstance()->PushViewModel(gameplay);
		}

	if (Keyboard::KeyDown(sf::Keyboard::W))
	{
		if (Keyboard::Key(sf::Keyboard::LControl))
		{
			if (mClearGUI.IsEnabled())
			{
				ClearCallback();
			}
			else
			{
				mClearGUI.Enable();
			}
		}
	}
	if (Keyboard::KeyDown(sf::Keyboard::Num1))
		SetTileType(Tile::TileType::Ground);
	if (Keyboard::KeyDown(sf::Keyboard::Num2))
		SetTileType(Tile::TileType::Water);
}

void MapEditorViewModel::HandleMouse(float dt)
{
	// Skip any mouse input if GUI consumed it
	if (Octomorph::Managers::EventManager::GetInstance()->ConsumedThisFrame())
		mIgnoreMouse = true;

	if (mIgnoreMouse)
	{
		if (Mouse::ButtonUp(sf::Mouse::Left))
			mIgnoreMouse = false;
		return;
	}

	// Add half the tilesize to the pos to ensure we're checking a point *within* the rect
	mToolEndPos = mSelectorShape.getPosition() + sf::Vector2f(Tile::TileSize / 2, Tile::TileSize / 2);

	sf::Vector2f minPos = sf::Vector2f(Octomorph::OctomorphMath::Min(mToolStartPos.x, mToolEndPos.x),
		Octomorph::OctomorphMath::Min(mToolStartPos.y, mToolEndPos.y));
	sf::Vector2f maxPos = sf::Vector2f(Octomorph::OctomorphMath::Max(mToolStartPos.x, mToolEndPos.x),
		Octomorph::OctomorphMath::Max(mToolStartPos.y, mToolEndPos.y));

	int horTiles = ((maxPos.x - minPos.x) / Tile::TileSize) + 1;
	int verTiles = ((maxPos.y - minPos.y) / Tile::TileSize) + 1;

	mRectToolRect.setPosition(minPos - sf::Vector2f(Tile::TileSize / 2, Tile::TileSize / 2));
	mRectToolRect.setSize(sf::Vector2f(horTiles * Tile::TileSize, verTiles * Tile::TileSize));

	switch (mTool)
	{
	case Tool::Single:
	{
		if (Mouse::ButtonDown(sf::Mouse::Left) || Keyboard::Key(sf::Keyboard::Space))
		{
			// Add half the tilesize to the pos to ensure we're checking a point *within* the rect
			sf::Vector2f pos = mSelectorShape.getPosition() + sf::Vector2f(Tile::TileSize / 2, Tile::TileSize / 2);
			std::shared_ptr<Tile> hoverTile = mMap->GetTileAtWorldPos(pos);
			if (hoverTile == nullptr)
			{
				std::shared_ptr<Tile> tile = std::make_shared<Tile>();
				tile->SetTileType(mTileType);
				// pos was offset for checking purposes.. we should use the real position here instead
				tile->SetWorldPos(mSelectorShape.getPosition());
				mMap->AddTile(tile);
			}
			else if (hoverTile->GetTileType() != mTileType)
			{
				hoverTile->SetTileType(mTileType);
			}
		}
	}
	break;
	case Tool::Rect:
	case Tool::Erase:
	{
		if (Mouse::ButtonDown(sf::Mouse::Left))
		{
			// Add half the tilesize to the pos to ensure we're checking a point *within* the rect
			mToolStartPos = mSelectorShape.getPosition() + sf::Vector2f(Tile::TileSize / 2, Tile::TileSize / 2);
			mRectToolRect.setPosition(mSelectorShape.getPosition());
			mRectToolRect.setSize(sf::Vector2f(Tile::TileSize, Tile::TileSize));
			mDrawRectToolRect = true;
			if (mTool == Tool::Rect)
				mRectToolRect.setFillColor(mSelectorShape.getFillColor());
			else if (mTool == Tool::Erase)
			{
				sf::Color c = sf::Color::Black;
				c.a = 127;
				mRectToolRect.setFillColor(c);
			}
		}
		else if (Mouse::ButtonUp(sf::Mouse::Left))
		{
			mDrawRectToolRect = false;

			for (int i = 0; i < horTiles; i++)
			{
				for (int j = 0; j < verTiles; j++)
				{
					sf::Vector2f pos = minPos + sf::Vector2f(i * Tile::TileSize, j * Tile::TileSize);
					std::shared_ptr<Tile> hoverTile = mMap->GetTileAtWorldPos(pos);
					if (hoverTile == nullptr)
					{
						if (mTool == Tool::Rect)
						{
							std::shared_ptr<Tile> tile = std::make_shared<Tile>();
							tile->SetTileType(mTileType);
							// pos was offset for checking purposes.. we should use the real position here instead
							tile->SetWorldPos(pos - sf::Vector2f(Tile::TileSize / 2, Tile::TileSize / 2));
							mMap->AddTile(tile);
						}
					}
					else
					{
						if (mTool == Tool::Rect)
						{
							if (hoverTile->GetTileType() != mTileType)
								hoverTile->SetTileType(mTileType);
						}
						else if (mTool == Tool::Erase)
						{
							mMap->DeleteTile(hoverTile);
						}
					}
				}
			}
		}
	}
	break;
	}
}

void MapEditorViewModel::CreateToolGui()
{
	sf::Vector2f guiPos = sf::Vector2f(0, 0);
	mToolGUI = Octomorph::GUI::GUI(guiPos);

	mOpenGUI.SetColor(sf::Color::Transparent);

	std::shared_ptr<Button> singleTool = std::make_shared<Button>();
	singleTool->SetPos(sf::Vector2f(0, 0));
	singleTool->SetSize(sf::Vector2f(45, 45));
	singleTool->SetCallback([&tool = mTool]() { tool = Tool::Single; });
	mToolGUI.AddWidget(singleTool);

	std::shared_ptr<Button> rectTool = std::make_shared<Button>();
	rectTool->SetPos(sf::Vector2f(50, 0));
	rectTool->SetSize(sf::Vector2f(45, 45));
	rectTool->SetCallback([&tool = mTool]() { tool = Tool::Rect; });
	mToolGUI.AddWidget(rectTool);

	std::shared_ptr<Button> eraseTool = std::make_shared<Button>();
	eraseTool->SetPos(sf::Vector2f(100, 0));
	eraseTool->SetSize(sf::Vector2f(45, 45));
	eraseTool->SetCallback([&tool = mTool]() { tool = Tool::Erase; });
	mToolGUI.AddWidget(eraseTool);

	mToolGUI.Enable();
}

void MapEditorViewModel::CreateOpenGui()
{
	sf::Vector2f guiPos = sf::Vector2f(ViewManager::GetInstance()->GetWindow().getSize());
	guiPos.x *= 0.5f;
	guiPos.y -= 50;
	mOpenGUI = Octomorph::GUI::GUI(guiPos);

	mOpenGUI.SetColor(sf::Color::Transparent);

	std::shared_ptr<Input> cmdInput = std::make_shared<Input>();
	cmdInput->SetAnchor(sf::Vector2f(0.5f, 0));
	cmdInput->SetSize(sf::Vector2f(400, 45));
	cmdInput->SetCallback(std::bind(&MapEditorViewModel::OpenCallback, this, std::placeholders::_1));
	mOpenGUI.AddWidget(cmdInput, true, true);
}

void MapEditorViewModel::CreateSaveGui()
{
	sf::Vector2f guiPos = sf::Vector2f(ViewManager::GetInstance()->GetWindow().getSize());
	guiPos.x *= 0.5f;
	guiPos.y -= 50;
	mSaveGUI = Octomorph::GUI::GUI(guiPos);

	mSaveGUI.SetColor(sf::Color::Transparent);

	std::shared_ptr<Input> cmdInput = std::make_shared<Input>();
	cmdInput->SetAnchor(sf::Vector2f(0.5f, 0));
	cmdInput->SetSize(sf::Vector2f(400, 45));
	cmdInput->SetCallback(std::bind(&MapEditorViewModel::SaveCallback, this, std::placeholders::_1));
	mSaveGUI.AddWidget(cmdInput, true, true);
}

void MapEditorViewModel::CreateClearGui()
{
	sf::Vector2f guiPos = sf::Vector2f(ViewManager::GetInstance()->GetWindow().getSize());
	guiPos.x *= 0.5f;
	guiPos.y -= 50;
	mClearGUI = Octomorph::GUI::GUI(guiPos);

	mClearGUI.SetColor(sf::Color::Black);

	std::shared_ptr<TextLabel> cmdInput = std::make_shared<TextLabel>();
	cmdInput->SetAnchor(sf::Vector2f(0.5f, 0));
	cmdInput->SetColor(sf::Color::White);
	cmdInput->SetString("Press CTRL+W again to clear");
	mClearGUI.AddWidget(cmdInput);
}

void MapEditorViewModel::OpenCallback(const std::string& path)
{
	mMap->LoadMapFile(MapPath + path);
	mOpenGUI.Disable();
}

void MapEditorViewModel::SaveCallback(const std::string& path)
{
	mMap->SetName(path);
	mMap->SaveMap(MapPath + path);
	mSaveGUI.Disable();
}

void MapEditorViewModel::ClearCallback()
{
	mMap->Clear();
	mClearGUI.Disable();
	Octomorph::Logging::LogManager::GetInstance()->Log(Octomorph::Logging::LogManager::LogLevel::INFO, "MapEditorViewModel::ClearCallback - Map cleared");
}
