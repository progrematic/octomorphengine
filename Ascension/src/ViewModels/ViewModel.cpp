#include <ViewModels/ViewModel.h>
#include <Octomorph/IO/json.h>
#include <Octomorph/Input/Keyboard.h>

using namespace Octomorph::Input;

ViewModel::ViewModel() : mLoaded(false), mName("UNNAMED") {}

void ViewModel::LoadUIView(std::string uiViewPath)
{
	Json::Value data;
	std::ifstream file;
	file.open(uiViewPath);
	if (file.is_open())
	{
		mLoaded = true;
		file >> data;

	}
	else
	{
		std::cout << "Error loading UIView: " << uiViewPath << std::endl;
	}
}

const char* ViewModel::GetName()
{
	return mName;
}

void ViewModel::Enable()
{
	if (mLoaded)
	{
		mGui.Enable();
	}
}

void ViewModel::Disable()
{
	if (mLoaded)
	{
		mGui.Disable();
	}
}

void ViewModel::Update(float dt)
{
	if (mLoaded)
	{
		if (Keyboard::KeyDown(sf::Keyboard::Tab))
		{
			if (Keyboard::Key(sf::Keyboard::LShift))
				mGui.PrevTabbable();
			else
				mGui.NextTabbable();
		}

		mGui.Update(dt);
	}
}

void ViewModel::Draw(float dt)
{
	if (mLoaded)
	{
		mGui.Draw(dt);
	}
}