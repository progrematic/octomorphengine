#include <ViewModels/MainViewModel.h>
#include <ViewModels/MapEditorViewModel.h>
#include <Managers/ViewModelManager.h>

#include <Octomorph/Input/Keyboard.h>

#include <Octomorph/Managers/ViewManager.h>
#include <Octomorph/GUI/Widgets/Input.h>

using namespace Octomorph::Managers;
using namespace Octomorph::GUI::Widgets;

MainViewModel::MainViewModel()
{
	mName = "MainViewModel";
	sf::Vector2f guiPos = sf::Vector2f(ViewManager::GetInstance()->GetWindow().getSize());
	guiPos.x *= 0.5f;
	guiPos.y *= 0.5f;
	mGui = Octomorph::GUI::GUI(guiPos);

	mGui.SetColor(sf::Color::Transparent);

	std::shared_ptr<Input> cmdInput = std::make_shared<Input>();
	cmdInput->SetAnchor(sf::Vector2f(0.5f, 0));
	cmdInput->SetPos(sf::Vector2f(0, 0));
	cmdInput->SetSize(sf::Vector2f(200, 45));
	cmdInput->SetCallback(std::bind(&MainViewModel::CommandInputCallback, this, std::placeholders::_1));
	mGui.AddWidget(cmdInput, true, true);

	mGui.Enable();
	mLoaded = true;
}

void MainViewModel::Update(float dt)
{
	ViewModel::Update(dt);

	if (Octomorph::Input::Keyboard::KeyDown(sf::Keyboard::Escape))
	{
		Octomorph::Managers::ViewManager::GetInstance()->GetWindow().close();
	}
}

// PRIVATE
void MainViewModel::CommandInputCallback(const std::string& cmd)
{
	if (cmd == "mapeditor")
	{
		std::shared_ptr<ViewModel> mapEditor = std::make_shared<MapEditorViewModel>();
		ViewModelManager::GetInstance()->PushViewModel(mapEditor);
	}
	else if (cmd == "pop")
	{
		ViewModelManager::GetInstance()->PopViewModel();
	}
}