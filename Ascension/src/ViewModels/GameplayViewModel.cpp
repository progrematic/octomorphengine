#include <ViewModels/GameplayViewModel.h>
#include <Managers/ViewModelManager.h>

#include <Octomorph/Input/Keyboard.h>
using namespace Octomorph::Input;

GameplayViewModel::GameplayViewModel()
{
	mName = "GameplayViewModel";
};

void GameplayViewModel::SetMap(std::shared_ptr<Map> map)
{
	mMap = map;
	mPlayer = std::make_shared<Player>();
	mPlayer->Initialize(mMap);
	mPlayer->SetPos(sf::Vector2f(100, 100));
}

void GameplayViewModel::Update(float dt)
{
	mMap->Update(dt);
	mPlayer->Update(dt);

	// This lives here and not in HandleKeyboard because popping this viewmodel invalidates the rest of the flow
	// Therefore, it must be the last thing we do in Update()
	if (Keyboard::KeyDown(sf::Keyboard::Escape))
	{
		ViewModelManager::GetInstance()->PopViewModel();
	}
}

void GameplayViewModel::Draw(float dt)
{
	mMap->Draw(dt);
	mPlayer->Draw(dt);
}