#ifndef MAPEDITORVIEWMODEL_H
#define MAPEDITORVIEWMODEL_H

#include <Octomorph/GUI/GUI.h>

#include <ViewModels/ViewModel.h>
#include <World/Map.h>

class MapEditorViewModel : public ViewModel
{
public:
	MapEditorViewModel();

	void Enable() override;
	void Disable() override;

	void Update(float dt) override;
	void Draw(float dt) override;

private:
	enum class Tool
	{
		Single,
		Rect,
		Erase,
		Count
	};

private:
	void SetTileType(Tile::TileType tileType);
	void HandleKeyboard(float dt);
	void HandleMouse(float dt);

	void CreateToolGui();
	void CreateOpenGui();
	void CreateSaveGui();
	void CreateClearGui();

	void OpenCallback(const std::string& path);
	void SaveCallback(const std::string& path);
	void ClearCallback();

private:
	Tool mTool;
	std::shared_ptr<Map> mMap;
	Tile::TileType mTileType;
	sf::RectangleShape mSelectorShape;
	bool mIgnoreMouse;

	sf::RectangleShape mHorTileHighlight;
	sf::RectangleShape mVerTileHighlight;
	sf::Vector2f mToolStartPos;
	sf::Vector2f mToolEndPos;
	bool mDrawRectToolRect;
	sf::RectangleShape mRectToolRect;

	Octomorph::GUI::GUI mToolGUI;
	Octomorph::GUI::GUI mOpenGUI;
	Octomorph::GUI::GUI mSaveGUI;
	Octomorph::GUI::GUI mClearGUI;
};

#endif
