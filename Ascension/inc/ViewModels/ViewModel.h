#ifndef VIEWMODEL_H
#define VIEWMODEL_H

#include <Octomorph/Headers.h>
#include <Octomorph/GUI/GUI.h>

class ViewModel
{
public:
	ViewModel();

	const char* GetName();

	void LoadUIView(std::string uiViewPath);

	virtual void Enable();
	virtual void Disable();

	virtual void Update(float dt);
	virtual void Draw(float dt);

protected:
	const char* mName;
	bool mLoaded;
	Octomorph::GUI::GUI mGui;
};

#endif
