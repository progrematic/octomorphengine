#ifndef MAINVIEWMODEL_H
#define MAINVIEWMODEL_H
 
#include <ViewModels/ViewModel.h>

class MainViewModel : public ViewModel
{
public:
	MainViewModel();

	void Update(float dt) override;

private:
	void CommandInputCallback(const std::string& cmd);

private:

};

#endif
