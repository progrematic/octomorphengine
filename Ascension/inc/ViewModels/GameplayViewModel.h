#ifndef GAMEPLAYVIEWMODEL_H
#define GAMEPLAYVIEWMODEL_H

#include <ViewModels/ViewModel.h>

#include <Objects/Player.h>
#include <World/Map.h>

class GameplayViewModel : public ViewModel
{
public:
	GameplayViewModel();

	void SetMap(std::shared_ptr<Map> map);

	void Update(float dt) override;
	void Draw(float dt) override;

private:
	std::shared_ptr<Map> mMap;
	std::shared_ptr<Player> mPlayer;
};

#endif
