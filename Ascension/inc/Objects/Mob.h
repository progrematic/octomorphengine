#ifndef MOB_H
#define MOB_H

#include <Objects/Object.h>
#include <World/Map.h>
#include <Globals.h>

#include <Octomorph/Headers.h>
#include <map>

class Mob : public Object
{
public:
	enum class JumpState
	{
		Grounded,
		Jumping,
		WallClingingLeft,
		WallClingingRight,
		Count
	};

public:
	Mob();

	void Initialize(std::shared_ptr<Map> map);

	void Update(float dt) override;
	void Draw(float dt) override;

	float GetStat(MobStat stat);
	sf::Vector2f GetPos();
	virtual void SetPos(sf::Vector2f pos);

protected:
	void SetJumpState(JumpState jumpState);
	bool IsWallClinging();
	bool WasWallClinging();

	void HandleMovement(float dt);
	float HandleHorizontalCollisions(float vel);
	float HandleVerticalCollisions(float vel);

protected:
	std::map<MobStat, float> mStatsList;
	std::shared_ptr<Map> mMap;
	sf::RectangleShape mShape;
	sf::Vector2f mSize;

	JumpState mJumpState;
	JumpState mLastJumpState;
	float mTimeInJumpState;

	float mGravity;
	float mInitialJumpMod;
	float mJumpMod;
	float mJumpDeltaMod;
	float mJumpDescentMod;
	float mWallClingSlideMod;
	float mWallClingHopMod;
	float mWallClingJumpInputLock;
	float mWallClingTime;
};

#endif
