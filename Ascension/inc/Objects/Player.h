#ifndef PLAYER_H
#define PLAYER_H

#include <Objects/Mob.h>

class Player : public Mob
{
public:
	Player();

	void Update(float dt) override;
	void Draw(float dt) override;

private:
};

#endif