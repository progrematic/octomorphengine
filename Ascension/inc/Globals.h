#ifndef GLOBALS_H
#define GLOBALS_H

#include <string>

static std::string MapPath = "Resources/Maps/";

enum class InputDigital
{
	MoveLeft,
	MoveRight,
	Jump,
	Crouch,
	Attack,
	Count
};

enum class MobStat
{
	INVALID,
	HP,
	MP,
	ENERGY,
	ATK,
	DEF,
	INT,
	DEX,
	HP_REGEN,
	MP_REGEN,
	ENERGY_REGEN,
	ATK_SPEED,
	MOV_SPEED,
	MOV_AIR_SPEED,
	PICKUP_RADIUS,
	SIGHT_RADIUS,
	COUNT
};

#endif
