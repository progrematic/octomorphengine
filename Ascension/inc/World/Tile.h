#ifndef TILE_H
#define TILE_H

#include <Octomorph/Headers.h>
#include <Octomorph/Globals.h>

#include <Objects/Object.h>

class Tile : public Object
{
public:
	enum class TileType
	{
		Invalid = -1,
		Ground,
		Water,
		Count
	};

	static int TileSize;

public:
	Tile();

	void SetWorldPos(sf::Vector2f pos);
	void SetGridPos(sf::Vector2f pos);
	void SetTileType(TileType type);
	
	sf::Vector2f GetWorldPos();
	sf::Vector2i GetGridPos();
	TileType GetTileType();

	void Update(float dt);
	void Draw(float dt);

private:
	bool IsInvalid();

private:
	TileType mTileType;
	sf::Vector2f mPos;

	// TODO: Replace rectangle shape with SpritesheetComponent
	sf::RectangleShape mShape;
};

#endif