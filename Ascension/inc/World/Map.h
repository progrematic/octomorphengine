#ifndef MAP_H
#define MAP_H

#include <Octomorph/Headers.h>
#include <Octomorph/Globals.h>

#include <World/Tile.h>

class Map
{
public:
	Map();

	void LoadMapFile(std::string mapPath);
	void SaveMap(std::string mapPath);
	void Clear();

	void SetName(const std::string& name);
	void AddTile(std::shared_ptr<Tile> tile);
	void DeleteTile(std::shared_ptr<Tile> tile);
	std::shared_ptr<Tile> GetTileAtWorldPos(sf::Vector2f pos);

	void Update(float dt);
	void Draw(float dt);

private:
	std::string mMapName;
	std::vector<std::shared_ptr<Tile>> mTiles;
};

#endif
