#ifndef VIEWMODELMANAGER_H
#define VIEWMODELMANAGER_H

#include <ViewModels/ViewModel.h>

#include <Octomorph/Headers.h>
#include <deque>

class ViewModelManager
{
public:
	ViewModelManager();

	static ViewModelManager* GetInstance();

	void PushViewModel(std::shared_ptr<ViewModel> vm);
	void PopViewModel();

	void Update(float dt);
	void Draw(float dt);

private:
	static ViewModelManager* mInstance;
	std::deque<std::shared_ptr<ViewModel>> mViewModels;
};

#endif
