#include <Objects/Mobs/Player.h>

#include <Octomorph/Managers/TextureManager.h>
#include <Octomorph/Managers/ViewManager.h>
#include <Managers/ItemManager.h>

#include <Octomorph/Input/Keyboard.h>
#include <Octomorph/Input/Mouse.h>
#include <Octomorph/Input/Joystick.h>

#include <Octomorph/Components/LightSourceComponent.h>
#include <Octomorph/Components/SpriteSheetComponent.h>
#include <Octomorph/Components/DynamicTransformComponent.h>
#include <Octomorph/Components/InputComponent.h>

#include <Debugging/Console.h>

#include <Octomorph/IO/json.h>
#include <fstream>

using namespace Octomorph::Managers;
using namespace Octomorph::Components;

Player::Player() : Mob(),
	mLastSteppedTile(nullptr),
	mPickupSpeed(500)
{
	mInventory.Initialize();

	AttachComponent<LightSourceComponent>();
	GetComponent<LightSourceComponent>()->SetRadius(250);
	GetComponent<LightSourceComponent>()->SetWaning(100);

	GetComponent<SpritesheetComponent>()->SetLayerMax(ETOI(DRAW_ORDER::COUNT));
	GetComponent<SpritesheetComponent>()->LoadSpritesheet("Resources/spritesheets/mobs/players.json");
	int spriteId;
	spriteId = TextureManager::AddTexture("Resources/spritesheets/mobs/players/heads/0.png");
	GetComponent<SpritesheetComponent>()->SetSprite(*TextureManager::GetTexture(spriteId), ETOI(DRAW_ORDER::HEAD));

	// Wearables - should get moved elsewhere once we introduce items and equipment
	spriteId = TextureManager::AddTexture("Resources/spritesheets/items/wearables/boots/0.png");
	GetComponent<SpritesheetComponent>()->SetSprite(*TextureManager::GetTexture(spriteId), ETOI(DRAW_ORDER::BOOTS));
	spriteId = TextureManager::AddTexture("Resources/spritesheets/items/wearables/pants/0.png");
	GetComponent<SpritesheetComponent>()->SetSprite(*TextureManager::GetTexture(spriteId), ETOI(DRAW_ORDER::PANTS));
	spriteId = TextureManager::AddTexture("Resources/spritesheets/items/wearables/armor/0.png");
	GetComponent<SpritesheetComponent>()->SetSprite(*TextureManager::GetTexture(spriteId), ETOI(DRAW_ORDER::ARMOR));
	
	GetComponent<SpritesheetComponent>()->SetAnimation(ETOI(ANIMATION_STATE::IDLE_DOWN));

	AttachComponent<InputComponent>();
	GetComponent<InputComponent>()->SetInputType(INPUT_TYPE::CONTROLLER);
	
	GetComponent<InputComponent>()->SetKeyboardAnalogInput(ETOI(INPUT_ANALOG::VERTICAL), true, sf::Keyboard::Down);
	GetComponent<InputComponent>()->SetKeyboardAnalogInput(ETOI(INPUT_ANALOG::VERTICAL), false, sf::Keyboard::Up);
	GetComponent<InputComponent>()->SetKeyboardAnalogInput(ETOI(INPUT_ANALOG::HORIZONTAL), true, sf::Keyboard::Right);
	GetComponent<InputComponent>()->SetKeyboardAnalogInput(ETOI(INPUT_ANALOG::HORIZONTAL), false, sf::Keyboard::Left);
	GetComponent<InputComponent>()->SetJoystickAnalogInput(ETOI(INPUT_ANALOG::VERTICAL), Joystick::Axis::LeftStickVertical);
	GetComponent<InputComponent>()->SetJoystickAnalogInput(ETOI(INPUT_ANALOG::HORIZONTAL), Joystick::Axis::LeftStickHorizontal);
	
	GetComponent<InputComponent>()->SetKeyboardDigitalInput(ETOI(INPUT_DIGITAL::ACTION), sf::Keyboard::Space);
	GetComponent<InputComponent>()->SetKeyboardDigitalInput(ETOI(INPUT_DIGITAL::ATTACK), sf::Keyboard::D);
	GetComponent<InputComponent>()->SetKeyboardDigitalInput(ETOI(INPUT_DIGITAL::SPECIAL), sf::Keyboard::S);
	GetComponent<InputComponent>()->SetKeyboardDigitalInput(ETOI(INPUT_DIGITAL::ROLL), sf::Keyboard::A);
	GetComponent<InputComponent>()->SetKeyboardDigitalInput(ETOI(INPUT_DIGITAL::PICKUP), sf::Keyboard::LShift);
	GetComponent<InputComponent>()->SetKeyboardDigitalInput(ETOI(INPUT_DIGITAL::INVENTORY), sf::Keyboard::I);

	GetComponent<InputComponent>()->SetJoystickDigitalInput(ETOI(INPUT_DIGITAL::ACTION), Joystick::Button::A);
	GetComponent<InputComponent>()->SetJoystickDigitalInput(ETOI(INPUT_DIGITAL::ATTACK), Joystick::Button::X);
	GetComponent<InputComponent>()->SetJoystickDigitalInput(ETOI(INPUT_DIGITAL::SPECIAL), Joystick::Button::Y);
	GetComponent<InputComponent>()->SetJoystickDigitalInput(ETOI(INPUT_DIGITAL::ROLL), Joystick::Button::B);
	GetComponent<InputComponent>()->SetJoystickDigitalInput(ETOI(INPUT_DIGITAL::PICKUP), Joystick::Button::Back);
	GetComponent<InputComponent>()->SetJoystickDigitalInput(ETOI(INPUT_DIGITAL::INVENTORY), Joystick::Button::RB);
}

void Player::Reset()
{
	mLastSteppedTile = nullptr;
	mPickupSpeed = 500;

	mInventory.Reset();
}

void Player::SetId(int id)
{
	mId = id;
}

int Player::GetId()
{
	return mId;
}

void Player::HandleInput(float dt)
{
	sf::Vector2f vel = sf::Vector2f(0, 0);

	std::shared_ptr<InputComponent> input = GetComponent<InputComponent>();
	
	////////////////////////////////////////////////////////////////////////////////////////////////////// Movement
	vel.y = input->GetAnalog(ETOI(INPUT_ANALOG::VERTICAL)) * mStatsList.at(STAT::MOV_SPEED);
	vel.x = input->GetAnalog(ETOI(INPUT_ANALOG::HORIZONTAL)) * mStatsList.at(STAT::MOV_SPEED);

	if (input->GetInputType() == INPUT_TYPE::KEYBOARD)
	{
		// Normalize speed vector here since keyboard can max both hor and vert at the same time
		vel = Octomorph::OctomorphMath::Normalize(vel, mStatsList.at(STAT::MOV_SPEED));
	}

	// If not zero, then we should update our look target. Otherwise, let's keep looking where we were looking before
	if (vel.x != 0 || vel.y != 0)
		SetLookTarget(GetComponent<DynamicTransformComponent>()->GetPos() + vel);
	vel *= dt;

	// Apply Velocity
	GetComponent<DynamicTransformComponent>()->SetVel(vel);
	ApplyVel();

	////////////////////////////////////////////////////////////////////////////////////////////////////// Other
	if (input->GetDigitalDown(ETOI(INPUT_DIGITAL::INVENTORY)))
		mInventory.ToggleShown();

	if (input->GetDigital(ETOI(INPUT_DIGITAL::PICKUP)))
		ItemManager::GetInstance()->PlayerPickup(this);
}

void Player::Update(float dt)
{
	Mob::Update(dt);

	// Update position if we're still colliding
	// (typically only happens when entering trial rooms where doors become walls)
	SetPos(ResolveCollision());
	sf::Vector2f pos = GetComponent<DynamicTransformComponent>()->GetPos();
	
	mInventory.SetWorldPos(pos);

	std::shared_ptr<Tile> stepTile = mDungeon->GetTile(pos);
	if (stepTile)
	{
		if (mLastSteppedTile == nullptr ||
			(stepTile->columnIndex != mLastSteppedTile->columnIndex ||
			stepTile->rowIndex != mLastSteppedTile->rowIndex))
		{
			mLastSteppedTile = stepTile;
			stepTile->stepCount++;
		}
	}

	mInventory.Update(dt);
	UpdateItems(dt);
	GetComponent<LightSourceComponent>()->Update(dt);
}

void Player::Draw(float dt)
{
	Mob::Draw(dt);

	std::vector<std::unique_ptr<Item>>::iterator itemIterator = mWorldItems.begin();
	while (itemIterator != mWorldItems.end())
	{
		Item& item = **itemIterator;
		item.Draw(dt);
		itemIterator++;
	}
}

Inventory& Player::GetInventory()
{
	return mInventory;
}

bool Player::IsInventoryFull(int incoming)
{
	return mInventory.IsFull(static_cast<int>(mWorldItems.size()) + incoming);
}

void Player::PickupItem(std::unique_ptr<Item> item)
{
	mWorldItems.push_back(std::move(item));
}

void Player::SetPos(sf::Vector2f pos)
{
	Mob::SetPos(pos);
	GetComponent<LightSourceComponent>()->SetPos(pos);
}

// PRIVATE

void Player::UpdateItems(float dt)
{
	std::vector<int> itemsToDelete;
	std::vector<std::unique_ptr<Item>>::iterator itemIterator = mWorldItems.begin();
	while (itemIterator != mWorldItems.end())
	{
		Item& item = **itemIterator;
		item.Update(dt);
		// Item not yet collected, let's move it towards the player
		sf::Vector2f pos = GetComponent<DynamicTransformComponent>()->GetPos();
		sf::Vector2f itemPos = Octomorph::OctomorphMath::MoveTowards(item.GetPos(), pos, mPickupSpeed * dt);
		item.SetPos(itemPos);
		if (Octomorph::OctomorphMath::IsAround(Octomorph::OctomorphMath::DistanceBetweenPoints(item.GetPos(), pos), 0, 10))
		{
			mInventory.AddItem(item.GetType(), item.GetId(), item.GetCount());
			itemsToDelete.push_back(static_cast<int>(itemIterator - mWorldItems.begin()));
		}
		itemIterator++;
	}

	// Clean up our local mItems vector
	int deletedItems = 0;
	for (int i = 0; i < itemsToDelete.size(); i++)
	{
		// Since we are deleting based on index, we need to constantly update our
		// index offset from what itemsToDelete tells us based on how many items
		// we've deleted so far.
		int offset = itemsToDelete[i] - deletedItems;
		mWorldItems.erase(mWorldItems.begin() + offset);
		deletedItems++;
	}
}
