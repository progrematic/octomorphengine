#include <Objects/Mobs/Mob.h>

#include <GameGlobals.h>

#include <Octomorph/Managers/TextureManager.h>

#include <Octomorph/Input/Keyboard.h>
#include <Octomorph/Input/Joystick.h>

#include <Octomorph/Components/DynamicTransformComponent.h>
#include <Octomorph/Components/SpritesheetComponent.h>

using namespace Octomorph::Components;

Mob::Mob() : 
	Object(),
	mLookDirection(DIRECTION::SOUTH)
{
	AttachComponent<DynamicTransformComponent>();
	AttachComponent<SpritesheetComponent>();

	for (int i = ETOI(STAT::INVALID) + 1; i < ETOI(STAT::COUNT); i++)
	{
		mStatsList.insert(std::pair<STAT, float>(ITOE(STAT, i), 300.0f));
	}
	mStatsList.at(STAT::PICKUP_RADIUS) = 2;
}

void Mob::Initialize(std::shared_ptr<Dungeon> dungeon)
{
	mDungeon = dungeon;
}

void Mob::Update(float dt)
{
	// Calculate look direction
	sf::Vector2f dist = mLookTarget - GetComponent<DynamicTransformComponent>()->GetPos();
	if (abs(dist.x) > abs(dist.y))
	{
		if (dist.x > 0)
			mLookDirection = DIRECTION::EAST;
		else
			mLookDirection = DIRECTION::WEST;
	}
	else
	{
		if (dist.y > 0)
			mLookDirection = DIRECTION::SOUTH;
		else
			mLookDirection = DIRECTION::NORTH;
	}

	// Decide on an animation state given our velocity
	sf::Vector2f vel = GetComponent<DynamicTransformComponent>()->GetVel();
	ANIMATION_STATE animState = ITOE(ANIMATION_STATE, GetComponent<SpritesheetComponent>()->GetCurrentAnimation());
	bool isWalking = vel.x != 0 || vel.y != 0;
	animState = GameHelpers::GetAnimationForDirection(isWalking ? ANIMATION_STATE::WALK_DOWN : ANIMATION_STATE::IDLE_DOWN, mLookDirection);

	// Update animation state if necessary
	if (GetComponent<SpritesheetComponent>()->GetCurrentAnimation() != ETOI(animState))
		GetComponent<SpritesheetComponent>()->SetAnimation(ETOI(animState));

	GetComponent<DynamicTransformComponent>()->Update(dt);
	GetComponent<SpritesheetComponent>()->Update(dt);
}

void Mob::Draw(float dt)
{
	GetComponent<SpritesheetComponent>()->Draw(dt);
}

float Mob::GetStat(STAT stat)
{
	return mStatsList.at(stat);
}

sf::Vector2f Mob::GetPos()
{
	return GetComponent<DynamicTransformComponent>()->GetPos();
}

void Mob::SetPos(sf::Vector2f pos)
{
	GetComponent<DynamicTransformComponent>()->SetPos(pos);
	GetComponent<SpritesheetComponent>()->SetPos(pos);
}

void Mob::ApplyVel()
{
	sf::Vector2f vel = GetComponent<DynamicTransformComponent>()->GetVel();
	// Get our position value and cache it
	sf::Vector2f pos = GetComponent<DynamicTransformComponent>()->GetPos();
	sf::Vector2f prevPos = pos;

	// Check hor and vert collisions individually and calculate a new position based on the outcomes
	if (CheckCollision(sf::Vector2f(pos.x + vel.x, pos.y)))
		pos.x = prevPos.x;
	else
		pos.x += vel.x;

	if (CheckCollision(sf::Vector2f(pos.x, pos.y + vel.y)))
		pos.y = prevPos.y;
	else
		pos.y += vel.y;

	// We don't use DynamicTransformComponent's ApplyVel() because we are resolving collisions here
	SetPos(pos);
}

void Mob::SetLookTarget(sf::Vector2f lookTarget)
{
	mLookTarget = lookTarget;
}

sf::Vector2f Mob::ResolveCollision()
{
	sf::Vector2f pos = GetComponent<DynamicTransformComponent>()->GetPos();

	if (CheckCollision(pos))
	{
		sf::Vector2f collisionVec = pos - (mLastCollidedTile->sprite.getPosition() + sf::Vector2f(TILE_SIZE / 2, TILE_SIZE / 2));
		int xMod = pos.x < mLastCollidedTile->sprite.getPosition().x ? 1 : -1;	// Check which side we're on and use that to determine
		int yMod = pos.y < mLastCollidedTile->sprite.getPosition().y ? 1 : -1;	//   if we should add or subtract TILE_SIZE / 2 below 
		if (abs(collisionVec.x) > abs(collisionVec.y))
			pos.x += collisionVec.x + ((TILE_SIZE / 2) * xMod);
		else
			pos.y += collisionVec.y + ((TILE_SIZE / 2) * yMod);
	}

	return pos;
}

// PROTECTED

bool Mob::CheckCollision(sf::Vector2f newPos)
{
	std::shared_ptr<Tile> overlappingTiles[4];

	sf::Vector2f collisionSize = (sf::Vector2f)GetComponent<SpritesheetComponent>()->GetCollisionDimensions();

	// Check player's 4 corners to get overlapping tiles
	overlappingTiles[0] = mDungeon->GetTile(sf::Vector2f(newPos.x - collisionSize.x / 2, newPos.y - collisionSize.y / 2));
	overlappingTiles[1] = mDungeon->GetTile(sf::Vector2f(newPos.x + collisionSize.x / 2, newPos.y - collisionSize.y / 2));
	overlappingTiles[2] = mDungeon->GetTile(sf::Vector2f(newPos.x + collisionSize.x / 2, newPos.y + collisionSize.y / 2));
	overlappingTiles[3] = mDungeon->GetTile(sf::Vector2f(newPos.x - collisionSize.x / 2, newPos.y + collisionSize.y / 2));

	for (int i = 0; i < 4; i++)
	{
		if (!mDungeon->IsPassable(overlappingTiles[i]->type))
		{
			mLastCollidedTile = overlappingTiles[i];
			return true;
		}
	}

	return false;
}
