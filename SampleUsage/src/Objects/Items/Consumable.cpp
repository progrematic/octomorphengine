#include <Objects/Items/Consumable.h>

#include <GameEnums.h>

#include <Octomorph/Managers/TextureManager.h>
#include <Octomorph/IO/json.h>

Consumable::Consumable()
{
	mType = ITEM_TYPE::CONSUMABLE;
	mConsumableType = CONSUMABLE_TYPE::INVALID;
}

void Consumable::Update(float dt)
{
	Item::Update(dt);
}

void Consumable::Draw(float dt)
{
	Item::Draw(dt);
}

void Consumable::SetPos(sf::Vector2f pos)
{
	Item::SetPos(pos);
}

void Consumable::LoadItem(int id)
{
	char* pngPath = "Resources/spritesheets/items/consumables.png";
	char* jsonPath = "Resources/items/consumables.json";

	int spriteId = Octomorph::Managers::TextureManager::AddTexture(pngPath);
	mTilesheet.Load(pngPath, jsonPath);
	mTilesheet.GetTile(id, mSprite);

	mId = id;
	Json::Value data;
	std::ifstream file;
	file.open(jsonPath);

	if (file.is_open())
	{
		file >> data;

		for (Json::Value::iterator it = data["consumables"].begin(); it != data["consumables"].end(); it++)
		{
			if ((*it)["id"].asInt() == mId)
			{
				mName = (*it)["name"].asString();
				mDesc = (*it)["desc"].asString();
				mValue = (*it)["value"].asInt();
				mCost = (*it)["cost"].asInt();
				mMaxStack = (*it)["maxStack"].asInt();
				break;
			}
		}
	}
	else
	{
		std::cout << "ERROR! Could not load consumable: " << id << std::endl;
	}
}
