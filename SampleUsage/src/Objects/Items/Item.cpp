#include <Objects/Items/Item.h>

#include <GameEnums.h>

#include <Octomorph/Managers/ViewManager.h>
#include <Octomorph/Managers/TextureManager.h>
#include <Octomorph/Components/DynamicTransformComponent.h>

using namespace Octomorph::Managers;
using namespace Octomorph::Components;

Item::Item() :
	mType(ITEM_TYPE::INVALID),
	mId(0),
	mCount(1),
	mMaxStack(1),
	mValue(0),
	mCost(0),
	mT(0),
	mAmplitude(10),
	mShouldBounce(false),
	mShouldShake(false)
{
	int spriteId = TextureManager::AddTexture("Resources/notfound.png");
	mSprite.setTexture(*TextureManager::GetTexture(spriteId));

	AttachComponent<DynamicTransformComponent>();
}

void Item::Update(float dt)
{
	GetComponent<DynamicTransformComponent>()->Update(dt);
	GetComponent<DynamicTransformComponent>()->ApplyVel(sf::Vector2f(0.5f, 0.5f));
}

void Item::Draw(float dt)
{
	if (mShouldBounce || mShouldShake)
	{
		mT += dt;
		mDisplayPos = GetComponent<DynamicTransformComponent>()->GetPos();
		if (mShouldBounce)
			mDisplayPos.y -= fabs(sinf(mT) * mAmplitude);
		if (mShouldShake)
			mDisplayPos.x -= sinf(mT * 50) * mAmplitude / 10;
		mSprite.setPosition(mDisplayPos);
	}
	ViewManager::GetInstance()->GetWindow().draw(mSprite);
}

sf::Vector2f Item::GetPos()
{
	return GetComponent<DynamicTransformComponent>()->GetPos();
}

void Item::SetPos(sf::Vector2f pos)
{
	GetComponent<DynamicTransformComponent>()->SetPos(pos);
	mSprite.setPosition(pos);
}

int Item::GetId()
{
	return mId;
}

ITEM_TYPE Item::GetType()
{
	return mType;
}

void Item::SetType(ITEM_TYPE type)
{
	mType = type;
}

int Item::GetValue()
{
	return mValue;
}

void Item::SetValue(int value)
{
	mValue = value;
}

int Item::GetCost()
{
	return mCost;
}

void Item::SetCost(int cost)
{
	mCost = cost;
}

int Item::GetCount()
{
	return mCount;
}

int Item::SetCount(int count)
{
	int ret = 0;
	mCount = count;
	if (mCount > mMaxStack)
	{
		ret = mCount - mMaxStack;
		mCount = mMaxStack;
	}
	return ret;
}

int Item::GetMaxStack()
{
	return mMaxStack;
}

void Item::SetMaxStack(int stack)
{
	mMaxStack = stack;
}

std::string Item::GetName()
{
	return mName;
}

std::string Item::GetDesc()
{
	return mDesc;
}

void Item::SetBounce(bool bounce)
{
	mShouldBounce = bounce;
	if (!mShouldBounce && !mShouldShake)
		mSprite.setPosition(GetComponent<DynamicTransformComponent>()->GetPos());
}

void Item::SetShake(bool shake)
{
	mShouldShake = shake;
}
