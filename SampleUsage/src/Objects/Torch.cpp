#include "Objects/Torch.h"

#include <GameEnums.h>

#include "Octomorph/Managers/TextureManager.h"

#include "Octomorph/Components/LightSourceComponent.h"
#include "Octomorph/Components/SpritesheetComponent.h"

using namespace Octomorph::Managers;
using namespace Octomorph::Components;

Torch::Torch()
{
	mTorchTextureId = TextureManager::AddTexture("Resources/spritesheets/objects/torch.png");
	mParticleTextureId = TextureManager::AddTexture("Resources/spr_particle.png");

	AttachComponent<SpritesheetComponent>();
	GetComponent<SpritesheetComponent>()->LoadSpritesheet("Resources/spritesheets/objects/torch.json");
	GetComponent<SpritesheetComponent>()->SetSprite(*TextureManager::GetTexture(mTorchTextureId));
	GetComponent<SpritesheetComponent>()->SetAnimation(0);

	AttachComponent<LightSourceComponent>();
	GetComponent<LightSourceComponent>()->SetRadius(50);
	GetComponent<LightSourceComponent>()->SetWaning(100);
	//GetComponent<LightSourceComponent>()->SetColor(sf::Color(255, 255, 0, 0));
	GetComponent<LightSourceComponent>()->SetFlicker(true);

	mParticleEmitter.Initialize(4, 5, 0, -1, 10, 0, 0, 3, -0.1f);
	std::unique_ptr<sf::Sprite> particleSprite = std::make_unique<sf::Sprite>();
	particleSprite->setTexture(*TextureManager::GetTexture(mParticleTextureId));
	particleSprite->setScale(0.1f, 0.1f);
	particleSprite->setColor(sf::Color(227, 140, 45, 150));
	mParticleEmitter.SetParticleSprite(std::move(particleSprite));

	mParticleEmitter.Emit();
}

void Torch::Update(float dt)
{
	GetComponent<LightSourceComponent>()->Update(dt);
	mParticleEmitter.Update(dt);
}

void Torch::Draw(float dt)
{
	GetComponent<SpritesheetComponent>()->Draw(dt);
	mParticleEmitter.Draw(dt);
}

sf::Vector2f Torch::GetPos()
{
	return mPos;
}

void Torch::SetPos(sf::Vector2f pos)
{
	mPos = pos;
	GetComponent<SpritesheetComponent>()->SetPos(mPos);
	GetComponent<LightSourceComponent>()->SetPos(mPos);
	mParticleEmitter.SetPos(mPos + sf::Vector2f(-2, -6));
}