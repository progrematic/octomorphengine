#include <Managers/ItemManager.h>

#include <Octomorph/Input/Keyboard.h>
#include <Octomorph/Components/DynamicTransformComponent.h>

#include <Objects/Items/Consumable.h>

ItemManager* ItemManager::mInstance = nullptr;

ItemManager::ItemManager()
{
	if (mInstance != nullptr)
	{
		std::cout << "ERROR: Attempting to initialize a second ItemManager!" << std::endl;
		return;
	}
}

ItemManager* ItemManager::GetInstance()
{
	if (!mInstance)
		mInstance = new ItemManager();

	return mInstance;
}

void ItemManager::Reset()
{
	mItems.clear();
}

void ItemManager::Update(float dt)
{
	std::vector<std::unique_ptr<Item>>::iterator itemIterator = mItems.begin();
	while (itemIterator != mItems.end())
	{
		Item& item = **itemIterator;
		item.Update(dt);
		item.SetShake(false);
		itemIterator++;
	}
}

void ItemManager::Draw(float dt)
{
	std::vector<std::unique_ptr<Item>>::iterator itemIterator = mItems.begin();
	while (itemIterator != mItems.end())
	{
		Item& item = **itemIterator;
		item.Draw(dt);
		itemIterator++;
	}
}

void ItemManager::PlayerPickup(Player* player)
{
	std::vector<int> pickups;
	std::vector<std::unique_ptr<Item>>::iterator itemIterator = mItems.begin();
	while (itemIterator != mItems.end())
	{
		Item& item = **itemIterator;
		if (!player->IsInventoryFull(static_cast<int>(pickups.size())))
		{
			float shakeOffset = 1 * TILE_SIZE;	// Offset is always 1 tile away from pickup radius
			float dist = Octomorph::OctomorphMath::DistanceBetweenPoints(player->GetPos(), item.GetPos());
			if (dist < player->GetStat(STAT::PICKUP_RADIUS) * TILE_SIZE)
			{
				pickups.push_back(static_cast<int>(itemIterator - mItems.begin()));
			}
			else if (dist < player->GetStat(STAT::PICKUP_RADIUS) * TILE_SIZE + shakeOffset)
			{
				item.SetShake(true);
			}
		}
		itemIterator++;
	}

	// Resolve the pickups (since we have to std::move, we can't do this in the loop above)
	// itemsToDelete - Used to ensure we don't delete all items within pickup range
	// if we run out of inventory space halfway through the stack
	std::vector<int> itemsToDelete;
	for (int i = 0; i < pickups.size(); i++)
	{
		if (!player->GetInventory().IsFull())
		{
			int index = pickups[i];
			// Player::PickupItem takes over the item itself, so we much make sure that we don't
			// attempt to reference mItems.at(index) after this call!
			player->PickupItem(std::move(mItems.at(index)));
			itemsToDelete.push_back(index);
		}
	}

	// Clean up our local mItems vector
	int deletedItems = 0;
	for (int i = 0; i < itemsToDelete.size(); i++)
	{
		// Since we are deleting based on index, we need to constantly update our
		// index offset from what itemsToDelete tells us based on how many items
		// we've deleted so far.
		int offset = itemsToDelete[i] - deletedItems;
		mItems.erase(mItems.begin() + offset);
		deletedItems++;
	}
}

const std::vector<std::unique_ptr<Item>>& ItemManager::GetItems()
{
	return mItems;
}

std::unique_ptr<Item> ItemManager::CreateItem(ITEM_TYPE type, int id, int count)
{
	std::unique_ptr<Item> item = nullptr;

	switch (type)
	{
	case ITEM_TYPE::CONSUMABLE:
	{
		item = std::make_unique<Consumable>();
	}
	break;
	}

	item->LoadItem(id);
	item->SetCount(count);

	return item;
}

void ItemManager::SpawnItem(ITEM_TYPE type, int id, int count, sf::Vector2f pos)
{
	SpawnItem(type, id, count, pos, sf::Vector2f(0, 0));
}

void ItemManager::SpawnItem(ITEM_TYPE type, int id, int count, sf::Vector2f pos, sf::Vector2f force)
{
	std::unique_ptr<Item> item = CreateItem(type, id, count);

	item->SetPos(pos);
	item->SetBounce(true);

	item->GetComponent<Octomorph::Components::DynamicTransformComponent>()->AddForce(force);
	mItems.push_back(std::move(item));
}