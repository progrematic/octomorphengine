#include <Managers/TrialManager.h>

TrialManager* TrialManager::mInstance = nullptr;

TrialManager::TrialManager() : IMessageHandler(ETOI(MESSAGE_TYPE::COUNT))
{
	if (mInstance != nullptr)
	{
		std::cout << "ERROR: Attempting to initialize a second TrialManager!" << std::endl;
		return;
	}
}

TrialManager::~TrialManager()
{
	IMessageHandler::Disable();
}

TrialManager* TrialManager::GetInstance()
{
	if (mInstance == nullptr)
	{
		mInstance = new TrialManager();
	}

	return mInstance;
}

void TrialManager::Initialize()
{

}

void TrialManager::Reset()
{
	mTrialRooms.clear();
	IMessageHandler::Enable();
	IMessageHandler::Subscribe(ETOI(MESSAGE_TYPE::REQUEST_TRIAL_END));
}

void TrialManager::AddTrialRoom(std::shared_ptr<TrialRoom> trialRoom)
{
	mTrialRooms.push_back(trialRoom);
}

bool TrialManager::IsTrialActive()
{
	for (std::shared_ptr<TrialRoom> room : mTrialRooms)
	{
		if (room->IsTrialActive())
			return true;
	}

	return false;
}

bool TrialManager::HandleMessage(const Octomorph::Messages::Message& msg)
{
	if (msg.GetType() == ETOI(MESSAGE_TYPE::REQUEST_TRIAL_END))
	{
		for (std::shared_ptr<TrialRoom> room : mTrialRooms)
		{
			if (room->IsTrialActive())
			{
				room->EndTrial();
			}
		}

		return true;
	}

	return false;
}
