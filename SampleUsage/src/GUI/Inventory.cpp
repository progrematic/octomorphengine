#include <GUI/Inventory.h>
#include <Octomorph/Input/Mouse.h>
#include <Managers/ItemManager.h>
#include <Octomorph/Managers/ViewManager.h>
#include <Octomorph/Managers/TextureManager.h>
#include <Objects/Items/Consumable.h>

#include <sstream>

using namespace Octomorph::Managers;
using namespace Octomorph::Input;

Inventory::Inventory() {}

void Inventory::Initialize()
{
	mItemBoxSize = 36;
	mItemSize = 32;
	mItemColumns = 5;
	mItemRows = 4;

	mDropForce = 50.f;
	mDragging = false;

	Reset();
}

void Inventory::Reset()
{
	mShown = false;
	mSelectedBox = nullptr;
	mEmptyBox = nullptr;
	mWorldPos = sf::Vector2f(0, 0);
	mItemBoxes.clear();
	BuildGUI();
}

void Inventory::Update(float dt)
{
	if (mShown)
	{
		mGui.Update(dt);

		float mouseX = static_cast<float>(Mouse::GetMouseX());
		float mouseY = static_cast<float>(Mouse::GetMouseY());

		if (mDragging)
		{
			mGui.SetPos(sf::Vector2f(mouseX, mouseY) - mStartDragOffset);

			mItemName->SetString("");
			mItemDesc->SetString("");
			for (std::shared_ptr<ItemBox> box : mItemBoxes)
			{
				box->button->GetLabel().SetPos(box->button->GetPos() + sf::Vector2f(mItemSize, mItemSize));
				if (box->item != nullptr && box->item->GetCount() > 1)
				{
					std::stringstream ss;
					ss << box->item->GetCount();
					box->button->GetLabel().SetString(ss.str());
				}
				else
				{
					box->button->GetLabel().SetString("");
				}
			}

			if (Mouse::ButtonUp(sf::Mouse::Left))
			{
				mDragging = false;
			}
		}
		else
		{
			std::shared_ptr<ItemBox> mouseBox = nullptr;

			mItemName->SetString("");
			mItemDesc->SetString("");
			bool foundMouseBox = false;
			for (std::shared_ptr<ItemBox> box : mItemBoxes)
			{
				if (!foundMouseBox)
				{
					if (Octomorph::OctomorphMath::IsPointInRect(sf::Vector2f(mouseX, mouseY), sf::FloatRect(box->button->GetPos() + mGui.GetPos(), box->button->GetSize())))
					{
						mouseBox = box;

						if (box->item != nullptr)
						{
							box->item->Update(dt);
							mItemName->SetString(box->item->GetName());
							mItemDesc->SetString(box->item->GetDesc());
						}
						foundMouseBox = true;
					}
				}

				box->button->GetLabel().SetPos(box->button->GetPos() + sf::Vector2f(mItemSize, mItemSize));
				if (box->item != nullptr && box->item->GetCount() > 1)
				{
					std::stringstream ss;
					ss << box->item->GetCount();
					box->button->GetLabel().SetString(ss.str());
				}
				else
				{
					box->button->GetLabel().SetString("");
				}
			}

			if (mSelectedBox != nullptr)
			{
				mMouseItem->Update(dt);
				mMouseItem->SetPos(sf::Vector2f(mouseX, mouseY));

				if (Mouse::ButtonUp(sf::Mouse::Left))
				{
					if (mouseBox == nullptr)
					{
						// Dropping the item outside of the inventory.. drop the item!
						sf::Vector2f worldMouse = ViewManager::GetInstance()->GetWindow().mapPixelToCoords(sf::Vector2i(static_cast<int>(floor(mouseX)), static_cast<int>(floor(mouseY))));
						float forceVal = (Octomorph::OctomorphRandom::GetInt(0, 50) + 50) / 100.f;
						sf::Vector2f force = Octomorph::OctomorphMath::Normalize(worldMouse - mWorldPos, mDropForce * forceVal);
						ItemManager::GetInstance()->SpawnItem(mSelectedBox->item->GetType(), mSelectedBox->item->GetId(), mSelectedBox->item->GetCount(), mWorldPos, force);

						mFreeSpace++;
						mSelectedBox->item = nullptr;
						UpdateEmptyBox();
					}
					else if (mouseBox->column == mSelectedBox->column && mouseBox->row == mSelectedBox->row)
					{
						// Dropping the item back at it's own box.. nothing happens!
					}
					else if (mouseBox != nullptr)
					{
						// Dropping the item at another box..
						if (mouseBox->item == nullptr)
						{
							// Simply move the item
							mouseBox->item = std::move(ItemManager::GetInstance()->CreateItem(mSelectedBox->item->GetType(), mSelectedBox->item->GetId(), mSelectedBox->item->GetCount()));
							mSelectedBox->item = nullptr;
							UpdateEmptyBox();
						}
						else
						{
							// Swap the items
							std::shared_ptr<Item> tmp = mouseBox->item;
							mouseBox->item = std::move(ItemManager::GetInstance()->CreateItem(mSelectedBox->item->GetType(), mSelectedBox->item->GetId(), mSelectedBox->item->GetCount()));
							mSelectedBox->item = std::move(ItemManager::GetInstance()->CreateItem(tmp->GetType(), tmp->GetId(), tmp->GetCount()));
						}
					}
					mSelectedBox = nullptr;
					mMouseItem = nullptr;
				}
			}
		}
	}
}

void Inventory::Draw(float dt)
{
	if (mShown)
	{
		mGui.Draw(dt);

		for (std::shared_ptr<ItemBox> box : mItemBoxes)
		{
			if (box->item != nullptr)
			{
				box->item->SetPos(box->button->GetPos() + mGui.GetPos());
				box->item->Draw(dt);
				box->button->GetLabel().Draw(dt, mGui.GetPos());	// Manually redraw the button label so count text is above item
				box->item->SetPos(box->button->GetPos() - mGui.GetPos());
			}
		}

		if (mMouseItem != nullptr)
		{
			mMouseItem->Draw(dt);
		}
	}
}

void Inventory::Show()
{
	mShown = true;
}

void Inventory::Hide()
{
	mShown = false;
}

void Inventory::ToggleShown()
{
	mShown = !mShown;
	if (mShown)
		mGui.Enable();
	else
		mGui.Disable();
}

void Inventory::SetWorldPos(sf::Vector2f worldPos)
{
	mWorldPos = worldPos;
}

bool Inventory::IsFull()
{
	return mFreeSpace == 0;
}

bool Inventory::IsFull(int extras)
{
	return (mItemLimit - mFreeSpace) + extras >= mItemLimit;
}

void Inventory::AddItem(ITEM_TYPE type, int id, int count)
{
	bool stacked = false;
	int leftover = count;
	if (type == ITEM_TYPE::CONSUMABLE)
	{
		// Item is stackable, let's find a stack
		for (std::shared_ptr<ItemBox> box : mItemBoxes)
		{
			if (box->item != nullptr)
			{
				if (box->item->GetType() == type && box->item->GetId() == id)
				{
					// Same item in this box.. have we reached the max?
					if (box->item->GetCount() < box->item->GetMaxStack())
					{
						// We can stack! So no need to do anything else
						leftover = box->item->SetCount(box->item->GetCount() + count);
						if (leftover == 0)
							stacked = true;
						break;
					}
				}
			}
		}
	}

	if (!stacked)
	{
		// We need to use up a new spot
		// Do we have one?
		while (leftover > 0)
		{
			if (IsFull())
			{
				// We have no more space, so we should drop the remaining items we couldn't pick up
				sf::Vector2f force;
				force.x = Octomorph::OctomorphRandom::GetInt(-mDropForce, mDropForce);
				force.y = Octomorph::OctomorphRandom::GetInt(-mDropForce, mDropForce);
				ItemManager::GetInstance()->SpawnItem(type, id, leftover, mWorldPos, force);
				break;
			}

			std::shared_ptr<Item> item = std::move(ItemManager::GetInstance()->CreateItem(type, id));
			leftover = item->SetCount(leftover);
			mEmptyBox->item = item;
			mFreeSpace--;
			UpdateEmptyBox();
		}
	}
}

// PRIVATE

void Inventory::SelectBox(void* arg)
{
	if (arg != nullptr)
	{
		mSelectedBox = nullptr;
		mMouseItem = nullptr;
		ItemBox* boxArg = static_cast<ItemBox*>(arg);
		if (boxArg->item != nullptr)
		{
			for (std::shared_ptr<ItemBox> box : mItemBoxes)
			{
				if (box.get() == arg)
				{
					mSelectedBox = box;
					mMouseItem = std::move(ItemManager::GetInstance()->CreateItem(box->item->GetType(), box->item->GetId(), box->item->GetCount()));
					break;
				}
			}
		}
	}
}

void Inventory::UpdateEmptyBox()
{
	mEmptyBox = nullptr;
	for (std::shared_ptr<ItemBox> box : mItemBoxes)
	{
		if (box->item == nullptr)
		{
			mEmptyBox = box;
			break;
		}
	}
}

void Inventory::BuildGUI()
{
	mGui.Reset();
	sf::Vector2u windowSize = ViewManager::GetInstance()->GetWindow().getSize();
	mGui.SetBorderSize(sf::Vector2f(25, 25));
	mGui.SetColor(sf::Color::Transparent);
	mGui.SetPos(sf::Vector2f(windowSize.x * 0.9f, windowSize.y * 0.05f));

	// Background button used for moving - set pos and size after all other widgets are created
	mBgButton = std::make_shared<Button>();
	sf::Color bgColor = sf::Color::Magenta;
	bgColor.a = 50;
	mBgButton->SetColorForState(bgColor, Button::BUTTON_STATE::NORMAL);
	mBgButton->SetColorForState(bgColor, Button::BUTTON_STATE::HOVER);
	mBgButton->SetColorForState(bgColor, Button::BUTTON_STATE::PRESSED);
	mBgButton->SetCallback(std::bind(&Inventory::Drag, this));
	mBgButton->GetLabel().SetString("");
	mGui.AddWidget(mBgButton);

	std::shared_ptr<TextLabel> titleLabel = std::make_shared<TextLabel>();
	titleLabel->SetAnchor(sf::Vector2f(0.5f, 0.5f));
	titleLabel->SetString("INVENTORY");
	mGui.AddWidget(titleLabel);

	float padding = 5;
	mItemLimit = mItemColumns * mItemRows;
	mFreeSpace = mItemLimit;

	sf::Vector2f itemOffset = sf::Vector2f((mItemBoxSize * mItemColumns + (padding * (mItemColumns - 1))) / 2.f,
											-(titleLabel->GetFontSize() + padding));

	int itemboxNormalSpriteId = TextureManager::AddTexture("Resources/ui/ItemBox.png");
	int itemboxHoverSpriteId = TextureManager::AddTexture("Resources/ui/ItemBoxHover.png");
	int itemboxPressedSpriteId = TextureManager::AddTexture("Resources/ui/ItemBoxPressed.png");

	for (int j = 0; j < mItemRows; j++)
	{
		for (int i = 0; i < mItemColumns; i++)
		{
			std::shared_ptr<ItemBox> box = std::make_shared<ItemBox>();

			std::shared_ptr<Button> itemButton = std::make_shared<Button>();
			itemButton->SetAnchor(sf::Vector2f(0, 0));
			itemButton->SetSize(sf::Vector2f(static_cast<float>(mItemBoxSize), static_cast<float>(mItemBoxSize)));
			itemButton->SetPos(-itemOffset + sf::Vector2f(((mItemBoxSize * i) + (padding * i)) + (padding / 2), ((mItemBoxSize * j) + (padding * j) - padding)));
			itemButton->SetImageForState(*TextureManager::GetTexture(itemboxNormalSpriteId), Button::BUTTON_STATE::NORMAL);
			itemButton->SetImageForState(*TextureManager::GetTexture(itemboxHoverSpriteId), Button::BUTTON_STATE::HOVER);
			itemButton->SetImageForState(*TextureManager::GetTexture(itemboxPressedSpriteId), Button::BUTTON_STATE::PRESSED);
			itemButton->GetLabel().SetAnchor(sf::Vector2f(1, 1));
			itemButton->GetLabel().SetString("");
			itemButton->GetLabel().SetFontSize(18);
			itemButton->GetLabel().SetColor(sf::Color::White);
			itemButton->SetCallback(std::bind(&Inventory::SelectBox, this, static_cast<void*>(box.get())));
			mGui.AddWidget(itemButton);

			box->column = i;
			box->row = j;
			box->button = itemButton;
			box->item = nullptr;
			mItemBoxes.push_back(box);
			if (mEmptyBox == nullptr)
				mEmptyBox = box;
		}
	}

	mItemName = std::make_shared<TextLabel>();
	mItemName->SetAnchor(sf::Vector2f(0.5f, 0.5f));
	mItemName->SetPos(sf::Vector2f(0, -itemOffset.y + ((mItemBoxSize * mItemRows) + (padding * mItemRows)) + padding));
	mItemName->SetString("");
	mGui.AddWidget(mItemName);

	mItemDesc = std::make_shared<TextLabel>();
	mItemDesc->SetAnchor(sf::Vector2f(0.5f, 0.5f));
	mItemDesc->SetPos(sf::Vector2f(0, mItemName->GetPos().y + titleLabel->GetFontSize() + (padding * 1.5f)));
	mItemDesc->SetString("");
	mGui.AddWidget(mItemDesc);

	// Setting background button pos and size now that we have all widgets added
	sf::FloatRect rc = mGui.GetRect();
	mBgButton->SetPos(sf::Vector2f(rc.left, rc.top) - mGui.GetPos());
	mBgButton->SetSize(sf::Vector2f(rc.width, rc.height));
}

void Inventory::Drag()
{
	mDragging = true;
	mStartDragOffset = sf::Vector2f(static_cast<float>(Mouse::GetMouseX()), static_cast<float>(Mouse::GetMouseY())) - mGui.GetPos();
}
