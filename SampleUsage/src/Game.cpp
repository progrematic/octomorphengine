#include <Octomorph/Headers.h>
#include <Game.h>
#include <Octomorph/Managers/EventManager.h>
#include <Managers/ItemManager.h>
#include <Managers/TrialManager.h>
#include <Octomorph/Managers/TextureManager.h>
#include <Octomorph/Managers/TimeManager.h>
#include <Octomorph/Managers/SwarmManager.h>
#include <Octomorph/Managers/ViewManager.h>
#include <Objects/Torch.h>
#include <Objects/Items/Consumable.h>
#include <Octomorph/Components/DynamicTransformComponent.h>
#include <Octomorph/Components/InputComponent.h>
#include <Octomorph/Components/SpriteComponent.h>
#include <Octomorph/Input/Mouse.h>
#include <Octomorph/Input/Keyboard.h>
#include <Dungeon/Features/Rooms/TrialRoom.h>

#include <Debugging/Console.h>

using namespace Octomorph::Managers;
using namespace Octomorph::Components;

Game::Game() {}

void Game::Initialize()
{
	mDungeon = std::make_shared<Dungeon>();

	LightManager::GetInstance()->Initialize();
	TrialManager::GetInstance()->Initialize();
	Console::GetInstance()->Initialize(this, mDungeon);
}

void Game::Reset()
{
	mObjects.clear();
	mPlayers.clear();

	TrialManager::GetInstance()->Reset();
	ItemManager::GetInstance()->Reset();

	mDungeon->Generate(75, 45, 50);
	LightManager::GetInstance()->Reset(sf::Vector2f(mDungeon->GetWidth(), mDungeon->GetHeight()), TILE_SIZE);

	//mSwarm = SwarmManager(mDungeon, mPlayer->GetPos(), 10);
	mDrawLight = true;
	mGameOver = false;
	mState = GAME_STATE::PLAYER_SELECT;

	ViewManager::GetInstance()->UpdatePlayerCount(0);
	std::shared_ptr<Room> spawnRoom = mDungeon->GetSpawnRoom();
	sf::Vector2f spawnPos;
	spawnPos.x = (spawnRoom->GetRect().left * TILE_SIZE) + ((float)(spawnRoom->GetRect().width - 1) / 2.f * TILE_SIZE) + (TILE_SIZE / 2);
	spawnPos.y = (spawnRoom->GetRect().top * TILE_SIZE) + ((float)(spawnRoom->GetRect().height - 1) / 2.f * TILE_SIZE) + (TILE_SIZE / 2);
	ViewManager::GetInstance()->SetGameViewpoint(VIEW::PLAYER1, spawnPos);
	HandleRoomEntry(spawnRoom);
}

void Game::Cleanup()
{
	LightManager::GetInstance()->Cleanup();
}

void Game::GameOver()
{
	mGameOver = true;
	Cleanup();
}

bool Game::IsGameOver()
{
	return mGameOver;
}

void Game::Update(float dt)
{
	mDungeon->Update(dt);

	switch (mState)
	{
	case GAME_STATE::PLAYER_SELECT:
	{
		INPUT_TYPE input = INPUT_TYPE::COUNT;
		int joystickId = -1;

		if (Keyboard::KeyDown(sf::Keyboard::Return))
		{
			input = INPUT_TYPE::KEYBOARD;
		}

		for (int i = 0; i < Octomorph::MAX_PLAYERS; i++)
		{
			if (Joystick::GetButtonDown(i, Joystick::Button::Start))
			{
				input = INPUT_TYPE::CONTROLLER;
				joystickId = i;
				break;
			}
		}

		bool inputMatch = false;
		for (std::shared_ptr<Player> player : mPlayers)
		{
			// Check if a player is already assigned to this input method
			// If so, we just want to start playing
			if (player->GetComponent<InputComponent>()->GetInputType() == input)
			{
				// If it's joystick, we need a further check
				if (input == INPUT_TYPE::CONTROLLER)
				{
					if (player->GetComponent<InputComponent>()->GetJoystickId() == joystickId)
					{
						// Joystick, and the IDs match, so let's play!
						inputMatch = true;
						break;
					}
				}
				else
				{
					// Not joystick, and we already have a keyboard player, so let's play!
					inputMatch = true;
					break;
				}
			}
		}

		if (inputMatch)
		{
			mState = GAME_STATE::GAMEPLAY;
		}
		else if (input != INPUT_TYPE::COUNT)
		{
			// No player with this input, so let's create a new player!
			SpawnPlayer(input, joystickId);
		}
	}
	break;
	case GAME_STATE::GAMEPLAY:
	{
		TimeManager::GetInstance()->StartTimer("main");
		if (!Console::GetInstance()->GetActive())
			HandleInput(dt);

		//mSwarm.Update(dt);

		for (std::shared_ptr<Player> player : mPlayers)
		{
			player->Update(dt);

			std::shared_ptr<Tile> playerTile = mDungeon->GetTile(player->GetPos());
			std::shared_ptr<Room> playerRoom = mDungeon->IsRoom(playerTile);
			if (playerRoom != nullptr && !playerRoom->GetDiscovered())
				HandleRoomEntry(playerRoom);

			ViewManager::GetInstance()->SetGameViewpointTarget(ITOE(VIEW, ETOI(VIEW::PLAYER1) + player->GetId()), player->GetPos());
		}
	}
	break;
	case GAME_STATE::PAUSE:
	{

	}
	break;
	}

	UpdateObjects(dt);

	// Update Managers
	ItemManager::GetInstance()->Update(dt);
	ViewManager::GetInstance()->Update(dt);
	Console::GetInstance()->Update(dt);

	TimeManager::GetInstance()->StopTimer("main");
}

void Game::Draw(float dt)
{
	ViewManager::GetInstance()->GetWindow().clear(sf::Color(0, 0, 0, 225));

	if (mPlayers.size() == 0)
	{
		// No players joined yet, but we still want to see the spawn room..
		ViewManager::GetInstance()->SetView(VIEW::PLAYER1);
		mDungeon->Draw(dt);

		std::vector<std::unique_ptr<Object>>::iterator objIterator = mObjects.begin();
		while (objIterator != mObjects.end())
		{
			Object& object = **objIterator;
			object.Draw(dt);
			objIterator++;
		}

		if (mDrawLight)
			LightManager::GetInstance()->Draw(dt);
	}

	for (int i = 0; i < mPlayers.size(); i++)
	{
		ViewManager::GetInstance()->SetView(ITOE(VIEW, ETOI(VIEW::PLAYER1) + i));
		mDungeon->Draw(dt);

		std::vector<std::unique_ptr<Object>>::iterator objIterator = mObjects.begin();
		while (objIterator != mObjects.end())
		{
			Object& object = **objIterator;
			object.Draw(dt);
			objIterator++;
		}

		ItemManager::GetInstance()->Draw(dt);

		for (std::shared_ptr<Player> player : mPlayers)
			player->Draw(dt);

		//mSwarm.Draw(dt);

		if (mDrawLight)
			LightManager::GetInstance()->Draw(dt);

		switch (mState)
		{
		case GAME_STATE::PLAYER_SELECT:
		{

		}
		break;
		case GAME_STATE::GAMEPLAY:
		{
		}
		break;
		case GAME_STATE::PAUSE:
		{

		}
		break;
		}
	}

	// Draw GUI
	ViewManager::GetInstance()->SetView(VIEW::UI);

	for (std::shared_ptr<Player> player : mPlayers)
		player->GetInventory().Draw(dt);

	Console::GetInstance()->Draw(dt);

	ViewManager::GetInstance()->SetView(VIEW::PLAYER1);

	ViewManager::GetInstance()->GetWindow().display();
}

void Game::HandleInput(float dt)
{
	if (!EventManager::GetInstance()->ConsumedThisFrame())
	{
		for (std::shared_ptr<Player> player : mPlayers)
			player->HandleInput(dt);

		sf::Vector2f worldMouse = ViewManager::GetInstance()->GetWindow().mapPixelToCoords(sf::Vector2i(Mouse::GetMouseX(), Mouse::GetMouseY()));
		mSwarm.SetPos(worldMouse);

		Console::GetInstance()->HandleInput(dt);
	}
}

void Game::HandleRoomEntry(std::shared_ptr<Room> room)
{
	if (!room->GetDiscovered())
	{
		room->SetDiscovered(true);

		std::shared_ptr<Tile> tlTile = mDungeon->GetTile(room->GetRect().left - 1, room->GetRect().top - 1);
		SpawnTorch(tlTile->sprite.getPosition() + sf::Vector2f(TILE_SIZE / 2, TILE_SIZE / 2));
		std::shared_ptr<Tile> trTile = mDungeon->GetTile(room->GetRect().left + room->GetRect().width, room->GetRect().top - 1);
		SpawnTorch(trTile->sprite.getPosition() + sf::Vector2f(TILE_SIZE / 2, TILE_SIZE / 2));
		std::shared_ptr<Tile> blTile = mDungeon->GetTile(room->GetRect().left - 1, room->GetRect().top + room->GetRect().height);
		SpawnTorch(blTile->sprite.getPosition() + sf::Vector2f(TILE_SIZE / 2, TILE_SIZE / 2));
		std::shared_ptr<Tile> brTile = mDungeon->GetTile(room->GetRect().left + room->GetRect().width, room->GetRect().top + room->GetRect().height);
		SpawnTorch(brTile->sprite.getPosition() + sf::Vector2f(TILE_SIZE / 2, TILE_SIZE / 2));
		LightManager::GetInstance()->GetInstance()->LightRect(room->GetRect());

		switch (room->GetType())
		{
		case ROOM_TYPE::TRIAL:
			((TrialRoom*)room.get())->StartTrial();
			break;
		}
	}
}

void Game::UpdateObjects(float dt)
{
	std::vector<std::unique_ptr<Object>>::iterator objIterator = mObjects.begin();
	while (objIterator != mObjects.end())
	{
		Object& object = **objIterator;
		object.Update(dt);
		objIterator++;
	}
}

bool Game::ToggleLight()
{
	mDrawLight = !mDrawLight;
	return mDrawLight;
}

void Game::SpawnPlayer(INPUT_TYPE input, int joystickId)
{
	std::shared_ptr<Player> player = std::make_shared<Player>();
	
	player->Initialize(mDungeon);
	player->Reset();
	player->SetId(mPlayers.size());

	player->GetComponent<InputComponent>()->SetInputType(input);
	player->GetComponent<InputComponent>()->SetJoystickId(joystickId);

	std::shared_ptr<Room> spawnRoom = mDungeon->GetSpawnRoom();
	sf::Vector2f playerPos;
	playerPos.x = (spawnRoom->GetRect().left * TILE_SIZE) + ((float)(spawnRoom->GetRect().width - 1) / 2.f * TILE_SIZE) + (TILE_SIZE / 2);
	playerPos.y = (spawnRoom->GetRect().top * TILE_SIZE) + ((float)(spawnRoom->GetRect().height - 1) / 2.f * TILE_SIZE) + (TILE_SIZE / 2);

	player->SetPos(playerPos);
	LightManager::GetInstance()->AddLightSource(player->GetComponent<LightSourceComponent>());

	mPlayers.push_back(player);
	ViewManager::GetInstance()->UpdatePlayerCount(mPlayers.size());
	ViewManager::GetInstance()->SetGameViewpoint(ITOE(VIEW, ETOI(VIEW::PLAYER1) + player->GetId()), playerPos);
}

void Game::SpawnTorch(sf::Vector2f pos)
{
	std::unique_ptr<Torch> torch = std::make_unique<Torch>();
	std::shared_ptr<Tile> tile = mDungeon->GetTile(pos);
	torch->SetPos(tile->sprite.getPosition() + sf::Vector2f(TILE_SIZE / 2, TILE_SIZE / 2));
	LightManager::GetInstance()->AddLightSource(torch->GetComponent<LightSourceComponent>());
	mObjects.push_back(std::move(torch));
}
