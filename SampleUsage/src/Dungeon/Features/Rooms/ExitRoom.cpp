#include <Dungeon/Features/Rooms/ExitRoom.h>

#include <GameEnums.h>
#include <Octomorph/Managers/ViewManager.h>

ExitRoom::ExitRoom()
{
	mType = ROOM_TYPE::EXIT;
	mTime = 0;
	mHazeColor = sf::Color::Red;
	mLocked = true;

	mHazeShader.loadFromFile("Resources/Shaders/Haze.frag", sf::Shader::Fragment);
}

void ExitRoom::CopyFromRoom(std::shared_ptr<Room> other)
{
	Room::CopyFromRoom(other);
	SetRect(mRect);
}

void ExitRoom::SetRect(sf::IntRect rect)
{
	Room::SetRect(rect);
	mHazeTexture.create(static_cast<unsigned int>(rect.width) * TILE_SIZE, static_cast<unsigned int>(rect.height) * TILE_SIZE);
	mHazeSprite.setTexture(mHazeTexture.getTexture());
	mHazeSprite.setPosition(sf::Vector2f(rect.left * TILE_SIZE, rect.top * TILE_SIZE));
}

void ExitRoom::Update(float dt)
{
	mTime += dt;
	mHazeShader.setUniform("color", sf::Glsl::Vec4(mHazeColor));
	mHazeShader.setUniform("size", sf::Glsl::Vec2(mRect.width * TILE_SIZE, mRect.height * TILE_SIZE));
	mHazeShader.setUniform("time", mTime);
	mHazeShader.setUniform("fade", static_cast<float>(TILE_SIZE));
}

void ExitRoom::Draw(float dt)
{
	if (mLocked)
	{
		mHazeTexture.clear(sf::Color::Transparent);
		mHazeTexture.draw(sf::Sprite(mHazeTexture.getTexture()), &mHazeShader);
		mHazeTexture.display();

		Octomorph::Managers::ViewManager::GetInstance()->GetWindow().draw(mHazeSprite);
	}
}

void ExitRoom::Unlock()
{
	mLocked = false;
}

void ExitRoom::SetHazeColor(sf::Color c)
{
	mHazeColor = c;
}
