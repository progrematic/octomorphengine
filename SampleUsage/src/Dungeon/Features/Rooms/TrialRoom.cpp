#include <Dungeon/Features/Rooms/TrialRoom.h>

#include <Octomorph/Managers/MessageManager.h>
#include <Octomorph/Managers/ViewManager.h>

#include <Messages/TrialStartedMessage.h>
#include <Messages/TrialEndedMessage.h>

TrialRoom::TrialRoom()
{
	mType = ROOM_TYPE::TRIAL;
	mActive = false;
}

void TrialRoom::SetTrialType(TRIAL_TYPE type)
{
	mTrialType = type;
}

TRIAL_TYPE TrialRoom::GetTrialType()
{
	return mTrialType;
}

bool TrialRoom::IsTrialActive()
{
	return mActive;
}

void TrialRoom::StartTrial()
{
	mActive = true;
	std::shared_ptr<TrialStartedMessage> msg = std::make_shared<TrialStartedMessage>();
	msg->Setup(std::static_pointer_cast<TrialRoom>(this->shared_from_this()));
	Octomorph::Managers::MessageManager::GetInstance()->SendMessage(msg);

	// Center the view on the trial room
	sf::Vector2f viewpoint = sf::Vector2f(((float)mRect.left + ((float)mRect.width / 2.f)) * TILE_SIZE,
										  ((float)mRect.top + ((float)mRect.height / 2.f)) * TILE_SIZE);
	Octomorph::Managers::ViewManager::GetInstance()->SetGameViewpointTarget(VIEW::PLAYER1, viewpoint);
}

void TrialRoom::EndTrial()
{
	mActive = false;
	std::shared_ptr<TrialEndedMessage> msg = std::make_shared<TrialEndedMessage>();
	msg->Setup(std::static_pointer_cast<TrialRoom>(this->shared_from_this()));
	Octomorph::Managers::MessageManager::GetInstance()->SendMessage(msg);
}
