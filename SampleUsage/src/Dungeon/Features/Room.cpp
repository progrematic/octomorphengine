#include <Dungeon/Features/Room.h>
#include <GameGlobals.h>

Room::Room()
{
	mType = ROOM_TYPE::GENERIC;
	mRect = sf::IntRect(0, 0, 0, 0);
	mDiscovered = false;
}

void Room::CopyFromRoom(std::shared_ptr<Room> other)
{
	mRect = other->mRect;
	mDiscovered = other->mDiscovered;
	mExits = other->mExits;
}

void Room::SetRect(sf::IntRect rect)
{
	mRect = rect;
}

sf::IntRect Room::GetRect()
{
	return mRect;
}

ROOM_TYPE Room::GetType()
{
	return mType;
}

void Room::SetDiscovered(bool x)
{
	mDiscovered = x;
}

bool Room::GetDiscovered()
{
	return mDiscovered;
}

std::vector<std::shared_ptr<Tile>> Room::GetExits()
{
	return mExits;
}

void Room::AddExit(std::shared_ptr<Tile> exit)
{
	mExits.emplace_back(exit);
}