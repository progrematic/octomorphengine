#include <Dungeon/Dungeon.h>

#include <GameGlobals.h>

#include <Octomorph/Managers/TextureManager.h>
#include <Octomorph/Managers/ViewManager.h>
#include <Octomorph/Managers/MessageManager.h>
#include <Managers/TrialManager.h>

#include <Dungeon/Features/Rooms/SpawnRoom.h>
#include <Dungeon/Features/Rooms/TrialRoom.h>
#include <Dungeon/Features/Rooms/ExitRoom.h>

#include <Messages/TrialStartedMessage.h>
#include <Messages/TrialEndedMessage.h>

Dungeon::Dungeon() : IMessageHandler(ETOI(MESSAGE_TYPE::COUNT))
{
	mTilesheet.Load("Resources/spritesheets/walls/dungeon.png", "Resources/spritesheets/walls/dungeon.json");
}

Dungeon::~Dungeon()
{
	IMessageHandler::Disable();
}

void Dungeon::Generate(int width, int height, int maxFeatures)
{
	mWidth = width;
	mHeight = height;

	mTiles.clear();
	for (int i = 0; i < mHeight; i++)
	{
		for (int j = 0; j < mWidth; j++)
		{
			std::shared_ptr<Tile> tile = std::make_shared<Tile>();
			tile->type = TILE_TYPE::EMPTY;
			tile->columnIndex = j;
			tile->rowIndex = i;
			tile->stepCount = 0;
			tile->sprite.setPosition((float)(TILE_SIZE * j), (float)(TILE_SIZE * i));

			mTiles.push_back(tile);
		}
	}

	mCorridors.clear();
	mRooms.clear();
	mWalls.clear();

	// Place the first room in the center
	if (!MakeRoom(mWidth / 2, mHeight - 1, DIRECTION::NORTH, true))
	{
		std::cout << "Unable to place the first room." << std::endl;
		return;
	}

	// Place the rest of the features
	for (int i = 1; i < maxFeatures; ++i)
	{
		if (!CreateFeature())
		{
			std::cout << "Unable to place more features (placed " << i << ")." << std::endl;
			break;
		}
	}

	// All features created - let's pick a room to be our exit room
	std::shared_ptr<Room> highestRoom = nullptr;
	for (std::shared_ptr<Room> room : mRooms)
	{
		if (highestRoom == nullptr || room->GetRect().top < highestRoom->GetRect().top)
		{
			highestRoom = room;
		}
	}

	std::shared_ptr<ExitRoom> exitRoom = std::make_shared<ExitRoom>();
	exitRoom->CopyFromRoom(highestRoom);
	mRooms.erase(std::find(mRooms.begin(), mRooms.end(), highestRoom));
	mRooms.emplace_back(exitRoom);

	// Ultra-smooth corridors!
	for (int i = 0; i < 15; i++)
		SmoothCorridors();
	BitmaskTextures();

	RandomizeColor();

	for (std::shared_ptr<Tile> tile : mTiles)
	{
		if (tile->type == TILE_TYPE::CORRIDOR)
			mCorridors.push_back(tile);
	}

	IMessageHandler::Enable();
	IMessageHandler::Subscribe(ETOI(MESSAGE_TYPE::TRIAL_STARTED));
	IMessageHandler::Subscribe(ETOI(MESSAGE_TYPE::TRIAL_ENDED));
}

void Dungeon::Print()
{
	for (int y = 0; y < mHeight; y++)
	{
		for (int x = 0; x < mWidth; x++)
		{
			switch (GetTile(x, y)->type)
			{
			case TILE_TYPE::EMPTY:
				std::cout << ".";
				break;
			case TILE_TYPE::FLOOR:
				std::cout << " ";
				break;
			case TILE_TYPE::CORRIDOR:
				std::cout << ",";
				break;
			case TILE_TYPE::DOOR:
				std::cout << "*";
				break;
			case TILE_TYPE::WALL:
				std::cout << "#";
				break;
			case TILE_TYPE::ROOM_WALL:
				std::cout << "$";
				break;
			default:
				std::cout << "?";
				break;
			}
		}
		std::cout << std::endl;
	}
}

void Dungeon::Update(float dt)
{
	for (std::shared_ptr<Tile> tile : mTiles)
	{
		sf::Color c = tile->sprite.getColor();
		c.g = 255 - (tile->stepCount * 5);
		if (c.g < 25)
			c.g = 25;
		c.b = c.g;
		tile->sprite.setColor(c);
	}

	for (std::shared_ptr<Room> room : mRooms)
	{
		room->Update(dt);
	}
}

void Dungeon::Draw(float dt)
{
	for (std::shared_ptr<Tile> tile : mTiles)
	{
		if (tile->type != TILE_TYPE::EMPTY)
			Octomorph::Managers::ViewManager::GetInstance()->GetWindow().draw(tile->sprite);
	}

	for (std::shared_ptr<Room> room : mRooms)
	{
		room->Draw(dt);
	}
}

bool Dungeon::IsTileValid(int x, int y)
{
	if (x >= 0 && y >= 0 && x < mWidth && y < mHeight)
		return true;

	return false;
}

int Dungeon::GetWidth()
{
	return mWidth;
}

int Dungeon::GetHeight()
{
	return mHeight;
}

std::shared_ptr<Tile> Dungeon::GetTile(int x, int y)
{
	if (IsTileValid(x, y))
		return mTiles[y * mWidth + x];

	return mTiles[0];
}

std::shared_ptr<Tile> Dungeon::GetTile(sf::Vector2f location)
{
	int x = static_cast<int>(location.x) / TILE_SIZE;
	int y = static_cast<int>(location.y) / TILE_SIZE;

	return GetTile(x, y);
}

std::shared_ptr<Tile> Dungeon::GetAdjacentTile(std::shared_ptr<Tile> tile, DIRECTION dir)
{
	int curX = tile->columnIndex;
	int curY = tile->rowIndex;

	switch (dir)
	{
	case DIRECTION::NORTH:
		return GetTile(curX, curY - 1);
	case DIRECTION::SOUTH:
		return GetTile(curX, curY + 1);
	case DIRECTION::EAST:
		return GetTile(curX + 1, curY);
	case DIRECTION::WEST:
		return GetTile(curX - 1, curY);
	default:
		return GetTile(0, 0);
	}
}

void Dungeon::SetTile(int x, int y, TILE_TYPE type)
{
	SetTile(GetTile(x, y), type);
}

void Dungeon::SetTile(std::shared_ptr<Tile> tile, TILE_TYPE type)
{
	tile->type = type;
	switch (tile->type)
	{
	case TILE_TYPE::FLOOR:
		mTilesheet.GetTile(static_cast<int>(TILE_SPRITE::FLOOR), tile->sprite);
		break;
	case TILE_TYPE::CORRIDOR:
		mTilesheet.GetTile(static_cast<int>(TILE_SPRITE::FLOOR_ALT), tile->sprite);
		break;
	case TILE_TYPE::DOOR:
		mTilesheet.GetTile(static_cast<int>(TILE_SPRITE::FLOOR), tile->sprite);
		break;
	case TILE_TYPE::WALL:
	case TILE_TYPE::ROOM_WALL:
		mTilesheet.GetTile(static_cast<int>(TILE_SPRITE::WALL_0), tile->sprite);
		break;
	default:
		mTilesheet.GetTile(static_cast<int>(TILE_SPRITE::WALL_0), tile->sprite);
		break;
	}
}

bool Dungeon::IsPassable(TILE_TYPE type)
{
	switch (type)
	{
	case TILE_TYPE::FLOOR:
		return true;
	case TILE_TYPE::CORRIDOR:
		return true;
	case TILE_TYPE::DOOR:
		return true;
	case TILE_TYPE::WALL:
	case TILE_TYPE::ROOM_WALL:
		return false;
	default:
		return false;
	}
}

bool Dungeon::IsWall(TILE_TYPE type)
{
	switch (type)
	{
	case TILE_TYPE::WALL:
	case TILE_TYPE::ROOM_WALL:
		return true;
	default:
		return false;
	}
}

std::shared_ptr<Room> Dungeon::IsRoom(std::shared_ptr<Tile> tile)
{
	int x = tile->columnIndex;
	int y = tile->rowIndex;

	for (std::shared_ptr<Room> room : mRooms)
	{
		if (x >= room->GetRect().left && x < room->GetRect().left + room->GetRect().width &&
			y >= room->GetRect().top && y < room->GetRect().top + room->GetRect().height)
		{
			return room;
		}
	}

	return nullptr;
}

std::shared_ptr<Room> Dungeon::GetSpawnRoom()
{
	return mRooms[0];
}

std::shared_ptr<Room> Dungeon::GetRandomRoom()
{
	int r = Octomorph::OctomorphRandom::GetInt((int)mRooms.size());
	return mRooms[r];
}

std::shared_ptr<Tile> Dungeon::GetRandomCorridor()
{
	int r = Octomorph::OctomorphRandom::GetInt((int)mCorridors.size());
	return mCorridors[r];
}

bool Dungeon::CreateFeature()
{
	for (int i = 0; i < 1000; ++i)
	{
		if (mWalls.empty())
			break;

		// choose a random side of a random room or corridor
		int r = Octomorph::OctomorphRandom::GetInt((int)mWalls.size());
		int x = Octomorph::OctomorphRandom::GetInt(mWalls[r].left, mWalls[r].left + mWalls[r].width - 1);
		int y = Octomorph::OctomorphRandom::GetInt(mWalls[r].top, mWalls[r].top + mWalls[r].height - 1);

		// north, south, west, east
		for (int j = 0; j < ETOI(DIRECTION::COUNT); j++)
		{
			if (CreateFeature(x, y, static_cast<DIRECTION>(j)))
			{
				mWalls.erase(mWalls.begin() + r);
				return true;
			}
		}
	}

	return false;
}

bool Dungeon::CreateFeature(int x, int y, DIRECTION dir)
{
	static const int roomChance = 35; // corridorChance = 100 - roomChance

	int dx = 0;
	int dy = 0;

	if (dir == DIRECTION::NORTH)
		dy = 1;
	else if (dir == DIRECTION::SOUTH)
		dy = -1;
	else if (dir == DIRECTION::EAST)
		dx = -1;
	else if (dir == DIRECTION::WEST)
		dx = 1;

	if (GetTile(x + dx, y + dy)->type != TILE_TYPE::FLOOR && GetTile(x + dx, y + dy)->type != TILE_TYPE::CORRIDOR)
		return false;

	if (Octomorph::OctomorphRandom::GetInt(100) < roomChance)
	{
		if (MakeRoom(x, y, dir))
		{
			SetTile(x, y, TILE_TYPE::DOOR);
			AssignExitToRooms(GetTile(x, y));
			return true;
		}
	}

	else
	{
		if (MakeCorridor(x, y, dir))
		{
			if (GetTile(x + dx, y + dy)->type == TILE_TYPE::FLOOR)
			{
				SetTile(x, y, TILE_TYPE::DOOR);
				AssignExitToRooms(GetTile(x, y));
			}
			else // don't place a door between corridors
				SetTile(x, y, TILE_TYPE::CORRIDOR);

			return true;
		}
	}

	return false;
}

bool Dungeon::MakeRoom(int x, int y, DIRECTION dir, bool firstRoom)
{
	int minRoomSize = 5;
	int maxRoomSize = 10;

	std::shared_ptr<Room> room;
	if (firstRoom)
	{
		room = std::make_shared<SpawnRoom>();
	}
	else
	{
		int r = Octomorph::OctomorphRandom::GetInt(static_cast<int>(ROOM_TYPE::EXIT) - 1) + 1; // -1/+1 to exclude spawn room
		switch (static_cast<ROOM_TYPE>(r))
		{
		case ROOM_TYPE::TRIAL:
		{
			room = std::make_shared<TrialRoom>();

			// Pick a trial type randomly (could give each type a weight in the future)
			TRIAL_TYPE trialType = static_cast<TRIAL_TYPE>(Octomorph::OctomorphRandom::GetInt(ETOI(TRIAL_TYPE::COUNT)));
			std::static_pointer_cast<TrialRoom>(room)->SetTrialType(trialType);
		}
		break;
		case ROOM_TYPE::GENERIC:
		default:
		{
			room = std::make_shared<Room>();
		}
		break;
		}
	}
	sf::IntRect rc;
	rc.width = Octomorph::OctomorphRandom::GetInt(minRoomSize, maxRoomSize);
	rc.height = Octomorph::OctomorphRandom::GetInt(minRoomSize, maxRoomSize);

	if (dir == DIRECTION::NORTH)
	{
		rc.left = x - rc.width / 2;
		rc.top = y - rc.height;
	}

	else if (dir == DIRECTION::SOUTH)
	{
		rc.left = x - rc.width / 2;
		rc.top = y + 1;
	}

	else if (dir == DIRECTION::WEST)
	{
		rc.left = x - rc.width;
		rc.top = y - rc.height / 2;
	}

	else if (dir == DIRECTION::EAST)
	{
		rc.left = x + 1;
		rc.top = y - rc.height / 2;
	}

	room->SetRect(rc);

	if (PlaceRect(rc, TILE_TYPE::FLOOR, true))
	{
		mRooms.emplace_back(room);

		if (dir != DIRECTION::SOUTH || firstRoom) // north side
			mWalls.emplace_back(sf::IntRect{ rc.left, rc.top - 1, rc.width, 1 });
		if (dir != DIRECTION::NORTH || firstRoom) // south side
			mWalls.emplace_back(sf::IntRect{ rc.left, rc.top + rc.height, rc.width, 1 });
		if (dir != DIRECTION::EAST || firstRoom) // west side
			mWalls.emplace_back(sf::IntRect{ rc.left - 1, rc.top, 1, rc.height });
		if (dir != DIRECTION::WEST || firstRoom) // east side
			mWalls.emplace_back(sf::IntRect{ rc.left + rc.width, rc.top, 1, rc.height });

		// Post-placement room setup
		switch (room->GetType())
		{
		case ROOM_TYPE::TRIAL:
		{
			TrialManager::GetInstance()->AddTrialRoom(std::static_pointer_cast<TrialRoom>(room));
			switch (std::static_pointer_cast<TrialRoom>(room)->GetTrialType())
			{
			case TRIAL_TYPE::SPIKE_MAZE:

				TILE_TYPE centerTileType = TILE_TYPE::WALL;

				std::shared_ptr<Tile> centerTile = GetTile(rc.left + (int)floor(rc.width / 2), rc.top + (int)floor(rc.height / 2));
				SetTile(centerTile, centerTileType);
				BitmaskTile(centerTile);
				if (rc.width % 2 == 0)
				{
					// Even width - must remove two tiles
					std::shared_ptr<Tile> tile = GetTile(rc.left + (int)floor(rc.width / 2) - 1, rc.top + (int)floor(rc.height / 2));
					SetTile(tile, centerTileType);
					BitmaskTile(tile);
				}
				if (rc.height % 2 == 0)
				{
					// Even height - must remove two tiles
					std::shared_ptr<Tile> tile = GetTile(rc.left + (int)floor(rc.width / 2), rc.top + (int)floor(rc.height / 2) - 1);
					SetTile(tile, centerTileType);
					BitmaskTile(tile);
				}
				if (rc.width % 2 == 0 && rc.height % 2 == 0)
				{
					// Even width and height - must remove corner tile
					std::shared_ptr<Tile> tile = GetTile(rc.left + (int)floor(rc.width / 2) - 1, rc.top + (int)floor(rc.height / 2) - 1);
					SetTile(tile, centerTileType);
					BitmaskTile(tile);
				}

				break;
			}
		}
		break;
		}

		return true;
	}

	return false;
}

bool Dungeon::MakeCorridor(int x, int y, DIRECTION dir)
{
	static const int minCorridorLength = 3;
	static const int maxCorridorLength = 6;

	sf::IntRect corridor;
	corridor.left = x;
	corridor.top = y;

	if (Octomorph::OctomorphRandom::GetBool()) // horizontal corridor
	{
		corridor.width = Octomorph::OctomorphRandom::GetInt(minCorridorLength, maxCorridorLength);
		corridor.height = 1;

		if (dir == DIRECTION::NORTH)
		{
			corridor.top = y - 1;

			if (Octomorph::OctomorphRandom::GetBool()) // west
				corridor.left = x - corridor.width + 1;
		}

		else if (dir == DIRECTION::SOUTH)
		{
			corridor.top = y + 1;

			if (Octomorph::OctomorphRandom::GetBool()) // west
				corridor.left = x - corridor.width + 1;
		}

		else if (dir == DIRECTION::WEST)
			corridor.left = x - corridor.width;

		else if (dir == DIRECTION::EAST)
			corridor.left = x + 1;
	}

	else // vertical corridor
	{
		corridor.width = 1;
		corridor.height = Octomorph::OctomorphRandom::GetInt(minCorridorLength, maxCorridorLength);

		if (dir == DIRECTION::NORTH)
			corridor.top = y - corridor.height;

		else if (dir == DIRECTION::SOUTH)
			corridor.top = y + 1;

		else if (dir == DIRECTION::WEST)
		{
			corridor.left = x - 1;

			if (Octomorph::OctomorphRandom::GetBool()) // north
				corridor.top = y - corridor.height + 1;
		}

		else if (dir == DIRECTION::EAST)
		{
			corridor.left = x + 1;

			if (Octomorph::OctomorphRandom::GetBool()) // north
				corridor.top = y - corridor.height + 1;
		}
	}

	if (PlaceRect(corridor, TILE_TYPE::CORRIDOR))
	{
		if (dir != DIRECTION::SOUTH && corridor.width != 1) // north side
			mWalls.emplace_back(sf::IntRect{ corridor.left, corridor.top - 1, corridor.width, 1 });
		if (dir != DIRECTION::NORTH && corridor.width != 1) // south side
			mWalls.emplace_back(sf::IntRect{ corridor.left, corridor.top + corridor.height, corridor.width, 1 });
		if (dir != DIRECTION::EAST && corridor.height != 1) // west side
			mWalls.emplace_back(sf::IntRect{ corridor.left - 1, corridor.top, 1, corridor.height });
		if (dir != DIRECTION::WEST && corridor.height != 1) // east side
			mWalls.emplace_back(sf::IntRect{ corridor.left + corridor.width, corridor.top, 1, corridor.height });

		return true;
	}

	return false;
}

bool Dungeon::PlaceRect(const sf::IntRect& rect, TILE_TYPE type, bool useRoomWalls)
{
	if (rect.left < 1 || rect.top < 1 || rect.left + rect.width > mWidth - 1 || rect.top + rect.height > mHeight - 1)
		return false;

	for (int y = rect.top; y < rect.top + rect.height; ++y)
		for (int x = rect.left; x < rect.left + rect.width; ++x)
		{
			if (GetTile(x, y)->type != TILE_TYPE::EMPTY)
				return false; // the area already used
		}

	for (int y = rect.top - 1; y < rect.top + rect.height + 1; ++y)
		for (int x = rect.left - 1; x < rect.left + rect.width + 1; ++x)
		{
			if (x == rect.left - 1 || y == rect.top - 1 || x == rect.left + rect.width || y == rect.top + rect.height)
				SetTile(x, y, useRoomWalls ? TILE_TYPE::ROOM_WALL : TILE_TYPE::WALL);
			else
				SetTile(x, y, type);
		}

	return true;
}

void Dungeon::AssignExitToRooms(std::shared_ptr<Tile> tile)
{
	// Check surrounding rooms and assign this as an exit if needed
	int x = tile->columnIndex;
	int y = tile->rowIndex;

	// Check north
	if (GetTile(x, y - 1)->type == TILE_TYPE::FLOOR)
	{
		std::shared_ptr<Room> room = IsRoom(GetTile(x, y - 1));
		if (room)
			room->AddExit(tile);
	}

	// Check south
	if (GetTile(x, y + 1)->type == TILE_TYPE::FLOOR)
	{
		std::shared_ptr<Room> room = IsRoom(GetTile(x, y + 1));
		if (room)
			room->AddExit(tile);
	}
	// Check east
	if (GetTile(x - 1, y)->type == TILE_TYPE::FLOOR)
	{
		std::shared_ptr<Room> room = IsRoom(GetTile(x - 1, y));
		if (room)
			room->AddExit(tile);
	}
	// Check west
	if (GetTile(x + 1, y)->type == TILE_TYPE::FLOOR)
	{
		std::shared_ptr<Room> room = IsRoom(GetTile(x + 1, y));
		if (room)
			room->AddExit(tile);
	}
}

void Dungeon::SmoothCorridors()
{
	std::vector<std::shared_ptr<Tile>> potentialCorridors;
	for (int j = 0; j < mHeight; j++)
	{
		potentialCorridors.clear();
		bool scanning = false;
		for (int i = 1; i < mWidth; i++)
		{
			if (!scanning && GetTile(i, j)->type == TILE_TYPE::WALL && GetTile(i - 1, j)->type == TILE_TYPE::CORRIDOR)
				scanning = true;

			if (scanning)
			{
				if (GetTile(i, j)->type == TILE_TYPE::WALL)
				{
					potentialCorridors.emplace_back(GetTile(i, j));
				}
				else if (GetTile(i, j)->type == TILE_TYPE::CORRIDOR)
				{
					// Found corridor on the other side of the walls.. dig them out!
					for (std::shared_ptr<Tile> t : potentialCorridors)
					{
						SetTile(t, TILE_TYPE::CORRIDOR);

						// Check surrounding tiles. If they're empty, make them walls!
						std::shared_ptr<Tile> adjacents[4];
						adjacents[0] = GetAdjacentTile(t, DIRECTION::NORTH);
						adjacents[1] = GetAdjacentTile(t, DIRECTION::SOUTH);
						adjacents[2] = GetAdjacentTile(t, DIRECTION::EAST);
						adjacents[3] = GetAdjacentTile(t, DIRECTION::WEST);

						for (int j = 0; j < 4; j++)
						{
							if (adjacents[j]->type == TILE_TYPE::EMPTY)
								SetTile(adjacents[j], TILE_TYPE::WALL);
						}
					}
					potentialCorridors.clear();
					scanning = false;

					// Recheck this tile
					i--;
				}
				else
				{
					// Found something other than corridor on the other side of the wall.. no good for smoothing
					potentialCorridors.clear();
					scanning = false;
				}
			}
		}
	}

	// We now duplicate the above but for vertical potentials
	for (int i = 0; i < mWidth; i++)
	{
		potentialCorridors.clear();
		bool scanning = false;
		for (int j = 1; j < mHeight; j++)
		{
			if (!scanning && GetTile(i, j)->type == TILE_TYPE::WALL && GetTile(i, j - 1)->type == TILE_TYPE::CORRIDOR)
				scanning = true;

			if (scanning)
			{
				if (GetTile(i, j)->type == TILE_TYPE::WALL)
				{
					potentialCorridors.emplace_back(GetTile(i, j));
				}
				else if (GetTile(i, j)->type == TILE_TYPE::CORRIDOR)
				{
					// Found corridor on the other side of the walls.. dig them out!
					for (std::shared_ptr<Tile> t : potentialCorridors)
					{
						SetTile(t, TILE_TYPE::CORRIDOR);

						// Check surrounding tiles. If they're empty, make them walls!
						std::shared_ptr<Tile> adjacents[4];
						adjacents[0] = GetAdjacentTile(t, DIRECTION::NORTH);
						adjacents[1] = GetAdjacentTile(t, DIRECTION::SOUTH);
						adjacents[2] = GetAdjacentTile(t, DIRECTION::EAST);
						adjacents[3] = GetAdjacentTile(t, DIRECTION::WEST);

						for (int j = 0; j < 4; j++)
						{
							if (adjacents[j]->type == TILE_TYPE::EMPTY)
								SetTile(adjacents[j], TILE_TYPE::WALL);
						}
					}
					potentialCorridors.clear();
					scanning = false;
					
					// Recheck this tile
					j--;
				}
				else
				{
					// Found something other than corridor on the other side of the wall.. no good for smoothing
					potentialCorridors.clear();
					scanning = false;
				}
			}
		}
	}
}

int Dungeon::CalculateBitmask(int x, int y)
{
	if (IsWall(GetTile(x, y)->type))
	{
		int value = 0;

		if (IsWall(GetTile(x, y - 1)->type))
		{
			value += 1;
		}

		if (IsWall(GetTile(x + 1, y)->type))
		{
			value += 2;
		}

		if (IsWall(GetTile(x, y + 1)->type))
		{
			value += 4;
		}

		if (IsWall(GetTile(x - 1, y)->type))
		{
			value += 8;
		}

		return value;
	}

	return -1;
}

void Dungeon::BitmaskTextures()
{
	for (int i = 0; i < mWidth; i++)
	{
		for (int j = 0; j < mHeight; j++)
		{
			int bitmask = CalculateBitmask(i, j);
			if (bitmask >= 0)
				mTilesheet.GetTile(bitmask, GetTile(i, j)->sprite);
		}
	}
}

void Dungeon::BitmaskTile(std::shared_ptr<Tile> tile)
{
	if (IsWall(tile->type))
		mTilesheet.GetTile(CalculateBitmask(tile->columnIndex, tile->rowIndex), tile->sprite);

	// Bitmask surrounding tiles
	std::shared_ptr<Tile> northTile = GetAdjacentTile(tile, DIRECTION::NORTH);
	if (IsWall(northTile->type))
		mTilesheet.GetTile(CalculateBitmask(northTile->columnIndex, northTile->rowIndex), northTile->sprite);
	std::shared_ptr<Tile> southTile = GetAdjacentTile(tile, DIRECTION::SOUTH);
	if (IsWall(southTile->type))
		mTilesheet.GetTile(CalculateBitmask(southTile->columnIndex, southTile->rowIndex), southTile->sprite);
	std::shared_ptr<Tile> eastTile = GetAdjacentTile(tile, DIRECTION::EAST);
	if (IsWall(eastTile->type))
		mTilesheet.GetTile(CalculateBitmask(eastTile->columnIndex, eastTile->rowIndex), eastTile->sprite);
	std::shared_ptr<Tile> westTile = GetAdjacentTile(tile, DIRECTION::WEST);
	if (IsWall(westTile->type))
		mTilesheet.GetTile(CalculateBitmask(westTile->columnIndex, westTile->rowIndex), westTile->sprite);
}

void Dungeon::RandomizeColor()
{
	sf::Color c;
	c.a = 255;
	c.r = Octomorph::OctomorphRandom::GetInt(50, 250);
	c.g = Octomorph::OctomorphRandom::GetInt(50, 250);
	c.b = Octomorph::OctomorphRandom::GetInt(50, 250);

	std::cout << (int)c.r << ":" << (int)c.g << ":" << (int)c.b << std::endl;

	for (std::shared_ptr<Tile> tile : mTiles)
	{
		tile->sprite.setColor(c);
	}
}

bool Dungeon::HandleMessage(const Octomorph::Messages::Message& msg)
{
	switch (ITOE(MESSAGE_TYPE, msg.GetType()))
	{
	case MESSAGE_TYPE::TRIAL_STARTED:
	{
		// Change exits to walls
		for (std::shared_ptr<Tile> tile : ((const TrialStartedMessage&)msg).GetRoom()->GetExits())
		{
			SetTile(tile, TILE_TYPE::WALL);
			BitmaskTile(tile);
		}
	}
	break;
	case MESSAGE_TYPE::TRIAL_ENDED:
	{
		// Change exits to walls
		for (std::shared_ptr<Tile> tile : ((const TrialEndedMessage&)msg).GetRoom()->GetExits())
		{
			SetTile(tile, TILE_TYPE::DOOR);
			BitmaskTile(tile);
		}
	}
	break;
	default:
		return false;
	}

	return true;
}
