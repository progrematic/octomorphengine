#include <Menu.h>

#include <Octomorph/GUI/Widgets/Input.h>
#include <Octomorph/Input/Keyboard.h>
#include <Octomorph/Input/Joystick.h>
#include <Octomorph/Input/Mouse.h>
#include <Octomorph/Managers/ViewManager.h>
#include <Octomorph/Managers/NetworkManager.h>
#include <Octomorph/Managers/TextureManager.h>
#include <Octomorph/Managers/EventManager.h>

using namespace Octomorph::Managers;
using namespace Octomorph::Input;

Menu::Menu(int width, int height, int bitDepth) :
	mState(MENU_STATE::MENU),
	mWidth(width),
	mHeight(height)
{
	ViewManager::GetInstance()->Initialize("Neverend", mWidth, mHeight, bitDepth, 
		sf::Style::Titlebar | sf::Style::Close/* | sf::Style::Fullscreen*/);

	Joystick::Initialize();

	for (int i = 0; i < ETOI(MENU_STATE::COUNT); i++)
	{
		mGuis.insert(GuiPair(ITOE(MENU_STATE, i), nullptr));
	}

	BuildBgGUI();
	BuildMenuGUI();
	BuildHostGUI();
	BuildJoinGUI();

	mGame.Initialize();

	// Enable first-time
	mGuis.at(mState)->Enable();
	SetState(mState);
	RunGame();
}

// PRIVATE

void Menu::RunGame()
{
	float curTime = mDTClock.restart().asSeconds();
	float dt = 0.f;
	mRunning = true;

	while (mRunning)
	{
		Mouse::Update();
		Keyboard::Update();
		Joystick::Update();
		EventManager::GetInstance()->Update();

		bool cleanup = false;
		sf::Event e;
		while (ViewManager::GetInstance()->GetWindow().pollEvent(e))
		{
			if ((e.type == sf::Event::Closed) || (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Escape)))
			{
				if (mState == MENU_STATE::GAME)
					mGame.GameOver();
				else
				{
					cleanup = true;
					break;
				}
			}

			if (e.type == sf::Event::MouseWheelScrolled)
			{
				float zoomDelta = 1.1f;
				if (e.mouseWheel.x > 0)
				{
					zoomDelta = 0.9f;
				}

				ViewManager::GetInstance()->GetView(VIEW::PLAYER1).zoom(zoomDelta);
			}

			if (e.type == sf::Event::MouseMoved)
			{
				Mouse::MousePosCallback(sf::Mouse::getPosition(ViewManager::GetInstance()->GetWindow()).x, sf::Mouse::getPosition(ViewManager::GetInstance()->GetWindow()).y);
			}

			if (e.type == sf::Event::MouseButtonPressed || e.type == sf::Event::MouseButtonReleased)
			{
				Mouse::MouseButtonCallback(e.mouseButton.button);
			}

			if (e.type == sf::Event::KeyPressed || e.type == sf::Event::KeyReleased)
			{
				Keyboard::KeyCallback(e.key.code);
			}

			// here, we blow away the last textenetered value so that we can check against -1 to see if the user typed anything new
			if (e.type == sf::Event::TextEntered)
			{
				Keyboard::TextCallback(e.text.unicode);
			}

			EventManager::GetInstance()->HandleEvent(e);
		}

		if (cleanup)
		{
			break;
		}

		float newTime = mDTClock.getElapsedTime().asSeconds();
		float frameTime = std::max(0.f, newTime - curTime);
		curTime = newTime;

		if (mState == MENU_STATE::GAME)
		{
			mGame.Update(frameTime);
			mGame.Draw(frameTime);

			if (mGame.IsGameOver())
			{
				SetState(MENU_STATE::MENU);
			}
		}
		else
		{
			mBgGui.Update(frameTime);

			if (Keyboard::KeyDown(sf::Keyboard::Tab))
			{
				if (Keyboard::Key(sf::Keyboard::LShift))
					mGuis.at(mState)->PrevTabbable();
				else
					mGuis.at(mState)->NextTabbable();

			}
			mGuis.at(mState)->Update(frameTime);

			// Need to check for game state again since the gui update might have updated state
			if (mState != MENU_STATE::GAME)
			{
				ViewManager::GetInstance()->SetView(VIEW::UI);
				ViewManager::GetInstance()->GetWindow().clear();
				mBgGui.Draw(frameTime);
				mGuis.at(mState)->Draw(frameTime);
				ViewManager::GetInstance()->GetWindow().display();
			}
		}
	}
}

void Menu::SetState(MENU_STATE state)
{
	MENU_STATE prevState = mState;
	mState = state;

	if (prevState != mState)
	{
		if (prevState != MENU_STATE::GAME)
			mGuis.at(prevState)->Disable();
		if (mState != MENU_STATE::GAME)
			mGuis.at(mState)->Enable();

		switch (mState)
		{
		case MENU_STATE::GAME:
		{
			mGame.Reset();
		}
		break;
		}
	}
}

void Menu::PlayGame()
{
	SetState(MENU_STATE::GAME);
}

void Menu::Host()
{
	SetState(MENU_STATE::HOST);
}

void Menu::Join()
{
	Keyboard::ClearTypedString();
	SetState(MENU_STATE::JOIN);
}

void Menu::BackToMenu()
{
	SetState(MENU_STATE::MENU);
}

void Menu::Quit()
{
	mRunning = false;
}

void Menu::BuildBgGUI()
{
	mBgGui = GUI(sf::Vector2f(static_cast<float>(mWidth) / 2.f, static_cast<float>(mHeight) / 2.f));
	mBgGui.SetColor(sf::Color::Transparent);
	int spriteId = spriteId = TextureManager::AddTexture("Resources/UI/menubg.png");
	if (spriteId >= 0)
	{
		std::shared_ptr<Image> bgImage = std::make_shared<Image>();
		bgImage->SetImage(*TextureManager::GetTexture(spriteId));
		bgImage->SetSize(sf::Vector2f(static_cast<float>(mWidth), static_cast<float>(mHeight)));
		mBgGui.AddWidget(bgImage, false);
	}
	mBgGui.Enable();
}

void Menu::BuildMenuGUI()
{
	std::shared_ptr<GUI> gui = std::make_shared<GUI>(sf::Vector2f(static_cast<float>(mWidth) / 2.f, static_cast<float>(mHeight) / 2.f - 50.f));
	gui->SetColor(sf::Color::Transparent);

	std::shared_ptr<Button> singleplayerButton = std::make_shared<Button>();
	singleplayerButton->SetAnchor(sf::Vector2f(0.5f, 0));
	singleplayerButton->SetSize(sf::Vector2f(200, 45));
	singleplayerButton->GetLabel().SetString("SINGLEPLAYER");
	singleplayerButton->SetCallback(std::bind(&Menu::PlayGame, this));
	gui->AddWidget(singleplayerButton);

	std::shared_ptr<Button> hostButton = std::make_shared<Button>();
	hostButton->SetAnchor(sf::Vector2f(1, 0));
	hostButton->SetPos(sf::Vector2f(-2.5f, 50));
	hostButton->SetSize(sf::Vector2f(200, 45));
	hostButton->GetLabel().SetString("HOST");
	hostButton->SetCallback(std::bind(&Menu::Host, this));
	gui->AddWidget(hostButton);

	std::shared_ptr<Button> joinButton = std::make_shared<Button>();
	joinButton->SetAnchor(sf::Vector2f(0, 0));
	joinButton->SetPos(sf::Vector2f(2.5f, 50));
	joinButton->SetSize(sf::Vector2f(200, 45));
	joinButton->GetLabel().SetString("JOIN");
	joinButton->SetCallback(std::bind(&Menu::Join, this));
	gui->AddWidget(joinButton);

	std::shared_ptr<Button> quitButton = std::make_shared<Button>();
	quitButton->SetAnchor(sf::Vector2f(0.5f, 0));
	quitButton->SetPos(sf::Vector2f(0, 100));
	quitButton->SetSize(sf::Vector2f(200, 45));
	quitButton->GetLabel().SetString("QUIT");
	quitButton->SetCallback(std::bind(&Menu::Quit, this));
	gui->AddWidget(quitButton);

	mGuis.at(MENU_STATE::MENU) = gui;
}

void Menu::BuildHostGUI()
{
	std::shared_ptr<GUI> gui = std::make_shared<GUI>(sf::Vector2f(static_cast<float>(mWidth) / 2.f, static_cast<float>(mHeight) / 2.f - 50.f));
	gui->SetColor(sf::Color::Transparent);

	std::shared_ptr<TextLabel> ipLabel = std::make_shared<TextLabel>();
	ipLabel->SetAnchor(sf::Vector2f(0.5f, 0));
	ipLabel->SetString(NetworkManager::GetLocalIp());
	gui->AddWidget(ipLabel);

	std::shared_ptr<Button> hostButton = std::make_shared<Button>();
	hostButton->SetAnchor(sf::Vector2f(0.5f, 0));
	hostButton->SetPos(sf::Vector2f(0, 50));
	hostButton->SetSize(sf::Vector2f(200, 45));
	hostButton->GetLabel().SetString("HOST");
	hostButton->SetCallback(std::bind(&Menu::PlayGame, this));
	gui->AddWidget(hostButton);

	std::shared_ptr<Button> quitButton = std::make_shared<Button>();
	quitButton->SetAnchor(sf::Vector2f(0.5f, 0));
	quitButton->SetPos(sf::Vector2f(0, 100));
	quitButton->SetSize(sf::Vector2f(200, 45));
	quitButton->GetLabel().SetString("BACK");
	quitButton->SetCallback(std::bind(&Menu::BackToMenu, this));
	gui->AddWidget(quitButton);

	mGuis.at(MENU_STATE::HOST) = gui;
}

void Menu::BuildJoinGUI()
{
	std::shared_ptr<GUI> gui = std::make_shared<GUI>(sf::Vector2f(static_cast<float>(mWidth) / 2.f, static_cast<float>(mHeight) / 2.f - 50.f));
	gui->SetColor(sf::Color::Transparent);

	std::shared_ptr<Input> ipInput = std::make_shared<Input>();
	ipInput->SetAnchor(sf::Vector2f(0.5f, 0));
	ipInput->SetPos(sf::Vector2f(-102.5f, 0));
	ipInput->SetSize(sf::Vector2f(200, 45));
	gui->AddWidget(ipInput, false, true);

	std::shared_ptr<Input> portInput = std::make_shared<Input>();
	portInput->SetAnchor(sf::Vector2f(0.5f, 0));
	portInput->SetPos(sf::Vector2f(102.5f, 0));
	portInput->SetSize(sf::Vector2f(200, 45));
	gui->AddWidget(portInput, false, true);

	std::shared_ptr<Button> joinButton = std::make_shared<Button>();
	joinButton->SetAnchor(sf::Vector2f(0.5f, 0));
	joinButton->SetPos(sf::Vector2f(0, 50));
	joinButton->SetSize(sf::Vector2f(200, 45));
	joinButton->GetLabel().SetString("JOIN");
	joinButton->SetCallback(std::bind(&Menu::PlayGame, this));
	gui->AddWidget(joinButton);

	std::shared_ptr<Button> quitButton = std::make_shared<Button>();
	quitButton->SetAnchor(sf::Vector2f(0.5f, 0));
	quitButton->SetPos(sf::Vector2f(0, 100));
	quitButton->SetSize(sf::Vector2f(200, 45));
	quitButton->GetLabel().SetString("BACK");
	quitButton->SetCallback(std::bind(&Menu::BackToMenu, this));
	gui->AddWidget(quitButton);

	mGuis.at(MENU_STATE::JOIN) = gui;
}