#include "Debugging/Console.h"

#include <Game.h>

#include "Octomorph/Input/Keyboard.h"
#include "Octomorph/Input/Mouse.h"
#include "Octomorph/Input/Joystick.h"

#include "Octomorph/Components/LightSourceComponent.h"
#include <Managers/ItemManager.h>
#include <Octomorph/Managers/ViewManager.h>

#include <Octomorph/Managers/MessageManager.h>
#include <Octomorph/Messages//Message.h>

using namespace Octomorph::Input;

Console* Console::mInstance = nullptr;

Console::Console()
{
	if (mInstance != nullptr)
	{
		std::cout << "ERROR: Attempting to instantiate a second Console!" << std::endl;
		return;
	}
	
	if (!mFont.loadFromFile("resources/fonts/arial.ttf"))
	{
		std::cout << "Error loading font" << std::endl;
	}
}

Console* Console::GetInstance()
{
	if (!mInstance)
		mInstance = new Console();

	return mInstance;
}

void Console::Initialize(Game* game, std::shared_ptr<Dungeon> dungeon)
{
	mActive = false;
	mGame = game;
	mDungeon = dungeon;
	mTypingCommand = false;

	mTypedCommandText.setFont(mFont);
	mTypedCommandText.setCharacterSize(18);
	mTypedCommandText.setFillColor(sf::Color::White);
	mTypedCommandText.setString("");

	mOffset = sf::Vector2f(10, 10);

	mBackgroundColor = sf::Color(100, 100, 100, 120);
	mBackground.setFillColor(mBackgroundColor);
	mTypedCommandBackgroundColor = sf::Color(100, 100, 100, 200);
	mTypedCommandBackground.setFillColor(mTypedCommandBackgroundColor);
	SetBackgroundSize(300);
}

void Console::Update(float dt)
{
	
}

void Console::Draw(float dt)
{
	if (mActive)
	{
		sf::RenderWindow& window = Octomorph::Managers::ViewManager::GetInstance()->GetWindow();

		window.draw(mBackground);
		window.draw(mTypedCommandBackground);
		window.draw(mTypedCommandText);

		int i = 0;
		for (std::vector<sf::Text>::reverse_iterator it = mHistory.rbegin(); it < mHistory.rend(); it++)
		{
			sf::Vector2f offset = -mOffset;
			offset.y -= (int)it->getCharacterSize() + (int)mTypedCommandText.getCharacterSize() + mOffset.y;
			it->setPosition(window.getSize().x - it->getLocalBounds().width + offset.x, window.getSize().y - (it->getCharacterSize() * i) + offset.y);
			window.draw(*it);
			i++;
		}
	}
}

void Console::HandleInput(float dt)
{
	if (Keyboard::KeyDown(sf::Keyboard::Tilde))
		mActive = !mActive;

	if (mActive && Keyboard::KeyDown(sf::Keyboard::Subtract))
	{
		mTypingCommand = !mTypingCommand;
		Keyboard::ClearTypedString();	// Legacy typing.. should use new GUI system
	}

	// Legacy typing.. should use new GUI system
	if (mTypingCommand)
	{
		if (Keyboard::KeyDown(sf::Keyboard::Return))
		{
			ExecuteTypedCommand();
		}
		else
		{
			UpdateCurCommandString();
		}
	}
	else
	{
		sf::Vector2f worldMouse = Octomorph::Managers::ViewManager::GetInstance()->GetWindow().mapPixelToCoords(sf::Vector2i(Mouse::GetMouseX(), Mouse::GetMouseY()));

		if (Keyboard::KeyDown(sf::Keyboard::E))
		{
			ExecuteCommand(DBGCOMMAND::END_TRIAL);
		}

		if (Mouse::ButtonDown(sf::Mouse::Button::Left))
		{
			std::shared_ptr<Tile> tile = mDungeon->GetTile(worldMouse);
			stringstream ss;
			ss << tile->columnIndex << ":" << tile->rowIndex;
			ExecuteCommand(DBGCOMMAND::SPAWN_TORCH, ss.str());
		}

		if (Mouse::ButtonDown(sf::Mouse::Button::Right))
		{
			//ExecuteCommand(DBGCOMMAND::GENERATE_DUNGEON, "50");
			
			std::shared_ptr<Tile> tile = mDungeon->GetTile(worldMouse);
			stringstream ss;
			ss << tile->columnIndex << ":" << tile->rowIndex;
			ExecuteCommand(DBGCOMMAND::SPAWN_ITEM, ss.str());
			
			//ExecuteCommand(DBGCOMMAND::RANDOMIZE_DUNGEON_COLOR);
		}
		if (Mouse::ButtonDown(sf::Mouse::Button::Middle))
		{
			std::shared_ptr<Tile> tile = mDungeon->GetTile(worldMouse);
			stringstream ss;
			ss << tile->columnIndex << ":" << tile->rowIndex;
			ExecuteCommand(DBGCOMMAND::PLAYER_TELEPORT, ss.str());
		}

		if (Keyboard::KeyDown(sf::Keyboard::Q) || Joystick::GetButtonDown(0, Joystick::Button::A))
		{
			ExecuteCommand(DBGCOMMAND::TOGGLE_LIGHTING);
		}

		if (Keyboard::Key(sf::Keyboard::P))
		{
			ExecuteCommand(DBGCOMMAND::PLAYER_LIGHT_RADIUS, "1");
		}
		if (Keyboard::Key(sf::Keyboard::L))
		{
			ExecuteCommand(DBGCOMMAND::PLAYER_LIGHT_RADIUS, "-1");
		}
		if (Keyboard::Key(sf::Keyboard::O))
		{
			ExecuteCommand(DBGCOMMAND::PLAYER_LIGHT_WANING, "1");
		}
		if (Keyboard::Key(sf::Keyboard::K))
		{
			ExecuteCommand(DBGCOMMAND::PLAYER_LIGHT_WANING, "-1");
		}
	}
}

void Console::Print(std::string msg, sf::Color c)
{
	sf::Text text;
	text.setFont(mFont);
	text.setCharacterSize(18);
	text.setFillColor(c);
	text.setString(msg);
	mHistory.push_back(text);

	float textWidth = text.getLocalBounds().width;
	if (textWidth > mBackgroundWidth)
	{
		SetBackgroundSize(static_cast<int>(ceilf(textWidth)));
	}
}
	 
void Console::SetActive(bool active)
{
	mActive = active;
}

bool Console::GetActive()
{
	return mTypingCommand;
}

// PRIVATE

void Console::ExecuteCommand(DBGCOMMAND cmd, std::string argStr)
{
	ExecuteCommand(cmd, Octomorph::OctomorphHelpers::Split(argStr, ':'));
}

void Console::ExecuteCommand(DBGCOMMAND cmd, std::vector<std::string> args)
{
	std::string msg = "unhandled: " + static_cast<int>(cmd);
	sf::Color color = sf::Color::Magenta;
	
	stringstream ss;
	switch (cmd)
	{
	case DBGCOMMAND::TOGGLE_LIGHTING:
	{
		bool lightState = mGame->ToggleLight();

		ss << "Lighting System: " << (lightState ? "ON" : "OFF");
		color = sf::Color::Green;
		msg = ss.str();
	}
	break;

	case DBGCOMMAND::PLAYER_TELEPORT:
	{
		if (args.size() == 2)
		{
			sf::Vector2f teleportPos;
			teleportPos.x = static_cast<float>(min(max(1, atoi(args[0].c_str())), mDungeon->GetWidth()));
			teleportPos.y = static_cast<float>(min(max(1, atoi(args[1].c_str())), mDungeon->GetHeight()));

			sf::Vector2f realPos = sf::Vector2f((teleportPos.x * TILE_SIZE) + (TILE_SIZE / 2), (teleportPos.y * TILE_SIZE) + (TILE_SIZE / 2));
			// TODO: Temporarily disabling this while refactoring player code. Will have to revisit
			//mPlayer->SetGridPos(realPos);

			ss << "Player teleported: " << teleportPos.x << ", " << teleportPos.y;
			color = sf::Color::Green;
			msg = ss.str();
		}
		else
		{
			ss << "Usage: " << DBGCOMMANDStrings[static_cast<int>(DBGCOMMAND::PLAYER_TELEPORT)] << " <x>:<y>";
			color = sf::Color::Red;
			msg = ss.str();
		}
	}
	break;

	case DBGCOMMAND::PLAYER_LIGHT_RADIUS:
	{
		if (args.size() == 1)
		{
			int delta = atoi(args[0].c_str());
			// TODO: Temporarily disabling this while refactoring player code. Will have to revisit
			//mPlayer->GetComponent<LightSourceComponent>()->SetRadius(mPlayer->GetComponent<LightSourceComponent>()->GetRadius() + delta);

			//ss << "Player Light Radius: " << mPlayer->GetComponent<LightSourceComponent>()->GetRadius();
			color = sf::Color::Green;
			msg = ss.str();
		}
		else
		{
			ss << "Usage: " << DBGCOMMANDStrings[static_cast<int>(DBGCOMMAND::PLAYER_LIGHT_RADIUS)] << " <delta>";
			color = sf::Color::Red;
			msg = ss.str();
		}
	}
	break;

	case DBGCOMMAND::PLAYER_LIGHT_WANING:
	{
		if (args.size() == 1)
		{
			int delta = atoi(args[0].c_str());
			// TODO: Temporarily disabling this while refactoring player code. Will have to revisit
			//mPlayer->GetComponent<LightSourceComponent>()->SetWaning(mPlayer->GetComponent<LightSourceComponent>()->GetWaning() + delta);

			//ss << "Player Light Waning: " << mPlayer->GetComponent<LightSourceComponent>()->GetWaning();
			color = sf::Color::Green;
			msg = ss.str();
		}
		else
		{
			ss << "Usage: " << DBGCOMMANDStrings[static_cast<int>(DBGCOMMAND::PLAYER_LIGHT_WANING)] << " <delta>";
			color = sf::Color::Red;
			msg = ss.str();
		}
	}
	break;

	case DBGCOMMAND::END_TRIAL:
	{
		std::shared_ptr<Octomorph::Messages::Message> message = std::make_shared<Octomorph::Messages::Message>();
		message->SetType(ETOI(MESSAGE_TYPE::REQUEST_TRIAL_END));
		Octomorph::Managers::MessageManager::GetInstance()->SendMessage(message);

		color = sf::Color::Green;
		msg = "Active trial ended";
	}
	break;

	case DBGCOMMAND::SPAWN_TORCH:
	{
		if (args.size() == 2)
		{
			sf::Vector2f spawnPos;
			spawnPos.x = static_cast<float>(min(max(1, atoi(args[0].c_str())), mDungeon->GetWidth()));
			spawnPos.y = static_cast<float>(min(max(1, atoi(args[1].c_str())), mDungeon->GetHeight()));

			sf::Vector2f realPos = sf::Vector2f((spawnPos.x * TILE_SIZE) + (TILE_SIZE / 2), (spawnPos.y * TILE_SIZE) + (TILE_SIZE / 2));
			mGame->SpawnTorch(realPos);

			ss << "Torch spawned: " << spawnPos.x << ", " << spawnPos.y;
			color = sf::Color::Green;
			msg = ss.str();
		}
		else
		{
			ss << "Usage: " << DBGCOMMANDStrings[static_cast<int>(DBGCOMMAND::SPAWN_TORCH)] << " <x>:<y>";
			color = sf::Color::Red;
			msg = ss.str();
		}
	}
	break;

	case DBGCOMMAND::SPAWN_ITEM:
	{
		if (args.size() == 2)
		{
			sf::Vector2f spawnPos;
			spawnPos.x = static_cast<float>(min(max(1, atoi(args[0].c_str())), mDungeon->GetWidth()));
			spawnPos.y = static_cast<float>(min(max(1, atoi(args[1].c_str())), mDungeon->GetHeight()));

			sf::Vector2f realPos = sf::Vector2f((spawnPos.x * TILE_SIZE) + (TILE_SIZE / 2), (spawnPos.y * TILE_SIZE) + (TILE_SIZE / 2));
			
			int num = Octomorph::OctomorphRandom::GetInt(1, 1);
			for (int i = 0; i < num; i++)
			{
				int id = Octomorph::OctomorphRandom::GetInt(0, 0);
				int count = Octomorph::OctomorphRandom::GetInt(5, 5);
				sf::Vector2f force;
				force.x = static_cast<float>(Octomorph::OctomorphRandom::GetInt(-50, 50));
				force.y = static_cast<float>(Octomorph::OctomorphRandom::GetInt(-50, 50));
				ItemManager::GetInstance()->SpawnItem(ITEM_TYPE::CONSUMABLE, id, count, realPos, force);
			}

			ss << "Item spawned: " << spawnPos.x << ", " << spawnPos.y;
			color = sf::Color::Green;
			msg = ss.str();
		}
		else
		{
			ss << "Usage: " << DBGCOMMANDStrings[static_cast<int>(DBGCOMMAND::SPAWN_ITEM)] << " <x>:<y>";
			color = sf::Color::Red;
			msg = ss.str();
		}
	}
	break;

	case DBGCOMMAND::GENERATE_DUNGEON:
	{
		if (args.size() == 1)
		{
			mGame->Reset();

			ss << "Dungeon generated";
			color = sf::Color::Green;
			msg = ss.str();
		}
		else
		{
			ss << "Usage: " << DBGCOMMANDStrings[static_cast<int>(DBGCOMMAND::GENERATE_DUNGEON)] << " <featureNum>";
			color = sf::Color::Red;
			msg = ss.str();
		}
	}
	break;

	case DBGCOMMAND::RANDOMIZE_DUNGEON_COLOR:
	{
		if (args.size() == 0)
		{
			mDungeon->RandomizeColor();

			ss << "Dungeon color randomized";
			color = sf::Color::Green;
			msg = ss.str();
		}
		else
		{
			ss << "Usage: " << DBGCOMMANDStrings[static_cast<int>(DBGCOMMAND::RANDOMIZE_DUNGEON_COLOR)];
			color = sf::Color::Red;
			msg = ss.str();
		}
	}
	break;

	case DBGCOMMAND::INVALID:
	{
		color = sf::Color::Red;
		msg = "Invalid command: " + args[0];
	}
	break;

	default:
	{
		ss << "Unhandled command: " << static_cast<int>(cmd);
		color = sf::Color::Red;
		msg = ss.str();
	}
	break;
	}

	Print(msg, color);
}

void Console::ExecuteTypedCommand()
{
	if (!mTypedCommandText.getString().isEmpty())
	{
		std::string typedStr = mTypedCommandText.getString().toAnsiString();
		Keyboard::ClearTypedString();
		UpdateCurCommandString();

		Print(typedStr, sf::Color::White);

		std::vector<std::string> args = Octomorph::OctomorphHelpers::Split(typedStr, ' ');
		for (char& c : args[0]) c = toupper(c);
		std::string argsStr = args.size() > 1 ? args[1] : "";

		bool commandExecuted = false;
		for (int i = 0; i < ETOI(DBGCOMMAND::COUNT); i++)
		{
			if (args[0].compare(DBGCOMMANDStrings[i]) == 0)
			{
				commandExecuted = true;
				ExecuteCommand(ITOE(DBGCOMMAND, i), argsStr);
				break;
			}
		}

		if (!commandExecuted)
			ExecuteCommand(DBGCOMMAND::INVALID, args[0]);
	}
}

void Console::SetBackgroundSize(int width)
{
	sf::RenderWindow& window = Octomorph::Managers::ViewManager::GetInstance()->GetWindow();
	mBackgroundWidth = width + (static_cast<int>(mOffset.x) * 2);	// Add offset padding on both sides
	mBackground.setSize(sf::Vector2f(static_cast<float>(mBackgroundWidth), static_cast<float>(window.getSize().y)));
	mBackground.setPosition(sf::Vector2f(window.getSize().x - static_cast<float>(mBackgroundWidth), 0));
	mTypedCommandBackground.setSize(sf::Vector2f(static_cast<float>(mBackgroundWidth), static_cast<float>(mTypedCommandText.getCharacterSize() * 2)));
	mTypedCommandBackground.setPosition(sf::Vector2f(window.getSize().x - static_cast<float>(mBackgroundWidth), window.getSize().y - static_cast<float>(mTypedCommandText.getCharacterSize() * 2)));
	mTypedCommandText.setPosition(sf::Vector2f(window.getSize().x - mBackgroundWidth + mOffset.x, window.getSize().y - mTypedCommandText.getCharacterSize() - mOffset.y));
}

void Console::UpdateCurCommandString()
{
	mTypedCommandText.setString(Keyboard::GetTypedString());
	mTypedCommandText.setPosition(sf::Vector2f(Octomorph::Managers::ViewManager::GetInstance()->GetWindow().getSize().x - mTypedCommandText.getLocalBounds().width - mOffset.x, mTypedCommandText.getPosition().y));
}