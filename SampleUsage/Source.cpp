#include <Menu.h>

/*
#include <Octomorph/Headers.h>
#include <Octomorph/Input/Keyboard.h>
#include <Octomorph/Managers/ViewManager.h>

using namespace Octomorph::Input;
*/

int main()
{
	Menu menu = Menu(1280, 720, 32);
	/*
	int screenWidth = 640;
	int screenHeight = 480;
	Octomorph::Managers::ViewManager::GetInstance()->Initialize("Micro", screenWidth, screenHeight, 32, sf::Style::Titlebar);
	sf::RenderWindow& window = Octomorph::Managers::ViewManager::GetInstance()->GetWindow();

	sf::RectangleShape player;
	float playerSize = 25;
	float playerOutline = 2;
	player.setPosition(screenWidth / 2, screenHeight / 2);
	player.setSize(sf::Vector2f(playerSize, playerSize));
	player.setFillColor(sf::Color(0, 255, 255));
	player.setOutlineThickness(playerOutline);
	player.setOutlineColor(sf::Color::Black);

	sf::RectangleShape playerMirror;
	playerMirror.setPosition(screenWidth / 2, screenHeight / 2);
	playerMirror.setSize(sf::Vector2f(playerSize, playerSize));
	playerMirror.setFillColor(sf::Color(0, 255, 255));
	playerMirror.setOutlineThickness(playerOutline);
	playerMirror.setOutlineColor(sf::Color::Black);

	sf::Vector2f vel;
	float friction = 0.76f;
	float gravity = 5;
	float speed = 5;
	float jumpHeight = 10;
	float jumpStart = 0;
	bool grounded = false;
	bool jumping = false;

	while (window.isOpen())
	{
		Keyboard::Update();
		sf::Event e;
		while (window.pollEvent(e))
		{
			if (e.type == sf::Event::EventType::Closed || (e.type == sf::Event::EventType::KeyPressed && e.key.code == sf::Keyboard::Key::Escape))
			{
				window.close();
			}

			if (e.type == sf::Event::KeyPressed || e.type == sf::Event::KeyReleased)
			{
				Keyboard::KeyCallback(e.key.code);
			}
		}

		if (player.getPosition().y + player.getSize().y >= screenHeight)
		{
			player.setPosition(player.getPosition().x, screenHeight - player.getSize().y);
			grounded = true;
		}

		if (Keyboard::Key(sf::Keyboard::A))
			vel.x = -speed;
		if (Keyboard::Key(sf::Keyboard::D))
			vel.x = speed;
		if (Keyboard::KeyDown(sf::Keyboard::Space))
		{
			if (grounded)
			{
				jumping = true;
				grounded = false;
				jumpStart = player.getPosition().y;
				vel.y = -gravity;
			}
		}

		if (!grounded && !jumping)
			vel.y = gravity;
		if (jumping && player.getPosition().y < jumpStart - jumpHeight)
		{
			jumping = false;
		}

		player.setPosition(player.getPosition() + vel);
		playerMirror.setPosition(player.getPosition());

		sf::Vector2f mirrorPos = playerMirror.getPosition();
		float xOffset = player.getPosition().x + player.getSize().x - screenWidth;
		if (xOffset > 0)
		{
			mirrorPos.x = -playerMirror.getSize().x + xOffset;
		}
		else if (mirrorPos.x < 0)
		{
			mirrorPos.x = screenWidth + mirrorPos.x;
		}
		playerMirror.setPosition(mirrorPos);

		sf::Vector2f playerPos = player.getPosition();
		if (playerPos.x < -player.getSize().x ||
			playerPos.x > screenWidth)
		{
			playerPos.x = mirrorPos.x;
		}
		player.setPosition(playerPos);
		
		vel.x *= friction;
		
		window.clear(sf::Color(127, 0, 127));
		window.draw(player);
		window.draw(playerMirror);
		window.display();
	}
	*/
	return 0;
}