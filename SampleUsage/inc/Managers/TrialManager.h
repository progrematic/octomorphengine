#ifndef NEVEREND_TRIALMANAGER
#define NEVEREND_TRIALMANAGER

#include <Octomorph/Headers.h>

#include <Dungeon/Features/Rooms/TrialRoom.h>

#include <Octomorph/Interfaces/IMessageHandler.h>
#include <Octomorph/Messages//Message.h>

class TrialManager : public Octomorph::Interfaces::IMessageHandler
{
public:
	static TrialManager* GetInstance();
	~TrialManager();

	void Initialize();
	void Reset();

	void AddTrialRoom(std::shared_ptr<TrialRoom> trialRoom);
	bool IsTrialActive();

	bool HandleMessage(const Octomorph::Messages::Message& msg) override;

private:
	TrialManager();

private:
	static TrialManager* mInstance;
	std::vector<std::shared_ptr<TrialRoom>> mTrialRooms;
};

#endif
