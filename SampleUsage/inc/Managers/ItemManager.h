#ifndef NEVEREND_ITEMMANAGER
#define NEVEREND_ITEMMANAGER

#include <Octomorph/Headers.h>
#include <GameEnums.h>

#include <Objects/Mobs/Player.h>
#include <Objects/Items/Item.h>

class ItemManager
{
public:
	static ItemManager* GetInstance();

	void Reset();

	void Update(float dt);
	void Draw(float dt);

	void PlayerPickup(Player* player);

	const std::vector<std::unique_ptr<Item>>& GetItems();
	std::unique_ptr<Item> CreateItem(ITEM_TYPE type, int id, int count = 0);
	void SpawnItem(ITEM_TYPE type, int id, int count, sf::Vector2f pos);
	void SpawnItem(ITEM_TYPE type, int id, int count, sf::Vector2f pos, sf::Vector2f force);
	
private:
	ItemManager();

private:
	static ItemManager* mInstance;
	std::vector<std::unique_ptr<Item>> mItems;
};

#endif
