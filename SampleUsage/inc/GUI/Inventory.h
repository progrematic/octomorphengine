#ifndef NEVEREND_INVENTORY
#define NEVEREND_INVENTORY

#include <Octomorph/Headers.h>
#include <GameEnums.h>

#include <Octomorph/GUI/GUI.h>
#include <Octomorph/GUI/Widgets/Button.h>
#include <Objects/Items/Item.h>

using namespace Octomorph::GUI;
using namespace Octomorph::GUI::Widgets;

class Inventory
{
public:
	Inventory();

	void Initialize();
	void Reset();

	void Update(float dt);
	void Draw(float dt);

	void Show();
	void Hide();
	void ToggleShown();

	void SetWorldPos(sf::Vector2f worldPos);

	void AddItem(ITEM_TYPE type, int id, int count);

	bool IsFull();
	bool IsFull(int extras);

private:
	struct ItemBox
	{
		int row;
		int column;
		std::shared_ptr<Button> button;
		std::shared_ptr<Item> item;
	};

	void SelectBox(void* arg);
	void UpdateEmptyBox();
	void BuildGUI();
	void Drag();

private:
	GUI mGui;
	bool mShown;
	sf::Vector2f mWorldPos;
	std::vector<std::shared_ptr<ItemBox>> mItemBoxes;
	std::shared_ptr<ItemBox> mEmptyBox;
	std::shared_ptr<ItemBox> mSelectedBox;
	std::shared_ptr<Item> mMouseItem;
	int mItemLimit;
	int mFreeSpace;
	std::shared_ptr<Button> mBgButton;
	std::shared_ptr<TextLabel> mItemName;
	std::shared_ptr<TextLabel> mItemDesc;

	bool mDragging;
	sf::Vector2f mStartDragOffset;

	float mDropForce;

	int mItemBoxSize;
	int mItemSize;
	int mItemColumns;
	int mItemRows;
};

#endif
