#ifndef DUNGEON_H
#define DUNGEON_H

#include <Octomorph/Headers.h>
#include <Octomorph/Globals.h>
#include <Dungeon/Features/Room.h>
#include <Octomorph/Misc//Tilesheet.h>

#include <Octomorph/Interfaces/IMessageHandler.h>

class Dungeon : public Octomorph::Interfaces::IMessageHandler
{
public:
	Dungeon();
	~Dungeon();

	void Generate(int width, int height, int maxFeatures);
	void Print();

	void Update(float dt);
	void Draw(float dt);

	int GetWidth();
	int GetHeight();

	std::shared_ptr<Tile> GetTile(int x, int y);
	std::shared_ptr<Tile> GetTile(sf::Vector2f location);
	std::shared_ptr<Tile> GetAdjacentTile(std::shared_ptr<Tile> tile, DIRECTION dir);
	void SetTile(int x, int y, TILE_TYPE type);
	void SetTile(std::shared_ptr<Tile> tile, TILE_TYPE type);
	
	bool IsPassable(TILE_TYPE type);
	bool IsWall(TILE_TYPE type);
	std::shared_ptr<Room> IsRoom(std::shared_ptr<Tile> tile);

	std::shared_ptr<Room> GetSpawnRoom();
	std::shared_ptr<Room> GetRandomRoom();
	std::shared_ptr<Tile> GetRandomCorridor();

	void SmoothCorridors();
	int CalculateBitmask(int x, int y);
	void BitmaskTextures();
	void BitmaskTile(std::shared_ptr<Tile> tile);

	void RandomizeColor();

	virtual bool HandleMessage(const Octomorph::Messages::Message& msg) override;

private:
	bool IsTileValid(int x, int y);
	bool CreateFeature();
	bool CreateFeature(int x, int y, DIRECTION dir);
	bool MakeRoom(int x, int y, DIRECTION dir, bool firstRoom = false);
	bool MakeCorridor(int x, int y, DIRECTION dir);
	bool PlaceRect(const sf::IntRect& rect, TILE_TYPE type, bool useRoomWalls = false);
	void AssignExitToRooms(std::shared_ptr<Tile> tile);

private:
	int mWidth, mHeight;
	Octomorph::Misc::Tilesheet mTilesheet;
	std::vector<std::shared_ptr<Tile>> mTiles;
	std::vector<std::shared_ptr<Tile>> mCorridors;
	std::vector<std::shared_ptr<Room>> mRooms;
	std::vector<sf::IntRect> mWalls;
};

#endif
