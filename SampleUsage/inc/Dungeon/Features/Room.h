#ifndef FEATURE_H
#define FEATURE_H

#include <Octomorph/Headers.h>
#include <GameGlobals.h>

class Room : public std::enable_shared_from_this<Room>
{
public:
	Room();

	virtual void CopyFromRoom(std::shared_ptr<Room> other);

	virtual void Update(float dt) {}
	virtual void Draw(float dt) {}
	
	virtual void SetRect(sf::IntRect rect);
	sf::IntRect GetRect();

	ROOM_TYPE GetType();

	void SetDiscovered(bool x);
	bool GetDiscovered();

	std::vector<std::shared_ptr<Tile>> GetExits();
	void AddExit(std::shared_ptr<Tile> exit);

protected:
	ROOM_TYPE mType;
	sf::IntRect mRect;
	std::vector<std::shared_ptr<Tile>> mExits;

	bool mDiscovered;
};

#endif
