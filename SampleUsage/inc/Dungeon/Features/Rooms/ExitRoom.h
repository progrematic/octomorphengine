#ifndef NEVEREND_EXITROOM
#define NEVEREND_EXITROOM

#include <Octomorph/Headers.h>
#include <Octomorph/Globals.h>

#include <Dungeon/Features/Room.h>

class ExitRoom : public Room
{
public:
	ExitRoom();

	virtual void CopyFromRoom(std::shared_ptr<Room> other) override;

	virtual void Update(float dt) override;
	virtual void Draw(float dt) override;

	virtual void SetRect(sf::IntRect rect) override;

	void Unlock();
	void SetHazeColor(sf::Color c);

private:
	bool mLocked;

	float mTime;
	sf::Color mHazeColor;
	sf::Sprite mHazeSprite;
	sf::RenderTexture mHazeTexture;
	sf::Shader mHazeShader;
};

#endif
