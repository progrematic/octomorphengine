#ifndef NEVEREND_TRIALROOM
#define NEVEREND_TRIALROOM

#include <Octomorph/Headers.h>
#include <GameEnums.h>

#include <Dungeon/Features/Room.h>

class TrialRoom : public Room
{
public:
	TrialRoom();

	void SetTrialType(TRIAL_TYPE type);
	TRIAL_TYPE GetTrialType();

	bool IsTrialActive();

	void StartTrial();
	void EndTrial();

private:
	TRIAL_TYPE mTrialType;
	bool mActive;
};

#endif
