#ifndef NEVEREND_SPAWNROOM
#define NEVEREND_SPAWNROOM

#include <Octomorph/Headers.h>
#include <Octomorph/Globals.h>

#include <Dungeon/Features/Room.h>

class SpawnRoom : public Room
{
public:
	SpawnRoom();

private:

};

#endif
