#ifndef GAMEGLOBALS_H
#define GAMEGLOBALS_H

#include <Octomorph/Headers.h>
#include <GameEnums.h>

static int const TILE_SIZE = 64;

namespace GameHelpers
{
	inline ANIMATION_STATE GetAnimationForDirection(ANIMATION_STATE animation, DIRECTION dir)
	{
		ANIMATION_STATE ret = animation;

		// Animation state order is DLRU
		switch (dir)
		{
		case DIRECTION::NORTH:
		{
			ret = static_cast<ANIMATION_STATE>(static_cast<int>(animation) + 3);
		}
		break;
		case DIRECTION::SOUTH:
		{
			ret = static_cast<ANIMATION_STATE>(static_cast<int>(animation) + 0);
		}
		break;
		case DIRECTION::EAST:
		{
			ret = static_cast<ANIMATION_STATE>(static_cast<int>(animation) + 2);
		}
		break;
		case DIRECTION::WEST:
		{
			ret = static_cast<ANIMATION_STATE>(static_cast<int>(animation) + 1);
		}
		break;
		}

		return ret;
	}
}

struct Tile
{
	TILE_TYPE type;
	int columnIndex;
	int rowIndex;
	sf::Sprite sprite;
	int stepCount;

	// A* variables
	int H;				// Heuristic / movement cost to goal.
	int G;				// Movement cost. (Total of entire path)
	int F;				// Estimated cost for full path. (G + H)
	Tile* parentNode;	// Node to reach this node.
};

#endif