#ifndef GAME_H
#define GAME_H

#include <Octomorph/Headers.h>
#include <Dungeon/Dungeon.h>
#include <Octomorph/Managers/LightManager.h>
#include <Octomorph/Managers/SwarmManager.h>
#include <Objects/Object.h>
#include <Objects/Mobs/Player.h>

class Game
{
public:
	Game();

	void Initialize();
	void Reset();
	void Cleanup();

	void Update(float dt);
	void Draw(float dt);

	void GameOver();
	bool IsGameOver();

	bool ToggleLight();
	void SpawnPlayer(Octomorph::Enums::INPUT_TYPE input, int joystickId = -1);
	void SpawnTorch(sf::Vector2f pos);

private:
	enum class GAME_STATE
	{
		PLAYER_SELECT,
		GAMEPLAY,
		PAUSE,
		COUNT
	};

	void HandleInput(float dt);
	void HandleRoomEntry(std::shared_ptr<Room> room);
	void UpdateObjects(float dt);

private:
	GAME_STATE mState;
	bool mDrawLight;
	bool mGameOver;

	std::shared_ptr<Dungeon> mDungeon;
	std::vector<std::unique_ptr<Object>> mObjects;
	std::vector<std::shared_ptr<Player>> mPlayers;

	Octomorph::Managers::SwarmManager mSwarm;
};

#endif
