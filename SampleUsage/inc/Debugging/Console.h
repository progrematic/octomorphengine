#ifndef CONSOLE_H
#define CONSOLE_H

#include <Octomorph/Headers.h>
#include <Dungeon/Dungeon.h>
#include <Objects/Mobs/Player.h>

class Game;

class Console
{
public:
	static Console* GetInstance();

	void Initialize(Game* game, std::shared_ptr<Dungeon> dungeon);

	void Update(float dt);
	void Draw(float dt);

	void HandleInput(float dt);

	void Print(std::string msg, sf::Color c = sf::Color::Cyan);

	void SetActive(bool active);
	bool GetActive();

private:
	enum class DBGCOMMAND
	{
		INVALID,
		TOGGLE_LIGHTING,
		PLAYER_TELEPORT,
		PLAYER_LIGHT_RADIUS,
		PLAYER_LIGHT_WANING,
		END_TRIAL,
		SPAWN_TORCH,
		SPAWN_ITEM,
		GENERATE_DUNGEON,
		RANDOMIZE_DUNGEON_COLOR,
		COUNT
	};

	const char* DBGCOMMANDStrings[ETOI(DBGCOMMAND::COUNT)] = {
		"INVALID",
		"TOGGLELIGHT",
		"TELEPORTPLAYER",
		"PLAYERLIGHTRADIUS",
		"PLAYERLIGHTWANING",
		"ENDTRIAL",
		"SPAWNTORCH",
		"SPAWNITEM",
		"GENERATEDUNGEON",
		"RANDOMIZEDUNGEONCOLOR"
	};

	Console();
	void ExecuteCommand(DBGCOMMAND cmd, std::string argStr = "");
	void ExecuteCommand(DBGCOMMAND cmd, std::vector<std::string> args);
	void ExecuteTypedCommand();
	void SetBackgroundSize(int width);
	void UpdateCurCommandString();

private:
	static Console* mInstance;

	bool mActive;

	Game* mGame;
	std::shared_ptr<Dungeon> mDungeon;

	std::vector<sf::Text> mHistory;
	sf::Text mTypedCommandText;
	bool mTypingCommand;
	sf::Vector2f mOffset;
	sf::RectangleShape mBackground;
	sf::RectangleShape mTypedCommandBackground;
	sf::Color mBackgroundColor;
	sf::Color mTypedCommandBackgroundColor;
	int mBackgroundWidth;

	sf::Font mFont;
};

#endif

