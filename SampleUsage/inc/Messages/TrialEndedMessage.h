#ifndef NEVEREND_TRIALENDEDMESSAGE
#define NEVEREND_TRIALENDEDMESSAGE

#include <Octomorph/Headers.h>
#include <Octomorph/Globals.h>

#include <Octomorph/Messages//Message.h>
#include <Dungeon/Features/Rooms/TrialRoom.h>

class TrialEndedMessage : public Octomorph::Messages::Message
{
public:
	TrialEndedMessage() { mType = ETOI(MESSAGE_TYPE::TRIAL_ENDED); mRoom = nullptr; }

	void Setup(std::shared_ptr<TrialRoom> room) { mRoom = room; }
	std::shared_ptr<TrialRoom> GetRoom() const { return mRoom; }
private:
	std::shared_ptr<TrialRoom> mRoom;
};

#endif
