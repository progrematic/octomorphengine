#ifndef NEVEREND_TRIALSTARTEDMESSAGE
#define NEVEREND_TRIALSTARTEDMESSAGE

#include <Octomorph/Headers.h>
#include <Octomorph/Globals.h>

#include <Octomorph/Messages//Message.h>
#include <Dungeon/Features/Rooms/TrialRoom.h>

class TrialStartedMessage : public Octomorph::Messages::Message
{
public:
	TrialStartedMessage() {	mType = ETOI(MESSAGE_TYPE::TRIAL_STARTED); mRoom = nullptr; }

	void Setup(std::shared_ptr<TrialRoom> room) { mRoom = room; }
	std::shared_ptr<TrialRoom> GetRoom() const { return mRoom; }
private:
	std::shared_ptr<TrialRoom> mRoom;
};

#endif
