#ifndef OBJECT_H
#define OBJECT_H

#include "Octomorph/Headers.h"
#include "Octomorph/Components/Component.h"

class Object
{
public:
	Object();

	virtual void Update(float dt) = 0;
	virtual void Draw(float dt) = 0;

	template<typename T>
	std::shared_ptr<T> AttachComponent()
	{
		std::shared_ptr<T> newComponent = std::make_shared<T>();

		for (std::shared_ptr<Octomorph::Components::Component>& existingComponent : mComponents)
		{
			if (std::dynamic_pointer_cast<T>(existingComponent))
			{
				existingComponent = newComponent;
				return newComponent;
			}
		}

		mComponents.push_back(newComponent);
		return newComponent;
	};

	template<typename T>
	std::shared_ptr<T> GetComponent()
	{
		for (std::shared_ptr<Octomorph::Components::Component> existingComponent : mComponents)
		{
			if (std::dynamic_pointer_cast<T>(existingComponent))
			{
				return std::dynamic_pointer_cast<T>(existingComponent);
			}
		}

		return nullptr;
	};

private:
	std::vector<std::shared_ptr<Octomorph::Components::Component>> mComponents;
};

#endif
