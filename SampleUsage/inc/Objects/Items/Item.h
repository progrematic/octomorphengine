#ifndef NEVEREND_ITEM
#define NEVEREND_ITEM

#include <Octomorph/Headers.h>
#include <GameEnums.h>

#include <Objects/Object.h>
#include <Octomorph/Misc//Tilesheet.h>

class Item : public Object
{
public:
	Item();

	virtual void LoadItem(int id) = 0;

	virtual void Update(float dt) override;
	virtual void Draw(float dt) override;

	sf::Vector2f GetPos();
	virtual void SetPos(sf::Vector2f pos);

	int GetId();
	ITEM_TYPE GetType();
	void SetType(ITEM_TYPE type);
	int GetValue();
	void SetValue(int value);
	int GetCost();
	void SetCost(int cost);
	int GetCount();
	int SetCount(int count);
	int GetMaxStack();
	void SetMaxStack(int stack);

	std::string GetName();
	std::string GetDesc();

	void SetBounce(bool bounce);
	void SetShake(bool shake);

protected:
	ITEM_TYPE mType;
	int mId;
	int mCount;
	int mMaxStack;

	std::string mName;
	std::string mDesc;
	int mValue;
	int mCost;

	Octomorph::Misc::Tilesheet mTilesheet;
	sf::Sprite mSprite;

private:
	bool mShouldShake;
	bool mShouldBounce;
	sf::Vector2f mDisplayPos;
	float mT;
	float mAmplitude;
};

#endif
