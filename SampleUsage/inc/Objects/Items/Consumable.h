#ifndef NEVEREND_CONSUMABLE
#define NEVEREND_CONSUMABLE

#include <Octomorph/Headers.h>
#include <Octomorph/Globals.h>

#include <Objects/Items/Item.h>

class Consumable : public Item
{
public:
	Consumable();

	virtual void Update(float dt) override;
	virtual void Draw(float dt) override;
	virtual void SetPos(sf::Vector2f pos) override;

	virtual void LoadItem(int id);
	
private:
	CONSUMABLE_TYPE mConsumableType;
};

#endif
