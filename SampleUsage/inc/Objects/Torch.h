#ifndef TORCH_H
#define TORCH_H

#include "Octomorph/Headers.h"
#include "Octomorph/Particles/ParticleEmitter.h"
#include "Objects/Object.h"

class Torch : public Object
{
public:
	Torch();

	void Update(float dt) override;
	void Draw(float dt) override;

	sf::Vector2f GetPos();
	void SetPos(sf::Vector2f pos);

private:
	int mTorchTextureId;
	int mParticleTextureId;
	sf::Vector2f mPos;
	Octomorph::Particles::ParticleEmitter mParticleEmitter;
};

#endif
