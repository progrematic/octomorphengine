#ifndef PLAYER_H
#define PLAYER_H

#include <Octomorph/Headers.h>
#include <GameGlobals.h>
#include <Objects/Mobs/Mob.h>
#include <Objects/Items/Item.h>
#include <GUI/Inventory.h>

class Player : public Mob
{
public:
	Player();

	void Reset();
	void SetId(int id);
	int GetId();

	void HandleInput(float dt);
	void Update(float dt) override;
	void Draw(float dt) override;

	Inventory& GetInventory();
	bool IsInventoryFull(int incoming);
	void PickupItem(std::unique_ptr<Item> item);

	virtual void SetPos(sf::Vector2f pos) override;

private:
	void UpdateItems(float dt);

private:
	int mId;
	std::shared_ptr<Tile> mLastSteppedTile;
	std::vector<std::unique_ptr<Item>> mWorldItems;

	float mPickupSpeed;

	Inventory mInventory;
};

#endif
