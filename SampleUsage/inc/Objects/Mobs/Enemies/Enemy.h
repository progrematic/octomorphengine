#ifndef NEVEREND_ENEMY
#define NEVEREND_ENEMY

#include <Octomorph/Headers.h>
#include <Octomorph/Globals.h>

#include <Objects/Mobs/Mob.h>

class Enemy : public Mob
{
public:

	Enemy();

	void Update(float dt) override;
	void Draw(float dt) override;

	virtual void SetPos(sf::Vector2f pos) override;

private:

};

#endif
