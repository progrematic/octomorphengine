#ifndef MOB_H
#define MOB_H

#include <Octomorph/Headers.h>
#include <Octomorph/Globals.h>
#include <Dungeon/Dungeon.h>
#include <Objects/Object.h>

#include <map>

class Mob : public Object
{
public:
	Mob();

	void Initialize(std::shared_ptr<Dungeon> dungeon);

	void Update(float dt) override;
	void Draw(float dt) override;

	float GetStat(STAT stat);
	sf::Vector2f GetPos();
	virtual void SetPos(sf::Vector2f pos);
	void ApplyVel();
	void SetLookTarget(sf::Vector2f lookTarget);

	virtual sf::Vector2f ResolveCollision();

protected:
	virtual bool CheckCollision(sf::Vector2f newPos);

protected:
	std::shared_ptr<Dungeon> mDungeon;
	std::shared_ptr<Tile> mLastCollidedTile;

	std::map<STAT, float> mStatsList;
	sf::Vector2f mLookTarget;
	DIRECTION mLookDirection;
};

#endif
