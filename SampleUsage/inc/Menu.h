#ifndef NEVEREND_MENU
#define NEVEREND_MENU

#include <Octomorph/Headers.h>
#include <Octomorph/Globals.h>

#include <Game.h>
#include <Octomorph/GUI/GUI.h>

class Menu
{
public:
	Menu(int width, int height, int bitDepth);

private:
	enum class MENU_STATE
	{
		MENU,
		HOST,
		JOIN,
		LOBBY,
		GAME,
		COUNT
	};
	typedef std::map<MENU_STATE, std::shared_ptr<GUI>> GuiMap;
	typedef std::pair<MENU_STATE, std::shared_ptr<GUI>> GuiPair;

	void RunGame();

	void SetState(MENU_STATE state);

	void PlayGame();
	void Host();
	void Join();
	void BackToMenu();
	void Quit();

	void BuildBgGUI();
	void BuildMenuGUI();
	void BuildHostGUI();
	void BuildJoinGUI();
	void BuildLobbyGUI();

private:
	bool mRunning;

	int mWidth;
	int mHeight;

	sf::Clock mDTClock;
	Game mGame;
	MENU_STATE mState;

	GUI mBgGui;
	GuiMap mGuis;
};

#endif
