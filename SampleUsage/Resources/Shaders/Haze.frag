uniform float time;
uniform vec4 color;
uniform vec2 size;
uniform float fade;

float rand(vec2 co)
{
    return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
}

void main(void)
{
	vec4 pixel = color;
	vec2 pos = gl_FragCoord.xy;

	time *= 10;

	float x = sin(time + (pos.x * 0.05));
	float y = sin(time + (pos.y * 0.5));
	pixel.a = (x + y) / 2;

	// Check fade
	float dist;

	dist = fade - pos.x;
	if (dist > 0)
	{
		pixel.a -= (dist / fade);
	}
	dist = fade - pos.y;
	if (dist > 0)
	{
		pixel.a -= (dist / fade);	
	}
	dist = pos.x - (size.x - fade);
	if (dist > 0)
	{
		pixel.a -= (dist / fade);
	}
	dist = pos.y - (size.y - fade);
	if (dist > 0)
	{
		pixel.a -= (dist / fade);
	}

	gl_FragColor = pixel;
}