uniform int pixelSize;
uniform int arrSize;
uniform vec2 posArr[100];
uniform vec2 sizArr[100];
uniform float radArr[100];
uniform float wanArr[100];
uniform vec4 colArr[100];

void main(void)
{
	vec4 pixel;
	pixel.r = 0;
	pixel.g = 0;
	pixel.b = 0;
	pixel.a = 1;

	vec2 pos = gl_FragCoord.xy;

	// Map to pixelSize
	if (mod(pos.x, pixelSize) != 0.0)
	{
		pos.x = (floor(pos.x / pixelSize)) * pixelSize;
	}
	
	if (mod(pos.y, pixelSize) != 0.0)
	{
		pos.y = (floor(pos.y / pixelSize)) * pixelSize;
	}
	
	for (int i = 0; i < arrSize; i++)
	{
		if (sizArr[i].x > 0 || sizArr[i].y > 0)
		{
			if (pos.x > posArr[i].x - (sizArr[i].x / 2) &&
				pos.x < posArr[i].x + (sizArr[i].x / 2) &&
				pos.y > posArr[i].y - (sizArr[i].y / 2) &&
				pos.y < posArr[i].y + (sizArr[i].y / 2))
			{
				pixel.r += colArr[i].x;
				pixel.g += colArr[i].y;
				pixel.b += colArr[i].z;
				pixel.a -= 1;
			}
			else
			{
				float dist = 0;
				if (pos.y > posArr[i].y - (sizArr[i].y / 2) &&
					pos.y < posArr[i].y + (sizArr[i].y / 2))
				{
					// Horizontal waning
					if (pos.x < posArr[i].x)
					{
						vec2 distPos = pos;
						distPos.x = posArr[i].x - (sizArr[i].x / 2);
						dist = length(distPos - pos);
					}
					else
					{
						vec2 distPos = pos;
						distPos.x = posArr[i].x + (sizArr[i].x / 2);
						dist = length(distPos - pos);
					}
				}
				else if (pos.x > posArr[i].x - (sizArr[i].x / 2) &&
						 pos.x < posArr[i].x + (sizArr[i].x / 2))
				{
					// Vertical waning
					if (pos.y < posArr[i].y)
					{
						vec2 distPos = pos;
						distPos.y = posArr[i].y - (sizArr[i].y / 2);
						dist = length(distPos - pos);
					}
					else
					{
						vec2 distPos = pos;
						distPos.y = posArr[i].y + (sizArr[i].y / 2);
						dist = length(distPos - pos);
					}
				}
				else
				{
					// Diagonal waning
					if (pos.y < posArr[i].y)
					{
						if (pos.x < posArr[i].x)
						{
							// Top-left diagonal
							vec2 distPos;
							distPos.x = posArr[i].x - (sizArr[i].x / 2);
							distPos.y = posArr[i].y - (sizArr[i].y / 2);
							dist = length(distPos - pos);
						}
						else
						{
							// Top-right diagonal
							vec2 distPos;
							distPos.x = posArr[i].x + (sizArr[i].x / 2);
							distPos.y = posArr[i].y - (sizArr[i].y / 2);
							dist = length(distPos - pos);
						}
					}
					else
					{
						if (pos.x < posArr[i].x)
						{
							// Bot-left diagonal
							vec2 distPos;
							distPos.x = posArr[i].x - (sizArr[i].x / 2);
							distPos.y = posArr[i].y + (sizArr[i].y / 2);
							dist = length(distPos - pos);
						}
						else
						{
							// Bot-right diagonal
							vec2 distPos;
							distPos.x = posArr[i].x + (sizArr[i].x / 2);
							distPos.y = posArr[i].y + (sizArr[i].y / 2);
							dist = length(distPos - pos);
						}
					}
				}

				if (dist < wanArr[i])
				{
					// Now that we have a distance we care about, let's calculate the pixel data
					pixel.r += colArr[i].x;
					pixel.g += colArr[i].y;
					pixel.b += colArr[i].z;
					pixel.a -= 1 - (dist / wanArr[i]);
				}
			}
		}
		else
		{
			float dist = length(posArr[i] - pos);
			if (dist < radArr[i])
			{
				pixel.r += colArr[i].x;
				pixel.g += colArr[i].y;
				pixel.b += colArr[i].z;
				pixel.a -= 1;
			}
			else if (dist < radArr[i] + wanArr[i])
			{
				dist -= radArr[i];
				pixel.r += colArr[i].x;
				pixel.g += colArr[i].y;
				pixel.b += colArr[i].z;
				pixel.a -= 1 - (dist / wanArr[i]);
			}
		}
		
		if (pixel.a < 0)
			pixel.a = 0;
	}
	
	gl_FragColor = pixel;
}