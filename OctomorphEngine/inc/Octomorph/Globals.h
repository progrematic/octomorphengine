#ifndef OCTOMORPH_GLOBALS_H
#define OCTOMORPH_GLOBALS_H

#include <Octomorph/Enums.h>
#include <random>

// MACROS
/// \brief Statically casts an Enum value to int
#define ETOI(x) static_cast<int>(x)
/// \brief Statically casts an int (y) into an Enum value (x)
#define ITOE(x, y) static_cast<x>(y)

namespace Octomorph
{
	static int const MAX_PLAYERS = 4;	// Update Enums' VIEW enum to match player num

	namespace OctomorphRandom // From http://www.roguebasin.com/index.php?title=C%2B%2B_Example_of_Dungeon-Building_Algorithm
	{
		static std::random_device rd;
		static std::mt19937 mt(rd());

		/// \brief Gets a random integer - [0 to exclusiveMax)
		inline int GetInt(int exclusiveMax)
		{
			std::uniform_int_distribution<> dist(0, exclusiveMax - 1);
			return dist(mt);
		}

		/// \brief Gets a random integer - [min to max]
		inline int GetInt(int min, int max)
		{
			std::uniform_int_distribution<> dist(0, max - min);
			return dist(mt) + min;
		}

		/// \brief Gets a random bool
		/// \param probability The change to roll a true - should be a number between 0 and 1
		inline bool GetBool(double probability = 0.5)
		{
			std::bernoulli_distribution dist(probability);
			return dist(mt);
		}
	}

	namespace OctomorphMath
	{
#define PI 3.14159

		/// \brief Calculates Pythagorean Theorem on Vectors a and b
		inline float DistanceBetweenPoints(sf::Vector2f a, sf::Vector2f b)
		{
			// Our good ol' buddy Pythag
			return (abs(sqrt(((a.x - b.x) * (a.x - b.x)) + ((a.y - b.y) * (a.y - b.y)))));
		}

		/// \brief Converts degrees to radians
		inline float ToRadians(float deg)
		{
			return (float)((deg * PI) / 180.f);
		}

		/// \brief Converts radians to degrees
		inline float ToDegrees(float rad)
		{
			return (float)((rad * 180.f) / PI);
		}

		/// \brief Whether or not a floating-point value is near the target
		/// \param x The float value
		/// \param y The target value
		/// \param error The margin of error used to determine what is considered "around"
		inline bool IsAround(float x, float y, float error)
		{
			return abs(x - y) < abs(error);
		}

		/// \brief Standard min function
		inline float Min(float x, float y)
		{
			if (x < y)
				return x;
			return y;
		}

		/// \brief Standard max function
		inline float Max(float x, float y)
		{
			if (x > y)
				return x;
			return y;
		}

		/// \brief Rounds a floating-point value to the nearest integer
		inline float Round(float x)
		{
			return floor(x + 0.5f);
		}

		/// \brief Moves a vector towards the target at the provided speed (think LERP)
		inline sf::Vector2f MoveTowards(sf::Vector2f current, sf::Vector2f target, float speed)
		{
			sf::Vector2f ret = target;

			if (!OctomorphMath::IsAround(current.x, target.x, 1.f) ||
				!OctomorphMath::IsAround(current.y, target.y, 1.f))
			{
				sf::Vector2f v = target - current;
				float distance = DistanceBetweenPoints(current, target);
				if (distance < speed)
					distance = speed;

				// Normalize vector to get speed ratios
				v.x /= distance;
				v.y /= distance;

				ret.x = current.x + (v.x * speed);
				ret.y = current.y + (v.y * speed);
			}

			return ret;
		}

		/// \brief Gets the length of the given vector
		inline float Length(sf::Vector2f vec)
		{
			return abs(sqrt(vec.x * vec.x + vec.y * vec.y));
		}

		/// \brief Normalizes a vector then multiplies by mul
		/// \param vec The vector to normalize
		/// \param mul The multiplication factor to apply after normalization - useful for normalizing direction and then applying a speed
		inline sf::Vector2f Normalize(sf::Vector2f vec, float mul = 1)
		{
			float length = Length(vec);
			if (length != 0)
			{
				vec.x /= length;
				vec.y /= length;
			}
			vec.x *= mul;
			vec.y *= mul;
			return vec;
		}

		/// \brief Caps a vector's length to the limit provided
		/// \param The vector to limit
		/// \param The length to cap the vector to
		/// \return A vector with the same direction as vec, limited in length to limit.
		inline sf::Vector2f Limit(sf::Vector2f vec, float limit)
		{
			if (pow(Length(vec), 2) > limit)
			{
				vec = Normalize(vec, limit);
			}

			return vec;
		}

		/// \brief Calculates the angle of the provided vector
		inline float GetHeading(sf::Vector2f vec, bool returnDegrees = true)
		{
			float heading = (float)atan2(vec.y, vec.x);

			return returnDegrees ? ToDegrees(heading) : heading;
		}

		/// \brief Maps a floating point value from one number range to another
		/// \param x The value to map
		/// \param min1 The minimum value of the "from" number range
		/// \param max1 The maximum value of the "from" number range
		/// \param min2 The minimum value of the "to" number range
		/// \param max2 The maximum value of the "to" number range
		/// \return The floating-point value in the new number range
		inline float Map(float x, float min1, float max1, float min2, float max2)
		{
			float slope = (max2 - min2) / (max1 - min1);
			return min2 + Round(slope * (x - min1));
		}

		/// \brief Whether or not the provided point is within the provided rect
		inline bool IsPointInRect(sf::Vector2f point, sf::FloatRect rc)
		{
			bool ret = false;
			if (point.x >= rc.left && point.x <= rc.left + rc.width &&
				point.y >= rc.top && point.y <= rc.top + rc.height)
			{
				ret = true;
			}

			return ret;
		}

		/// \brief Whether or not the provided rects are colliding
		inline bool AreRectsColliding(sf::FloatRect a, sf::FloatRect b)
		{
			bool ret = false;
			if (a.left < b.left + b.width && a.left + a.width > b.left &&
				a.top < b.top + b.height && a.top + a.height > b.top)
			{
				ret = true;
			}

			return ret;
		}
	}

	namespace OctomorphHelpers
	{
		/// \brief Splits a string by the provided delimiter and stores the result in T
		/// \param result Should be some sort of std inserter.
		/// Avoid using this method to split a string. Use the other Split helper method, which uses this one.
		template<typename T>
		inline void Split(const std::string& s, char delim, T result)
		{
			std::stringstream ss;
			ss.str(s);
			std::string item;
			while (std::getline(ss, item, delim)) {
				*(result++) = item;
			}
		}

		/// \brief Splits a string by the provided delimiter
		/// \return The elements within the provided string
		inline std::vector<std::string> Split(const std::string& s, char delim)
		{
			std::vector<std::string> elements;
			Split(s, delim, std::back_inserter(elements));
			return elements;
		}
	}
}

#endif
