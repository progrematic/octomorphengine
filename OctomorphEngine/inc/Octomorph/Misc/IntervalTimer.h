#ifndef OCTOMORPH_INTERVALTIMER
#define OCTOMORPH_INTERVALTIMER

#include <Octomorph/Headers.h>
#include <Octomorph/Globals.h>

#include <functional>

namespace Octomorph
{
	namespace Misc
	{
		/// \brief A class used to hit a callback at a set rate
		class IntervalTimer
		{
		public:

			IntervalTimer();

			/// \brief Initialize the timer and define the rate and callback
			/// \param interval The time between callback hits in seconds
			/// \param callback A void(void) callback
			void Initialize(float interval, std::function<void(void)> callback);

			/// \brief Starts the interval timer
			void Start();
			/// \brief Stops the interval timer
			void Stop();

			/// \brief Updates the internal elapsed time to determine if the callback should be called
			void Update(float dt);

		private:
			std::function<void(void)> mCallback;
			float mInterval;
			float mElapsed;

			bool mEnabled;
		};
	}
}

#endif
