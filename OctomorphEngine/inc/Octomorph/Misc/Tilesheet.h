#ifndef OCTOMORPH_TILESHEET
#define OCTOMORPH_TILESHEET

#include <Octomorph/Headers.h>
#include <Octomorph/Globals.h>

namespace Octomorph
{
	namespace Misc
	{
		/// \brief A class used to load tilesheets from xml files

		/// Quick example of a tilesheet xml
		/// \code{.xml}
		/// {
		/// 	"horTiles": 4,
		/// 	"verTiles" : 6,
		/// 	"colWidth" : 64,
		/// 	"colHeight" : 64,
		/// 	"frameWidth" : 64,
		/// 	"frameHeight" : 64
		/// }
		/// \endcode
		class Tilesheet
		{
		public:
			Tilesheet();

			/// \brief Load the tilesheet
			/// \param tilesheetPath The path of the tilesheet
			/// \param jsonPath The path to the accompanying json file
			void Load(std::string tilesheetPath, std::string jsonPath);
			/// \brief Gets the tile from the tilesheet at the given index
			/// \param index The index into the tilesheet
			/// \param sprite A Sprite reference which gets its texture set based on the tilesheet
			void GetTile(int index, sf::Sprite& sprite);

		private:
			sf::Vector2i GetFrameFromIndex(int index);

		private:
			sf::Sprite mSpritesheet;

			int mHorTiles;
			int mVerTiles;
			int mTileWidth;
			int mTileHeight;
		};
	}
}

#endif
