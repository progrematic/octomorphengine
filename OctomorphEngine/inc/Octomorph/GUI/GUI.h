#ifndef OCTOMORPH_GUI
#define OCTOMORPH_GUI

#include <Octomorph/Headers.h>
#include <Octomorph/Globals.h>
#include <Octomorph/GUI/Widgets/Widget.h>
#include <Octomorph/GUI/Widgets/Image.h>
#include <Octomorph/Interfaces/IEventHandler.h>

namespace Octomorph
{
	namespace GUI
	{
		/// \brief A collection of Widgets encapsulated within a frame
		class GUI
		{
		public:
			GUI();
			GUI(sf::Vector2f pos);

			/// \brief Clears all initialized state
			void Reset();

			/// \brief Sets the GUI frame background color
			void SetColor(sf::Color color);
			/// \brief Sets the GUI frame background image
			void SetImage(sf::Texture& texture);
			/// \brief Enables all tabbable Widgets and adds each Widget as a handler to the EventManager
			void Enable();
			/// \brief Disables all tabbable Widgets and removes each Widget as a handler to the EventManager
			void Disable();
			/// \brief Toggles between Enable() and Disable()
			void ToggleEnabled();
			/// \brief Whether or not the GUI's enabled
			bool IsEnabled();

			/// \brief Updates the active Tabbable component to the next one in the GUI
			void NextTabbable();
			/// \brief Updates the active Tabbable component to the previous one in the GUI
			void PrevTabbable();

			sf::Vector2f GetPos();
			void SetPos(sf::Vector2f pos);

			/// \brief GUI backgrounds are automatically resized based on the position of the Widgets inside of it.
			/// BorderSize adds a buffer on the X and Y axis
			void SetBorderSize(sf::Vector2f border);

			/// \brief Gets the rect that's calculated based on the position of the Widgets inside of it.
			sf::FloatRect GetRect();

			/// \brief Adds a Widget to this GUI.
			/// \param widget A shared_ptr to the Widget
			/// \param registerEventManager Whether or not this Widget needs to register to the Event Manager
			/// \param tabbable Whether or not this Widget reacts to being tabbed to
			void AddWidget(std::shared_ptr<Widgets::Widget> widget, bool registerEventManager = true, bool tabbable = false);

			/// \brief Loads a GUI from a json file
			void LoadGUIFromFile(std::string path);

			void Update(float dt);
			void Draw(float dt);

		private:
			bool mEnabled;
			// The GUI position
			sf::Vector2f mPos;
			// The x/y buffer added to the background rect when calculating the rect based on the Widget positions
			sf::Vector2f mBorderSize;
			// The background rectangle
			sf::RectangleShape mShape;
			// A background image - if applicable
			std::shared_ptr<Widgets::Image> mBg;
			// The list of Widgets
			std::vector<std::shared_ptr<Widgets::Widget>> mWidgets;
			// The list of Tabbable IEventHandlers
			std::vector<std::shared_ptr<Interfaces::IEventHandler>> mTabbables;
			// The list of EventManager-registered IEventHandlers
			std::vector<std::shared_ptr<Interfaces::IEventHandler>> mRegisters;
			// The currently-active IEventHandler - driven by the Tabbable flow
			std::shared_ptr<Interfaces::IEventHandler> mCurActiveHandler;
		};
	}
}

#endif
