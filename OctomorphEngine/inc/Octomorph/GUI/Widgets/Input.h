#ifndef OCTOMORPH_INPUT
#define OCTOMORPH_INPUT

#include <Octomorph/Headers.h>
#include <Octomorph/Globals.h>

#include <Octomorph/GUI/Widgets/Widget.h>
#include <Octomorph/GUI/Widgets/TextLabel.h>
#include <Octomorph/Misc//IntervalTimer.h>
#include <Octomorph/Interfaces/IEventHandler.h>

namespace Octomorph
{
	namespace GUI
	{
		namespace Widgets
		{
			/// \brief A Widget used to capture text from the user
			///
			/// This widget listens to Events and updates its internal string if it's active
			class Input : public Widget, public Interfaces::IEventHandler
			{
			public:
				Input();

				/// \brief Called by the main GUI Update method, this is used to drive any logic running on a loop
				/// \param guiPos The global GUI position, since Widget positions are offset from the GUI's position
				virtual void Update(float dt, sf::Vector2f guiPos);
				/// \brief Called by the main GUI Draw method, this is used to draw this button and its TextLabel
				/// \param guiPos The global GUI position, since Widget positions are offset from the GUI's position
				virtual void Draw(float dt, sf::Vector2f guiPos);
				/// \brief Called by the main GUI Draw method, this is used to draw this Widget to the supplied Texture
				/// \param texture The texture to draw this widget onto
				virtual void Draw(float dt, sf::RenderTexture& texture);

				/// \brief Sets the active flag. Adds/Removes itself as a handler to the EventManager
				/// \param x Whether or not we're activating this IEventHandler
				virtual void SetActive(bool x) override;

				/// \brief Sets the callback to execute on ENTER press
				/// \param callback An std::function that takes a const std::string& and returns a void
				virtual void SetCallback(std::function<void(const std::string&)> callback);

				/// \brief Listens for the SFML TextEntered event which contains the character that was typed.
				/// Only called from the EventManager if this IEventHandler is set to Active, 
				/// \param e The SFML event
				virtual bool HandleEvent(sf::Event e);

			private:
				void ToggleCursor();

			private:
				// A TextLabel widget that's drawn on top of the button
				TextLabel mLabel;
				// A string representing what the user has inputted so far
				sf::String mString;
				// The (usually white) rectangle used to define the Input
				sf::RectangleShape mShape;
				// A thin rectangle which blinks to simulate the carot
				sf::RectangleShape mCursor;
				// The outline of the white rectangle
				float mOutlineThickness;
				// An interval timer used to blink the carot
				Misc::IntervalTimer mIntervalTimer;
				// Whether or not we should draw the carot
				bool mDrawCursor;
				// The callback
				std::function<void(const std::string&)> mCallback;
			};
		}
	}
}

#endif
