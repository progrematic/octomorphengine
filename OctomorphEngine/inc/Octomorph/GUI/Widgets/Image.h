#ifndef OCTOMORPH_IMAGE
#define OCTOMORPH_IMAGE

#include <Octomorph/Headers.h>
#include <Octomorph/Globals.h>

#include <Octomorph/GUI/Widgets/Widget.h>

namespace Octomorph
{
	namespace GUI
	{
		namespace Widgets
		{
			/// \brief A Widget used to draw an image
			///
			/// This widget keeps track of a Sprite used exclusively for drawing on a GUI
			class Image : public Widget
			{
			public:
				Image();

				/// \brief Sets up the Sprite using the texture being passed in
				/// \param texture The texture to load into the sprite
				void SetImage(sf::Texture& texture);

				/// \brief Called by the main GUI Update method, this is used to drive any logic running on a loop
				/// \param guiPos The global GUI position, since Widget positions are offset from the GUI's position
				virtual void Update(float dt, sf::Vector2f guiPos);
				/// \brief Called by the main GUI Draw method, this is used to draw this button and its TextLabel
				/// \param guiPos The global GUI position, since Widget positions are offset from the GUI's position
				virtual void Draw(float dt, sf::Vector2f guiPos);
				/// \brief Called by the main GUI Draw method, this is used to draw this Widget to the supplied Texture
				/// \param texture The texture to draw this widget onto
				virtual void Draw(float dt, sf::RenderTexture& texture);

			private:
				sf::Sprite mSprite;
			};
		}
	}
}

#endif
