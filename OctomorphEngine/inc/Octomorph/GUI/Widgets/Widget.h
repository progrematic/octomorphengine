#ifndef OCTOMORPH_WIDGET
#define OCTOMORPH_WIDGET

#include <Octomorph/Headers.h>
#include <Octomorph/Globals.h>

namespace Octomorph
{
	namespace GUI
	{
		namespace Widgets
		{
			/// \brief A component that makes up a GUI.
			///
			/// Contains information for how to draw and position widgets
			class Widget
			{
			public:
				Widget();

				/// \brief Sets the real position of the Widget relative to the GUI. Recalculates the final position again.
				/// \param pos The position relative to the GUI
				virtual void SetPos(sf::Vector2f pos);
				/// \brief Sets the size of the widget. Recalculates the final position again.
				virtual void SetSize(sf::Vector2f size);
				
				virtual sf::Vector2f GetPos();
				virtual sf::Vector2f GetSize();

				/// \brief Sets the anchor (centerpoint) of the widget - Recalculates the final position again.
				void SetAnchor(sf::Vector2f anchor);
				/// \brief Gets the real position without the anchor piece
				/// \return The real position
				sf::Vector2f GetRealPos();
				/// \brief Gets the position at the anchor
				/// \return The position at the anchor
				sf::Vector2f GetPosAtAnchor(sf::Vector2f anchor);

				/// \brief Called by the main GUI Update method, this is used to drive any logic running on a loop
				/// \param guiPos The global GUI position, since Widget positions are offset from the GUI's position
				virtual void Update(float dt, sf::Vector2f guiPos) = 0;
				/// \brief Called by the main GUI Draw method, this is used to draw this Widget to the screen
				/// \param guiPos The global GUI position, since Widget positions are offset from the GUI's position
				virtual void Draw(float dt, sf::Vector2f guiPos) = 0;
				/// \brief Called by the main GUI Draw method, this is used to draw this Widget to the supplied Texture
				/// \param texture The texture to draw this widget onto
				virtual void Draw(float dt, sf::RenderTexture& texture) = 0;
				/// \brief Used to draw what we believe the position and anchors are internally
				virtual void DebugDraw(float dt, sf::Vector2f guiPos);
				/// \brief Used to draw what we believe the position and anchors are internally
				virtual void DebugDraw(float dt, sf::RenderTexture& texture);

			private:
				void UpdatePos();

			protected:
				sf::Vector2f mPos;
				sf::Vector2f mSize;
				sf::Vector2f mAnchor;

			private:
				sf::Vector2f mRealPos;
			};
		}
	}
}

#endif
