#ifndef OCTOMORPH_TEXTLABEL
#define OCTOMORPH_TEXTLABEL

#include <Octomorph/Headers.h>
#include <Octomorph/Globals.h>
#include <Octomorph/GUI/Widgets/Widget.h>

namespace Octomorph
{
	namespace GUI
	{
		namespace Widgets
		{
			/// \brief A Widget used to print text to the GUI.
			///
			/// This Widget stores and draws text on the screen.
			class TextLabel : public Widget
			{
			public:
				TextLabel();

				/// \brief Called by the main GUI Update method, this is used to drive any logic running on a loop
				/// \param guiPos The global GUI position, since Widget positions are offset from the GUI's position
				virtual void Update(float dt, sf::Vector2f guiPos);
				/// \brief Called by the main GUI Draw method, this is used to draw this button and its TextLabel
				/// \param guiPos The global GUI position, since Widget positions are offset from the GUI's position
				virtual void Draw(float dt, sf::Vector2f guiPos);
				/// \brief Called by the main GUI Draw method, this is used to draw this Widget to the supplied Texture
				/// \param texture The texture to draw this widget onto
				virtual void Draw(float dt, sf::RenderTexture& texture);

				void SetFont(sf::Font font);
				void SetString(std::string s);
				void SetColor(sf::Color c);
				void SetFontSize(int size);
				int GetFontSize();

			private:
				void UpdateSize();

			private:
				sf::Font mFont;
				sf::Text mText;
			};
		}
	}
}

#endif
