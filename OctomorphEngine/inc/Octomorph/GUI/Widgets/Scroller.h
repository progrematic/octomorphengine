#ifndef OCTOMORPH_SCROLLER
#define OCTOMORPH_SCROLLER

#include <Octomorph/Headers.h>
#include <Octomorph/Enums.h>
#include <Octomorph/GUI/Widgets/Widget.h>

namespace Octomorph
{
	namespace GUI
	{
		namespace Widgets
		{
			/// \brief [UNFINISHED. DO NOT USE] A container Widget that can scroll content
			class Scroller : public Widget
			{
			public:
				Scroller();

				void SetOrientation(Enums::ORIENTATION orientation);
				void SetMaskSize(sf::Vector2f size);
				void SetInsideColor(sf::Color color);
				void SetBorderColor(sf::Color color);
				void SetBorderSize(float size);
				void SetSpacing(float spacing);

				void AddWidget(std::shared_ptr<Widget> widget);

				virtual void Update(float dt, sf::Vector2f guiPos);
				virtual void Draw(float dt, sf::Vector2f guiPos);
				/// \brief Called by the main GUI Draw method, this is used to draw this Widget to the supplied Texture
				/// \param texture The texture to draw this widget onto
				virtual void Draw(float dt, sf::RenderTexture& texture);

			private:
				void DrawToTexture(float dt);
				void RepositionWidgets();

			private:
				std::vector<std::shared_ptr<Widget>> mWidgets;
				
				sf::RenderTexture mTexture;
				sf::Shader mMaskShader;

				Enums::ORIENTATION mOrientation;

				sf::Vector2f mMaskSize;
				sf::Vector2f mWidgetCummulativeSize;

				sf::RectangleShape mBorder;
				sf::RectangleShape mInsideBG;
				float mBorderSize;
				float mSpacing;
				float mOffset;
			};
		}
	}
}

#endif
