#ifndef OCTOMORPH_BUTTON
#define OCTOMORPH_BUTTON

#include <Octomorph/Headers.h>
#include <Octomorph/Globals.h>
#include <Octomorph/GUI/Widgets/Widget.h>
#include <Octomorph/GUI/Widgets/TextLabel.h>
#include <Octomorph/GUI/Widgets/Image.h>
#include <Octomorph/Interfaces/IEventHandler.h>

#include <functional>

namespace Octomorph
{
	namespace GUI
	{
		namespace Widgets
		{
			/// \brief A Widget that can call a callback when clicked.
			///
			/// This widget allows setting colours or sprites for all BUTTON_STATEs.
			///
			/// The callback can be set with a void argument or a void* argument as eneded.
			class Button : public Widget, public Interfaces::IEventHandler
			{
			public:
				/// \brief Button state that drives colour/sprite loading
				enum class BUTTON_STATE
				{
					NORMAL,	///< Normal mode
					HOVER,	///< When the mouse is over the button
					PRESSED	///< When the mouse is pressed and over the button
				};

			public:
				Button();

				/// \brief Sets a button colour for the given state
				/// \param color The background color to set
				/// \param state The button state the color will be applied to
				void SetColorForState(sf::Color color, BUTTON_STATE state);
				/// \brief Sets a button texture for the given state
				/// \param color The background texture to set
				/// \param state The button state the texture will be applied to
				void SetImageForState(sf::Texture& image, BUTTON_STATE state);
				/// \brief Sets a callback to be executed when the button is clicked
				/// \param callback A callback returning void and taking void
				void SetCallback(std::function<void(void)> callback);
				/// \brief Sets a callback with an argument to be executed when the button is clicked
				/// \param callback A callback returning void and taking void*
				/// It's up to the caller to know what to expect void* will be, since they're the ones creating the callback.
				void SetCallback(std::function<void(void*)> callback, void* arg);
				void SetState(BUTTON_STATE state);
				/// \brief Used to get a reference to the TextLabel widget
				/// \return A reference to the local TextLabel widget used for button text
				TextLabel& GetLabel();

				/// \brief Called by the main GUI HandleEvent method, this is used to pull the Mouse data that drives the BUTTON_STATE
				/// \param e The SFML event
				bool HandleEvent(sf::Event e);

				/// \brief Called by the main GUI Update method, this is used to drive any logic running on a loop
				/// \param guiPos The global GUI position, since Widget positions are offset from the GUI's position
				virtual void Update(float dt, sf::Vector2f guiPos);
				/// \brief Called by the main GUI Draw method, this is used to draw this button and its TextLabel
				/// \param guiPos The global GUI position, since Widget positions are offset from the GUI's position
				virtual void Draw(float dt, sf::Vector2f guiPos);
				/// \brief Called by the main GUI Draw method, this is used to draw this Widget to the supplied Texture
				/// \param texture The texture to draw this widget onto
				virtual void Draw(float dt, sf::RenderTexture& texture);

			private:
				// Whether or not the button is selected
				bool mSelected;
				// The button's real position, as opposed to the draw position
				sf::Vector2f mRealPos;
				// The TextLabel widget used to draw the button caption
				TextLabel mLabel;
				// The Rectangle used to define the button - this gets coloured in the SetColorForState call
				sf::RectangleShape mShape;
				// The button state
				BUTTON_STATE mState;
				// The callback with void argument
				std::function<void(void)> mCallback;
				// The callback with void* argument
				std::function<void(void*)> mCallbackWithArg;
				// The argument for the callback with void* argument.
				// We need this because when SetCallbackWithArg gets called, we need to cache the arg until we call the callback itself and pass it along
				void* mArg;
				// A map storing the colours for each state
				std::map<BUTTON_STATE, sf::Color> mStateColors;
				// A map storing the textures for each state
				std::map<BUTTON_STATE, std::shared_ptr<Image>> mStateImages;
			};
		}
	}
}

#endif
