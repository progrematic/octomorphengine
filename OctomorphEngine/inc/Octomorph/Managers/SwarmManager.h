#ifndef SWARMMANAGER_H
#define SWARMMANAGER_H

#include <Octomorph/Headers.h>
#include <Octomorph/Globals.h>

#include <Octomorph/Components/DynamicTransformComponent.h>

namespace Octomorph
{
	namespace Managers
	{
		/// \brief A manager used to handle Swarms
		class SwarmManager
		{
		public:
			SwarmManager() {}
			/// \brief A swarm has its own position that agents strive to be in.
			SwarmManager(sf::Vector2f pos, int agentCount);

			/// \brief Sets the various values that drive the swarm algorithm.
			/// Default values are as follows:
			///
			/// mSpeed = 5;
			///
			/// mSeparateSpeed = 100;
			///
			/// mMaxSteer = 0.8f;
			///
			/// mArrivalRadius = mSpeed * 20;
			///
			/// mSeparationRadius = 30;
			///
			/// mSeekFactor = 1;
			///
			/// mSeparateFactor = 1.25f;
			///
			/// mSettleDistance = 50;
			///
			/// mSettleVelocity = 1;
			void SetSwarmValues(float speed, float separateSpeed, float maxSteer, float arrivalRadius, float separationRadius,
				float seekFactor, float separateFactor, float settleDistance, float settleVelocity);

			/// \brief Applies Separate and Seek algorithms on all agents
			void Update(float dt);
			/// \brief Draws all agents as red squared. Useful for debugging purposes
			void Draw(float dt);

			/// \brief Sets the Swarm position, where all Agents will strive to be in
			void SetPos(sf::Vector2f pos);
			/// \brief Gets the Swarm position, where all Agents will strive to be in
			sf::Vector2f GetPos();

			/// \brief Returns a const reference to the swarm agents - which are just DynamicTransformComponents, so the game code can do whatever it wants with these.
			const std::vector<std::shared_ptr<Components::DynamicTransformComponent>>& GetAgents();

		private:
			sf::Vector2f Seek(std::shared_ptr<Components::DynamicTransformComponent> agent, sf::Vector2f target);
			sf::Vector2f Separate(std::shared_ptr<Components::DynamicTransformComponent> agent);

		private:
			float mSpeed;
			float mSeparateSpeed;
			float mMaxSteer;
			float mArrivalRadius;
			float mSeparationRadius;
			float mSeekFactor;
			float mSeparateFactor;
			float mSettleDistance;
			float mSettleVelocity;

			sf::Vector2f mPos;
			std::vector<std::shared_ptr<Components::DynamicTransformComponent>> mAgents;
		};
	}
}

#endif