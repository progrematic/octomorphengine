#ifndef VIEWMANAGER_H
#define VIEWMANAGER_H

#include <Octomorph/Headers.h>
#include <Octomorph/Globals.h>

using namespace Octomorph::Enums;

namespace Octomorph
{
	namespace Managers
	{
		/// \brief A manager used to facilitate SFML View management
		class ViewManager
		{
		public:
			static ViewManager* GetInstance();

			/// \brief Creates the screen with a single-player view
			/// \param flags This should be multiple sf::Style values piped together
			void Initialize(char* windowTitle, int resX, int resY, int bitDepth, int flags);

			/// \brief [EXPERIMENTAL] - Splits the screen as needed based on the number of players
			void UpdatePlayerCount(int playerNum);

			/// \brief Gets the window
			sf::RenderWindow& GetWindow();
			/// \brief Sets the view subsequent draw calls will affect
			void SetView(VIEW view);

			/// \brief Sets the view pan speed - Update method will pan the camera over by this speed
			void SetGameViewpointSpeed(VIEW view, float speed);
			/// \brief Sets the viewpoint target - Update method will pan the camera over to this location
			void SetGameViewpointTarget(VIEW view, sf::Vector2f target);
			/// \brief Sets the viewpoint location - This will snap the view to this location
			void SetGameViewpoint(VIEW view, sf::Vector2f gameViewpoint);

			/// \brief Moves each view over to its respective viewpoint target
			void Update(float dt);

			/// \brief Gets a reference to our SFML view
			sf::View& GetView(VIEW view);
		private:
			ViewManager();

			struct PlayerView
			{
				sf::Vector2f viewpoint;
				sf::Vector2f targetViewpoint;
				float viewSpeed;

				sf::View view;
			};

		private:
			static ViewManager* mInstance;

			sf::RenderWindow mWindow;

			int mResX;
			int mResY;

			int mPlayerNum;

			PlayerView mViews[ETOI(VIEW::COUNT)];
		};
	}
}

#endif
