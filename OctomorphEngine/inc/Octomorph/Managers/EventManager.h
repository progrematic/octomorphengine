#ifndef OCTOMORPH_EVENTMANAGER
#define OCTOMORPH_EVENTMANAGER

#include <Octomorph/Headers.h>
#include <Octomorph/Globals.h>

#include <Octomorph/Interfaces/IEventHandler.h>

#include <deque>

namespace Octomorph
{
	namespace Managers
	{
		/// \brief A manager to pass along SFML events to all interested IEventHandlers
		class EventManager
		{
		public:
			static EventManager* GetInstance();

			/// \brief Resets the 'HandledThisFrame' state - Call this at the start of your Update loop.
			void Update();

			/// \brief Adds a shared_ptr to an IEventHandler to the internal deque.
			void AddHandler(std::shared_ptr<Interfaces::IEventHandler> handler);
			/// \brief Removes a shared_ptr to an IeventHandler from the internal deque.
			void RemoveHandler(std::shared_ptr<Interfaces::IEventHandler> handler);
			/// \brief Takes an SFML event and calls HandleEvent on the latest-registered handler. If it returns true, it will consume this event and it won't call HandleEvent on the rest of the handlers in the deque.
			void HandleEvent(sf::Event e);

			/// \brief Whether or not an event was consumed this frame. Useful for detecting if a GUI::Widget has consumed an event - in which case, we may not want to handle it at the Game level.
			bool ConsumedThisFrame();

		private:
			EventManager();

		private:
			static EventManager* mInstance;
			std::deque<std::shared_ptr<Interfaces::IEventHandler>> mHandlers;
			bool mHandled;
		};
	}
}

#endif
