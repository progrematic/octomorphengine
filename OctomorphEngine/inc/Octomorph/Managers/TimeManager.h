#ifndef TIMEMANAGER_H
#define TIMEMANAGER_H

#include <Octomorph/Headers.h>
#include <thread>

using namespace std::chrono;

namespace Octomorph
{
	namespace Managers
	{
		/// \brief A manager used to facilitate timing
		class TimeManager
		{
		public:
			static TimeManager* GetInstance();

			~TimeManager();

			/// \brief Starts a timer by the name provided
			void StartTimer(std::string name);
			/// \brief Stops the timer by the name provided
			/// \param printDuration Whether or not we should print the duration to the console
			/// \return The length of time in milliseconds
			float StopTimer(std::string name, bool printDuration = false);
			/// \brief Gets the duration of a timer by the name provided
			/// \return The length of time in milliseconds
			float GetDuration(std::string name);
			/// \brief Prints the duration of a timer by the name provided
			void PrintDuration(std::string name);

		private:
			TimeManager();

		private:
			struct TimeData
			{
				high_resolution_clock::time_point startTime;
				high_resolution_clock::time_point endTime;
				duration<double, std::milli> timeSpan;
				bool timed;
			};

			static TimeManager* mInstance;

			std::map<std::string, TimeData> mTimeData;
		};
	}
}

#endif
