#ifndef TEXTUREMANAGER_H
#define TEXTUREMANAGER_H

#include <Octomorph/Headers.h>

namespace Octomorph
{
	namespace Managers
	{
		/// \brief A manager used to ease SFML Texture loading
		class TextureManager
		{
		public:
			TextureManager();

			/// \brief Adds a texture using the provided path if it hasn't already been added before
			/// \return The Texture ID of the texture represented by the provided path
			static int AddTexture(std::string path);
			/// \brief Removes a texture from our internal Texture map
			static void RemoveTexture(int id);
			/// \brief Gets an SFML Texture pointer to be used in creating/loading Sprites
			static sf::Texture* GetTexture(int id);

		private:
			static std::map<std::string, std::pair<int, std::unique_ptr<sf::Texture>>> mTextures;
			static int mCurId;
		};
	}
}

#endif
