#ifndef OCTOMORPH_NETWORKMANAGER
#define OCTOMORPH_NETWORKMANAGER

#include <Octomorph/Headers.h>
#include <Octomorph/Globals.h>

#include <SFML/Network.hpp>

#include <thread>
#include <mutex>

namespace Octomorph
{
	namespace Managers
	{
		/// \brief [UNFINISHED. DO NOT USE] A manager used to simplify Client/Server setup/communication
		class NetworkManager
		{
		public:
			static NetworkManager* GetInstance();
			/// \brief Get's the local IP address
			static std::string GetLocalIp();

			// SERVER
			/// \brief [HOST] - Takes the port and starts a thread running TryListenAsync()
			void Host(int port);
			/// \brief [HOST] - Attempts to open up a TCP Listener on the port specified in Host()
			/// Opening the port is a blocking call, so this runs asynchronously on it's own thread
			void TryListenAsync();
			void ListenAsync();

			// CLIENT
			void Join(std::string address, int port);
			void TryJoinAsync();

			bool IsReady();
			void Update();

		private:
			NetworkManager();

		private:
			static NetworkManager* mInstance;
			bool mReady;

			// SERVER
			bool mListen;
			int mListenPort;
			std::thread mListenThread;
			sf::TcpListener mTCPListener;
			std::vector<std::unique_ptr<sf::TcpSocket>> mTCPClients;

			// CLIENT
			std::string mServerAddress;
			int mServerPort;
			bool mConnectedToServer;
			sf::TcpSocket mTCPServer;
			sf::UdpSocket mUDPServer;

			// BOTH
			std::thread mThread;
			std::mutex mMutex;
		};
	}
}

#endif
