#ifndef LIGHTMANAGER_H
#define LIGHTMANAGER_H

#include <Octomorph/Headers.h>
#include <Octomorph/Components/LightSourceComponent.h>

#include <thread>
#include <mutex>

#define MAX_LIGHTS 100	// Ensure to change the value in Fog.fragment as well!

namespace Octomorph
{
	namespace Managers
	{
		/// \brief A manager to handle Lights - runs on its own thread with a single shader
		class LightManager
		{
		public:
			static LightManager* GetInstance();

			~LightManager();

			/// \brief Loads the Fog.fragment shader. You'll need to put it in this directory relative to your game: Resources/Shaders/Fog.frag
			void Initialize();
			/// \brief Resets all Light Sources, joins the thread if necessary, and then spawns a new thread on LightManager::Run
			void Reset(sf::Vector2f worldSize, int tileSize);
			/// \brief Cleans up all Light Sources and joins the thread if necessary
			void Cleanup();

			/// \brief The function running on the new Thread. This has a loop in it which updates the Shader info and runs the shader
			void Run();
			/// \brief Called from the game, this draws the Texture on which the shader runs.
			void Draw(float dt);

			/// \brief Adds a shared_ptr of LightSourceComponent into the local LightSourceComponent vector
			void AddLightSource(std::shared_ptr<Components::LightSourceComponent> lightSource);
			/// \brief Releases the shared_ptr of all cached LightSourceComponents
			void ClearLightSources();

			/// \brief Creates a rectangular LightSourceComponent
			/// \param discoverRange The number of tiles around the Rect that wane off (see Components::LightSourceComponent SetWaning)
			void LightRect(sf::IntRect rect, float discoverRange = 1);

		private:
			LightManager();

		private:
			struct LightSourceInfo
			{
				int lightNum;
				sf::Glsl::Vec2 pos[MAX_LIGHTS];
				sf::Glsl::Vec2 size[MAX_LIGHTS];
				float radius[MAX_LIGHTS];
				float waning[MAX_LIGHTS];
				sf::Glsl::Vec4 color[MAX_LIGHTS];
			};

			static LightManager* mInstance;

			int mTileSize;
			sf::RenderTexture mFogTexture;
			sf::Shader mFogShader;
			std::vector<std::shared_ptr<Components::LightSourceComponent>> mLightSources;
			std::thread thread;
			std::mutex mutex;
			float mWorldHeight;

			LightSourceInfo mLightSourceInfo;
			bool mRunning;
		};
	}
}

#endif
