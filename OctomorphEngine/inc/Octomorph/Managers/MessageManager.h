#ifndef OCTOMORPH_MESSAGEMANAGER
#define OCTOMORPH_MESSAGEMANAGER

#include <Octomorph/Headers.h>
#include <Octomorph/Globals.h>

#include <Octomorph/Interfaces/IMessageHandler.h>

namespace Octomorph
{
	namespace Managers
	{
		/// \brief Used to drive game-specific messages accross game entities/classes
		class MessageManager
		{
		public:
			static MessageManager* GetInstance();

			/// \brief Calls HandleMessage on all IMessageHandlers subscribed to the Message Type
			void SendMessage(std::shared_ptr<Octomorph::Messages::Message> msg);

			/// \brief Adds an IMessageHandler to the internal MessageHandler vector
			void AddHandler(Interfaces::IMessageHandler* handler);
			/// \brief Removes an IMessageHandler from the internal MessageHandler vector
			void RemoveHandler(Interfaces::IMessageHandler* handler);

		private:
			MessageManager();

		private:
			static MessageManager* mInstance;
			std::vector<Interfaces::IMessageHandler*> mHandlers;
		};
	}
}

#endif
