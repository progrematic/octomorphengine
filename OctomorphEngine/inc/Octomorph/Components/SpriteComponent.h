#ifndef ANIMATIONCOMPONENT_H
#define ANIMATIONCOMPONENT_H

#include <Octomorph/Headers.h>
#include "Component.h"

namespace Octomorph
{
	namespace Components
	{
		/// \brief Tracks a Sprite for the given attached object
		///
		/// This Component provides no Sprite-modifying methods.
		/// If you need to modify the sprite, GetSprite() returns a reference to the local sprite.
		class SpriteComponent : public Component
		{
		public:
			SpriteComponent();

			void Draw(float dt);

			void SetSprite(sf::Texture& texture);
			sf::Sprite& GetSprite();

		private:
			sf::Sprite mSprite;
		};
	}
}

#endif
