#ifndef OCTOMORPH_SPRITESHEETCOMPONENT
#define OCTOMORPH_SPRITESHEETCOMPONENT

#include <Octomorph/Headers.h>
#include <Octomorph/Globals.h>

#include <Octomorph/Components/Component.h>

namespace Octomorph
{
	namespace Components
	{
		/// \brief Tracks a Spritesheet for the given attached object
		/// Spritesheet and animations are driven off of accompanying XMLs
		///
		/// Quick example of a spritesheet xml
		/// \code{.xml}
		/// {
		/// 	"horTiles": 3,
		/// 	"verTiles": 4,
		/// 	"colWidth": 16,
		/// 	"colHeight": 28,
		/// 	"frameWidth": 32,
		/// 	"frameHeight": 32,
		/// 	"animations":
		/// 	[
		/// 		{
		/// 			"id": 0,
		/// 			"speed": 0,
		/// 			"spriteIndeces":[1]
		/// 		},
		/// 		{
		/// 			"id": 1,
		/// 			"speed": 0,
		/// 			"spriteIndeces":[4]
		/// 		},
		/// 		{
		/// 			"id": 2,
		/// 			"speed": 0,
		/// 			"spriteIndeces":[7]
		/// 		},
		/// 		{
		/// 			"id": 3,
		/// 			"speed": 0,
		/// 			"spriteIndeces":[10]
		/// 		},
		/// 		{
		/// 			"id": 4,
		/// 			"speed": 5,
		/// 			"spriteIndeces":[0, 1, 2, 1]
		/// 		},
		/// 		{
		/// 			"id": 5,
		/// 			"speed": 5,
		/// 			"spriteIndeces": [ 3, 4, 5, 4 ]
		/// 		},
		/// 		{
		/// 			"id": 6,
		/// 			"speed": 5,
		/// 			"spriteIndeces": [ 6, 7, 8, 7 ]
		/// 		},
		/// 		{
		/// 			"id": 7,
		/// 			"speed": 5,
		/// 			"spriteIndeces": [ 9, 10, 11, 10 ]
		/// 		}
		/// 	]
		/// }
		/// \endcode

		class SpritesheetComponent : public Component
		{
		public:
			SpritesheetComponent();
			/// \brief Allows the caller to define how many layers you want to draw to
			/// \param layerMax The max number of draw layers
			void SetLayerMax(int layerMax);
			/// \brief Loads the spritesheet xml file
			/// \param path The relative path to the spritesheet xml file
			void LoadSpritesheet(std::string path);
			/// \brief Sets the sprite at the given layer
			/// \param texture The texture
			/// \param layer The layer to draw to - must have called SetLayerMax for layers > 0
			void SetSprite(sf::Texture& texture, int layer = 0);
			/// \brief Clears the sprite at the given layer
			/// \param layer The layer
			void ClearSprite(int layer = 0);
			/// \brief Sets the spritesheet animation to play
			/// \param state Sets the spritesheet animation state
			void SetAnimation(int state);
			/// \brief Draws the Spritesheet for the given animation
			/// \param Delta Time
			void Draw(float dt);

			void SetPos(sf::Vector2f pos);
			sf::Vector2f GetPos();

			/// \brief Gets the current animation state
			/// \return The current animation state
			int GetCurrentAnimation();

			/// \brief Gets the frame dimensions as specified in the spritesheet xml
			/// \return The frame dimensions
			sf::Vector2f GetFrameDimensions();
			/// \brief Gets the collision dimensions as specified in the spritesheet xml
			/// \return The collision dimensions
			sf::Vector2f GetCollisionDimensions();

		private:
			/// \brief Contains the speed and frames for an animation
			struct Animation
			{
				int speed;					///< Number of ticks between frames
				std::vector<int> frames;	///< List of indeces into the spritesheet to circularly iterate over
			};

			/// \brief Clears sprites for all layers and resets them
			void ClearSprites();
			/// \brief Sets the Error state on this Component - loads notfound.png
			void LoadError();
			/// \brief Moves on to the next frame as per the current animation state and updates the sprites
			void NextFrame();
			/// \brief Returns a Frame for a given index into the spritesheet
			/// \param index The index into the spritesheet
			/// \return Frame representing the given index into the spritesheet
			sf::Vector2i GetFrameFromIndex(int index);

		private:
			// The max layers - defaults to 0 if SetLayerMax is not called
			int mLayerMax;
			// The current animation state
			int mCurState;
			// The current frame in the animation
			int mCurFrame;
			// The cummulative Delta Time used to trigger the next frame based on the animation speed
			float mDt;
			// Whether or not we're in an error state
			bool mError;
			// The position of the current Sprite
			sf::Vector2f mPos;
			// A map of layer to sprite so we can layer sprites on top of one another
			std::map<int /*layer*/, std::shared_ptr<sf::Sprite>> mSprites;
			// A map of animation state to Animation struct
			std::map<int /*animation state*/, Animation> mAnimations;

			int mHorTiles;		// Number of horizontal frames in the spritesheet
			int mVerTiles;		// Number of vertical frames in the spritesheet
			int mColWidth;		// Collision frame width
			int mColHeight;		// Collision frame height
			int mFrameWidth;	// Frame width
			int mFrameHeight;	// Frame height
		};
	}
}

#endif
