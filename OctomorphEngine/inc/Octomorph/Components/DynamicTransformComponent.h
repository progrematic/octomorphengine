#ifndef DYNAMICTRANSFORMCOMPONENT_H
#define DYNAMICTRANSFORMCOMPONENT_H

#include <Octomorph/Headers.h>
#include "StaticTransformComponent.h"

namespace Octomorph
{
	namespace Components
	{
		/// \class DynamicTransformComponent
		/// \brief An extension to StaticTransformComponent that introduces Velocity and Forces
		///
		/// Drives position via forces and velocity instead of setting the position directly
		class DynamicTransformComponent : public StaticTransformComponent
		{
		public:
			DynamicTransformComponent();

			/// \brief Updates pos in StaticTransformComponent by adding our velocity
			/// \param resetVel Whether or not the velocity should be zeroed out after being applied
			/// \return The new position after applying the velocity
			sf::Vector2f ApplyVel(bool resetVel = true);
			/// \brief Updates pos in StaticTransformComponent by adding our velocity
			/// \param decay Our velocity gets 'decay' multiplied into it after being applied
			/// \return The new position after applying the velocity
			sf::Vector2f ApplyVel(sf::Vector2f decay);
			/// \brief Adds force to our internal velocity
			void AddForce(sf::Vector2f force);
			/// \brief Returns what the position would be after applying the velocity
			/// \return The current position plus the internal velocity (without actually changing the position)
			sf::Vector2f GetFuturePos();

			void SetVel(sf::Vector2f vel);
			sf::Vector2f& GetVel();

		private:
			sf::Vector2f mVel;
		};
	}
}

#endif
