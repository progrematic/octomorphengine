#ifndef OCTOMORPH_INPUTCOMPONENT
#define OCTOMORPH_INPUTCOMPONENT

#include <Octomorph/Headers.h>
#include <Octomorph/Globals.h>

#include <Octomorph/Components/Component.h>
#include <Octomorph/Input/Joystick.h>
#include <Octomorph/Input/Keyboard.h>
#include <map>

using namespace Octomorph::Enums;
using namespace Octomorph::Input;

namespace Octomorph
{
	namespace Components
	{
		/// \brief Allows an Object to react to various forms of Input
		///
		/// Provides a simple way to abstract direct input through an enum and define which inputs the attached object reacts to, and what the state of those inputs are
		class InputComponent : public Component
		{
		public:
			InputComponent();
			/// \brief Defines which INPUT_TYPE the attached Object listens to
			void SetInputType(INPUT_TYPE type);
			/// \brief Returns the INPUT_TYPE the attached Object listens to
			/// \return The INPUT_TYPE the attached Object listens to - defaults to Keyboard if SetInputType wasn't called
			INPUT_TYPE GetInputType();
			/// \brief Binds a Keyboard key to an Analog Input
			/// \param high Whether or not the key binds to the HIGH end of the Analog input or not
			void SetKeyboardAnalogInput(int input, bool high, sf::Keyboard::Key key);
			/// \brief Binds a Keyboard key to a Digital Input
			void SetKeyboardDigitalInput(int input, sf::Keyboard::Key key);
			/// \brief Sets the SFML Joystick ID to listen to if the INPUT_TYPE is set to Joystick
			void SetJoystickId(int id);
			/// \brief Gets the Joystick ID this Component listens to
			/// \return The Joystick ID - defaults to 1 if SetJoystickId wasn't called
			int GetJoystickId();
			/// \brief Binds a Joystick Axis to an Analog Input
			void SetJoystickAnalogInput(int input, Joystick::Axis key);
			/// \brief Binds a Joystick Button to a Digital Input
			void SetJoystickDigitalInput(int input, Joystick::Button key);
			/// \brief Gets the Analog value for the given input
			/// \return A float value representing the Analog input
			float GetAnalog(int input);
			/// \brief Gets the Digital value for the given input
			/// \return Whether or not the Digital input is currently down
			bool GetDigital(int input);
			/// \brief Gets whether or not the Digital input for the given input was just pressed
			/// \return Whether or not the Digital input was just pressed
			bool GetDigitalDown(int input);
			/// \brief Gets whether or not the Digital input for the given input was just released
			/// \return Whether or not the Digital input was just released
			bool GetDigitalUp(int input);

		private:
			/// \brief Keyboard Key representation of an axis
			/// Stores two Keyboard Keys used to simulate high and low extremeties of an Analog axis
			struct KeyboardAxis
			{
				sf::Keyboard::Key high;	///< Keyboard Key simulating the HIGH end of the Axis value
				sf::Keyboard::Key low;	///< Keyboard key simulating the LOW end of the Axis value
			};

		private:
			// The INPUT_TYPE this Component listens to
			INPUT_TYPE mInputType;

			// A map storing the Keyboard representations of Analog Inputs
			std::map<int, KeyboardAxis> mKeyboardAnalogInputs;
			// A map storing the Keyboard representations of Digital Inputs
			std::map<int, sf::Keyboard::Key> mKeyboardDigitalInputs;

			// The Joystick ID this Component listens to if mInputType is set to Joystick
			int mJoystickId;
			// A map storing the Joystick representations of Analog Inputs
			std::map<int, Joystick::Axis> mJoystickAnalogInputs;
			// A map storing the Joystick representations of Digital Inputs
			std::map<int, Joystick::Button> mJoystickDigitalInputs;
		};
	}
}

#endif
