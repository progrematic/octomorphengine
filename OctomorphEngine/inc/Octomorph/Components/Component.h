#ifndef COMPONENT_H
#define COMPONENT_H

namespace Octomorph
{
	namespace Components
	{
		/// \brief The parent to all component classes.
		///
		/// This simply ensures that all child components have an Update method, even if it's this empty one.
		class Component
		{
		public:
			Component() {}

			/// \brief An empty Update method that can be overridden by Components as needed
			virtual void Update(float dt) {};

		private:

		};
	}
}

#endif
