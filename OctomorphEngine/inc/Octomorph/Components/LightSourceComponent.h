#ifndef LIGHTSOURCECOMPONENT_H
#define LIGHTSOURCECOMPONENT_H

#include <Octomorph/Headers.h>
#include "Component.h"

namespace Octomorph
{
	namespace Components
	{
		/// \brief Allows an object to circularly emit light
		///
		/// Provides control over various aspects of a light source
		class LightSourceComponent : public Component
		{
		public:
			LightSourceComponent();

			/// \brief Overrides the base Update method to handle Flicker if it's enabled
			void Update(float dt) override;

			sf::Vector2f GetPos();
			void SetPos(sf::Vector2f pos);
			sf::Vector2f GetSize();
			void SetSize(sf::Vector2f size);
			sf::Color GetColor();
			void SetColor(sf::Color color);
			/// \brief The light source's radius of 100% bright light
			float GetRadius();
			/// \brief The light source's radius of 100% bright light
			void SetRadius(float radius);
			/// \brief The number of pixels in which the light gradients from 100% to 0% light
			float GetWaning();
			/// \brief The number of pixels in which the light gradients from 100% to 0% light
			void SetWaning(float waning);
			/// \brief Whether or not the light flickers
			bool GetFlicker();
			/// \brief Sets the flicker state
			/// \param flicker Whether or not this light should flicker
			/// \param flickerRate Milliseconds between flickers
			/// \param flickerIntensity The amount the waning flickers by
			void SetFlicker(bool flicker, float flickerRate = 8, float flickerIntensity = 2);

		private:
			// The light source's position
			sf::Vector2f mPos;
			// The light source's size (allows for rectangular light sources)
			sf::Vector2f mSize;
			// The light source's colour
			sf::Color mColor;
			// The light source's Radius of 100% bright light
			float mRadius;
			// The number of pixels in which the light gradients from 100%-0% light
			float mWaning;
			// Whether or not the light should Flicker
			bool mFlicker;
			// How often the light should flicker in MS
			float mFlickerRate;
			// The cummulative delta time - used to decide when to flicker
			float mDt;
			// Tracks the flicker direction so we can flicker in and out properly
			int mFlickerDirection;
			// How much distance the light should flicker by
			float mFlickerIntensity;
		};
	}
}

#endif
