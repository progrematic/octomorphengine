#ifndef STATICTRANSFORMCOMPONENT_H
#define STATICTRANSFORMCOMPONENT_H

#include <Octomorph/Headers.h>
#include "Component.h"

namespace Octomorph
{
	namespace Components
	{
		/// \class StaticTransformComponent
		/// \brief Holds positional information
		///
		/// Allows for storage and manipulation of a position
		class StaticTransformComponent : public Component
		{
		public:
			StaticTransformComponent();

			void SetPos(sf::Vector2f pos);
			sf::Vector2f& GetPos();

		protected:
			sf::Vector2f mPos;
		};
	}
}

#endif
