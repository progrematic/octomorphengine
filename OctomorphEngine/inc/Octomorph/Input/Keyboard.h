#ifndef TANKS_KEYBOARD
#define TANKS_KEYBOARD

#include <Octomorph/Headers.h>

namespace Octomorph
{
	namespace Input
	{
		/// \brief An SFML Keyboard abstraction used to have better control and access over Keyboard state and events
		class Keyboard
		{
		public:
			/// \brief The Key event handler. Call this in your PollEvents for a KeyUp or KeyDown event.
			/// \code
			/// if (e.type == sf::Event::KeyPressed || e.type == sf::Event::KeyReleased)
			/// {
			///		Keyboard::KeyCallback(e.key.code)
			/// }
			/// \endcode
			static void KeyCallback(sf::Keyboard::Key key);
			/// \brief [DEPRECATED - use a GUI::Widgets::Input object to get Textual input from the user]. The Text event handler. Call this in your PollEvents for a TextEntered event.
			/// \code
			/// if (e.type == sf::Event::TextEntered)
			/// {
			/// 	Keyboard::TextCallback(e.text.unicode);
			/// }
			/// \endcode
			static void TextCallback(sf::Uint32 c);

			/// \brief Resets all KeyUp/KeyDown values to false.
			/// Since this should be called before we poll any events, we want to clear any previous Up/Down state since nothing has been pressed or released this frame yet.
			static void Update();

			/// \brief Whether any Key was pressed this frame
			static bool KeyDown();
			/// \brief Whether the Key was pressed this frame
			static bool KeyDown(sf::Keyboard::Key key);
			/// \brief Whether the Key was released this frame
			static bool KeyUp(sf::Keyboard::Key key);
			/// \brief Whether the Key is currently down
			static bool Key(sf::Keyboard::Key key);
			/// \brief [DEPRECATED - use a GUI::Widgets::Input object to get Textual input from the user]. Gets the text the user has entered so far
			static sf::String GetTypedString();
			/// \brief [DEPRECATED - use a GUI::Widgets::Input object to get Textual input from the user]. Clear the cached text the user has entered so far
			static void ClearTypedString();
		private:
			static bool keys[];
			static bool keysDown[];
			static bool keysUp[];
			static sf::String string;
		};
	}
}

#endif