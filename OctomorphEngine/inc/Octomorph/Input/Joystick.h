#ifndef PROGENGINE_JOYSTICK
#define PROGENGINE_JOYSTICK

#include <Octomorph/Headers.h>

#include <iostream>
#include <map>
#include <array>
#include <thread>
using namespace std;

namespace Octomorph
{
	namespace Input
	{
		/// \brief An SFML Joystick abstraction used to have better control and access over Joystick state and events
		class Joystick
		{
		public:

			/// \brief An enumeration of all Joytick axes
			enum Axis
			{
				LeftStickHorizontal,
				LeftStickVertical,
				Triggers,	///< LT/RT share a single axis. Helper methods exist to separate this properly.
				RightStickVertical,
				RightStickHorizontal,
				AxisSize
			};

			/// \brief An enumeration of all Button axes
			enum Button
			{
				A,
				B,
				X,
				Y,
				LB,
				RB,
				Back,
				Start,
				LeftStick,
				RightStick,
				DPAD_Up,
				DPAD_Right,
				DPAD_Down,
				DPAD_Left,
				ButtonSize
			};

			/// \brief An enumeration allowing us to use Axes as buttons when they pass a certain threshold in a given direction.
			enum AxisButton
			{
				LeftStick_Left,
				LeftStick_Right,
				LeftStick_Up,
				LeftStick_Down,
				LeftTrigger,
				RightStick_Left,
				RightStick_Right,
				RightStick_Up,
				RightStick_Down,
				RightTrigger
			};

			/// \brief Reevaluates connected joysticks and sets them up as necessary
			static void Initialize();
			/// \brief Updates joystick states
			static void Update();

			/// \brief Gets the status of the Button for a given Joystick ID
			/// \return Whether or not the button is currently pressed
			static bool GetButton(int joystickID, Button button);
			/// \brief Gets the status of the Button for a given Joystick ID
			/// \return Whether or not the button was pressed this frame
			static bool GetButtonDown(int joystickID, Button button);
			/// \brief Gets the status of the Button for a given Joystick ID
			/// \return Whether or not the button was released this frame
			static bool GetButtonUp(int joystickID, Button button);
			/// \brief Gets the status of the AxisButton for a given Joystick ID
			/// \return Whether or not the AxisButton is currently pressed
			static bool GetAxisButton(int joystickID, AxisButton button);
			/// \brief Gets the status of the AxisButton for a given Joystick ID
			/// \return Whether or not the AxisButton was pressed this frame
			static bool GetAxisButtonDown(int joystickID, AxisButton button);
			/// \brief Gets the status of the AxisButton for a given Joystick ID
			/// \brief Whether or not the AxisButton was released this frame
			static bool GetAxisButtonUp(int joystickID, AxisButton button);

			/// \brief Gets the status of the Axis for a given Joystick ID
			/// \return The axis value for the given Axis
			static float GetAxis(int joystickID, Axis axis);
			/// \brief Gets the Lext Axis Force (hypotenuse of Horizontal and Vertical Axes) for a given Joystick ID
			static float GetLeftAxisForce(int joystickID);
			/// \brief Gets the Right Axis Force (hypotenuse of Horizontal and Vertical Axes) for a given Joystick ID
			static float GetRightAxisForce(int joystickID);
			/// \brief Gets the Left Axis Angle in degrees for a given Joystick ID
			static float GetLeftAxisAngle(int joystickID);
			/// \brief Gets the Right Axis Angle in degrees for a given Joystick ID
			static float GetRightAxisAngle(int joystickID);
			/// \brief Gets the Left Trigger axis value for a given Joystick ID
			static float GetLeftTrigger(int joystickID);
			/// \brief Gets the Right Trigger axis value for a given Joystick ID
			static float GetRightTrigger(int joystickID);

			/// \brief Prints the Joystick State for a given Joystick ID
			static void PrintDebug(int joystickID);
			/// \brief Prints a small Joystick State for a given Joystick ID
			static void PrintDebugSmall(int joystickID);
			/// \brief Prints the small Joystick State for all connected joysticks
			static void PrintDebugAll();

		private:
			static float deadzone;

			static map<int, array<bool, Button::ButtonSize>> buttons;
			static map<int, array<bool, Button::ButtonSize>> lastButtons;
			static map<int, array<float, Axis::AxisSize>> axes;
			static map<int, array<float, Axis::AxisSize>> lastAxes;
			static map<int, bool> availableJoysticks;
			static float axisButtonThreshold;
		};
	}
}

#endif