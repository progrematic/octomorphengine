#ifndef TWITCH_MOUSE
#define TWITCH_MOUSE

#include <Octomorph/Headers.h>

namespace Octomorph
{
	namespace Input
	{
		/// \brief An SFML Mouse abstraction used to have better control and access over Mouse state and events
		class Mouse
		{
		public:
			/// \brief The Mouse Position event handler. Call this in your PollEvents for a MouseMoved event.
			/// \code
			/// if (e.type == sf::Event::MouseMoved)
			/// {
			///		// Note we pass the ViewManager's window so we can get the position relative to our window and not to the screen.
			/// 	Mouse::MousePosCallback(sf::Mouse::getPosition(ViewManager::GetInstance()->GetWindow()).x, sf::Mouse::getPosition(ViewManager::GetInstance()->GetWindow()).y);
			/// }
			/// \endcode
			static void MousePosCallback(int _x, int _y);
			/// \brief The Mouse Button event handler. Call this in your PollEvents for a MouseButtonPressed or MouseButtonReleased event.
			/// \code
			/// if (e.type == sf::Event::MouseButtonPressed || e.type == sf::Event::MouseButtonReleased)
			/// {
			/// 	Mouse::MouseButtonCallback(e.mouseButton.button);
			/// }
			/// \endcode
			static void MouseButtonCallback(sf::Mouse::Button button);

			/// \brief Resets all ButtonUp/ButtonDown values to false.
			/// Since this should be called before we poll any events, we want to clear any previous Up/Down state since nothing has been pressed or released this frame yet.
			static void Update();

			/// \brief Gets the Mouse X position
			static int GetMouseX();
			/// \brief Gets the Mouse Y position
			static int GetMouseY();

			/// \brief Whether or not the Mouse Button was pressed this frame
			static bool ButtonDown(sf::Mouse::Button button);
			/// \brief Whether or not the Mouse Button was released this frame
			static bool ButtonUp(sf::Mouse::Button button);
			/// \brief Whether or not the Mouse Button is currently down
 			static bool Button(sf::Mouse::Button button);

		private:
			static int x;
			static int y;

			static bool buttons[];
			static bool buttonsDown[];
			static bool buttonsUp[];
		};
	}
}

#endif
