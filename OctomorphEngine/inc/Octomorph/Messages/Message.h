#ifndef OCTOMORPH_MESSAGE
#define OCTOMORPH_MESSAGE

#include <Octomorph/Headers.h>
#include <Octomorph/Globals.h>

namespace Octomorph
{
	namespace Messages
	{
		/// \brief A Message with a type.
		/// Game-specific messages should inherit from this class.
		/// MessageManager deals with Message and all subclasses.
		class Message
		{
		public:
			Message() { mType = -1; }

			/// \brief Sets the message type. This should be driven by a game-defined Enum
			void SetType(int type) { mType = type; }
			/// \brief Gets the message type.
			int GetType() const { return mType; }

		protected:
			int mType;
		};
	}
}

#endif
