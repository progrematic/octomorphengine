#ifndef PARTICLEEMITTER_H
#define PARTICLEEMITTER_H

#include <Octomorph/Headers.h>
#include <Octomorph/Components/Component.h>

namespace Octomorph
{
	namespace Particles
	{
		/// \brief A collection of particle-specific values
		struct Particle
		{
			float lifespan;			///< Seconds before particle is destroyed
			sf::Vector2f pos;		///< Particle position
			sf::Vector2f posDiff;	///< Particle velocity?
		};

		/// \brief [UNFINISHED. DO NOT USE] A particle emitter used to emit particles in a given direction, orientation, and with custom properties
		class ParticleEmitter
		{
		public:
			ParticleEmitter();

			void Initialize(int emissionStartLength, int emissionEndLength, float emissionAngle, float emissionTime, float emissionRate, float particleStartSize, float particleEndSize, float particleLifespan, float particleSpeed);

			sf::Vector2f GetPos();
			void SetPos(sf::Vector2f pos);

			void SetParticleSprite(std::unique_ptr<sf::Sprite> sprite);

			void Emit();
			void Stop();

			void Update(float dt);
			void Draw(float dt);

		private:
			void SpawnParticle();
			void UpdateParticles(float dt);

		private:
			sf::Vector2f mPos;
			std::unique_ptr<sf::Sprite> mParticleSprite;

			int mEmissionStartLength;
			int mEmissionEndLength;
			float mEmissionAngle;
			float mEmissionTime;
			float mEmissionRate;

			float mParticleStartSize;
			float mParticleEndSize;
			float mParticleLifespan;
			float mParticleSpeed;

			bool mEmit;
			float mLastEmitTime;
			float mTotalEmitTime;

			std::vector<std::unique_ptr<Particle>> mParticles;
		};
	}
}

#endif
