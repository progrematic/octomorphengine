#ifndef OCTOMORPH_EVENTHANDLER
#define OCTOMORPH_EVENTHANDLER

#include <Octomorph/Headers.h>
#include <Octomorph/Globals.h>

namespace Octomorph
{
	namespace Interfaces
	{
		/// \brief An Interface that any class can inherit from if they need to handle SFML Events
		class IEventHandler : public std::enable_shared_from_this<IEventHandler>
		{
		public:
			virtual ~IEventHandler() {}
			/// \brief Enforcing that any child of IEventHandler actually handles SFML events.
			/// When this IEventHandler is active, EventManager will call HandleEvent from the latest-registered IEventHandler to the earliest-registered IEventHandler.
			/// \return Whether or not this event should be consumed. If true, no other IEventHandlers will have HandleEvent called for this event
			virtual bool HandleEvent(sf::Event e) = 0;
			/// \brief Sets whether or not this IEventHandler should have its HandleEvent method called
			virtual void SetActive(bool x) { mEventHandlerActive = x; }

		protected:
			bool mEventHandlerActive;
		};
	}
}

#endif
