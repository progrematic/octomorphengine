#ifndef OCTOMORPH_IMESSAGEHANDLER
#define OCTOMORPH_IMESSAGEHANDLER

#include <Octomorph/Headers.h>
#include <Octomorph/Globals.h>

#include <Octomorph/Messages//Message.h>

namespace Octomorph
{
	namespace Interfaces
	{
		/// \brief An Interface that any class can inherit from if they need to handle game-specific Messages
		class IMessageHandler
		{
		public:
			/// \brief Must define the maximum number of messages - Drive this with a game-specific Enum
			IMessageHandler(int messageTypeMax);
			virtual ~IMessageHandler() {}

			/// \brief Enables this IMessageHandler, and adds itself to MessageManager to be handled
			void Enable();
			/// \brief Disables this IMessageHandler, and removes itself from MessageManager
			void Disable();
			/// \brief Whether or not this IMessageHandler will receive any HandleMessages
			bool GetEnabled();

			/// \brief Called by MessageManager for all IMessageHandlers that subscribe to the Message type being sent from MessageManager
			virtual bool HandleMessage(const Messages::Message& msg) { return false; }

			/// \brief Whether or not this IMessageHandler is subscribed to the Message Type
			/// Message Types are driven by game-specific Enums, so we check against ints here.
			virtual bool IsSubscribed(int type);
			/// \brief Subscribes to the passed-in message type so that MessageManager will call HandleMessage on this IMessageHandler for any Messages of this type
			virtual void Subscribe(int type);
			/// \brief Unsubscribes to the passed-in message type so that MessageManager will not call HandleMessage on this IMessageHandler for any Messages of this type
			virtual void Unsubscribe(int type);

		private:
			bool mEnabled;

			std::map<int, bool> mSubscriptions;
		};
	}
}

#endif
