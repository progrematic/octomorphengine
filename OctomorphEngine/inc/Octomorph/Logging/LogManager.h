#ifndef OCTOMORPH_LOGMANAGER_H
#define OCTOMORPH_LOGMANAGER_H

#include <Octomorph/Headers.h>

namespace Octomorph
{
	namespace Logging
	{
		class LogManager
		{
		public:
			enum class LogLevel
			{
				INFO,
				WARN,
				DEBUG,
				ERROR,
				COUNT
			};

			struct Message
			{
				LogLevel level;
				const char* msg;
			};

		public:
			LogManager();

			static LogManager* GetInstance();
			void Log(LogLevel level, const char* msg, bool forcePrint = true);

		private:
			const char* GetHeader(LogLevel level);

		private:
			static LogManager* mInstance;
			std::vector<std::unique_ptr<Message>> mMessages;
		};
	}
}

#endif