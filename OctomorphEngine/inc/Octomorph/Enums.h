#ifndef OCTOMORPH_ENUMS
#define OCTOMORPH_ENUMS

namespace Octomorph
{
	namespace Enums
	{
		/// \brief Various input types
		enum class INPUT_TYPE
		{
			KEYBOARD,
			CONTROLLER,
			COUNT
		};

		enum class ORIENTATION
		{
			HORIZONTAL,
			VERTICAL,
			COUNT
		};

		/// \brief Views - supports up to four players, though multiplayer is experimental
		enum class VIEW
		{
			MAIN,
			PLAYER1,
			PLAYER2,
			PLAYER3,
			PLAYER4,
			UI,
			COUNT
		};
	}
}

#endif
