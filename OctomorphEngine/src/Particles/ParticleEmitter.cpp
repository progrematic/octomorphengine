#include <Octomorph/Particles/ParticleEmitter.h>
#include <Octomorph/Managers/ViewManager.h>

namespace Octomorph
{
	namespace Particles
	{
		ParticleEmitter::ParticleEmitter() :
			mEmissionStartLength(0),
			mEmissionEndLength(0),
			mEmissionAngle(0),
			mEmissionTime(-1),
			mEmissionRate(10),
			mParticleStartSize(1),
			mParticleEndSize(10),
			mParticleLifespan(1),
			mParticleSpeed(1),
			mEmit(false),
			mLastEmitTime(0),
			mTotalEmitTime(0)
		{}

		void ParticleEmitter::Initialize(int emissionStartLength, int emissionEndLength, float emissionAngle, float emissionTime, float emissionRate, float particleStartSize, float particleEndSize, float particleLifespan, float particleSpeed)
		{
			mEmissionStartLength = emissionStartLength;
			mEmissionEndLength = emissionEndLength;
			mEmissionAngle = emissionAngle;
			mEmissionTime = emissionTime;
			mEmissionRate = emissionRate;

			mParticleStartSize = particleStartSize;
			mParticleEndSize = particleEndSize;
			mParticleLifespan = particleLifespan;
			mParticleSpeed = particleSpeed;
		}

		sf::Vector2f ParticleEmitter::GetPos()
		{
			return mPos;
		}

		void ParticleEmitter::SetPos(sf::Vector2f pos)
		{
			mPos = pos;
		}

		void ParticleEmitter::SetParticleSprite(std::unique_ptr<sf::Sprite> sprite)
		{
			mParticleSprite = std::move(sprite);
		}

		void ParticleEmitter::Emit()
		{
			mEmit = true;
			mLastEmitTime = 0;
			mTotalEmitTime = 0;
		}

		void ParticleEmitter::Stop()
		{
			mEmit = false;
		}

		void ParticleEmitter::Update(float dt)
		{
			if (mEmit)
			{
				mLastEmitTime += dt;
				mTotalEmitTime += dt;

				if (mTotalEmitTime < mEmissionTime || mEmissionTime < 0)
				{
					if (mLastEmitTime >= (1.f / mEmissionRate))
					{
						mLastEmitTime = 0;
						SpawnParticle();
					}
				}
				else
				{
					Stop();
				}
			}
			UpdateParticles(dt);
		}

		void ParticleEmitter::Draw(float dt)
		{
			auto particleIterator = mParticles.begin();
			while (particleIterator != mParticles.end())
			{
				Particle& particle = **particleIterator;
				mParticleSprite->setPosition(particle.pos);
				Managers::ViewManager::GetInstance()->GetWindow().draw(*mParticleSprite);

				particleIterator++;
			}
		}

		// PRIVATE

		void ParticleEmitter::SpawnParticle()
		{
			sf::Vector2f pos = mPos;
			sf::Vector2f posDiff(0, 0);

			float xOffset = (float)(std::rand() % (mEmissionStartLength * 100) / 100.f);
			float endXOffset = (xOffset / mEmissionStartLength) * mEmissionEndLength;

			xOffset -= mEmissionStartLength / 2;
			endXOffset -= mEmissionEndLength / 2;

			pos.x += xOffset;
			posDiff.x = (endXOffset - xOffset) / abs(1 / mParticleSpeed);
			posDiff.y = mParticleSpeed;

			std::unique_ptr<Particle> particle = std::make_unique<Particle>();
			particle->lifespan = mParticleLifespan;
			particle->pos = pos;
			particle->posDiff = posDiff;
			mParticles.push_back(std::move(particle));
		}

		void ParticleEmitter::UpdateParticles(float dt)
		{
			auto particleIterator = mParticles.begin();
			while (particleIterator != mParticles.end())
			{
				Particle& particle = **particleIterator;
				particle.pos += particle.posDiff;
				particle.lifespan -= dt;

				if (particle.lifespan <= 0)
				{
					particleIterator = mParticles.erase(particleIterator);
				}
				else
				{
					particleIterator++;
				}
			}
		}
	}
}