#include <Octomorph/Interfaces/IMessageHandler.h>

#include <Octomorph/Managers/MessageManager.h>

namespace Octomorph
{
	namespace Interfaces
	{
		IMessageHandler::IMessageHandler(int messageTypeMax)
		{
			for (int i = 0; i < messageTypeMax; i++)
			{
				mSubscriptions.insert(std::pair<int, bool>(i, false));
			}

			mEnabled = false;
		}

		void IMessageHandler::Enable()
		{
			mEnabled = true;
			Managers::MessageManager::GetInstance()->AddHandler(this);
		}

		void IMessageHandler::Disable()
		{
			mEnabled = false;
			Managers::MessageManager::GetInstance()->RemoveHandler(this);
		}

		bool IMessageHandler::GetEnabled()
		{
			return mEnabled;
		}

		bool IMessageHandler::IsSubscribed(int type)
		{
			return mSubscriptions.at(type);
		}

		void IMessageHandler::Subscribe(int type)
		{
			mSubscriptions.at(type) = true;
		}

		void IMessageHandler::Unsubscribe(int type)
		{
			mSubscriptions.at(type) = false;
		}
	}
}