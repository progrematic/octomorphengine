#include <Octomorph/Logging/LogManager.h>

namespace Octomorph
{
	namespace Logging
	{

		LogManager* LogManager::mInstance = nullptr;

		LogManager::LogManager() {}

		LogManager* LogManager::GetInstance()
		{
			if (mInstance == nullptr)
			{
				mInstance = new LogManager();
			}
			return mInstance;
		}

		void LogManager::Log(LogManager::LogLevel level, const char* msg, bool forcePrint)
		{
			std::unique_ptr<Message> logMsg = std::make_unique<Message>();
			logMsg->level = level;
			logMsg->msg = msg;

			if (level >= LogLevel::ERROR || forcePrint)
			{
				std::cout << GetHeader(level) << msg << std::endl;
			}

			mMessages.push_back(std::move(logMsg));
		}

		// PRIVATE
		const char* LogManager::GetHeader(LogManager::LogLevel level)
		{
			switch (level)
			{
			case LogLevel::INFO:
				return "[INFO] ";
			case LogLevel::WARN:
				return "[WARN] ";
			case LogLevel::DEBUG:
				return "[DEBUG] ";
			case LogLevel::ERROR:
				return "[ERROR] ";
			}
			return "[INVALID_HEADER] ";
		}
	}
}