#include "Octomorph/Input/Keyboard.h"

namespace Octomorph
{
	namespace Input
	{
		bool Keyboard::keys[sf::Keyboard::KeyCount] = { 0 };
		bool Keyboard::keysDown[sf::Keyboard::KeyCount] = { 0 };
		bool Keyboard::keysUp[sf::Keyboard::KeyCount] = { 0 };
		sf::String Keyboard::string = "";

		void Keyboard::Update()
		{
			std::fill(keysDown, keysDown + sizeof(keysDown), false);
			std::fill(keysUp, keysUp + sizeof(keysUp), false);
		}

		void Keyboard::KeyCallback(sf::Keyboard::Key key)
		{
			if (key < 0)
				return;

			bool down = sf::Keyboard::isKeyPressed(key);

			if (down && keys[key] == false)
			{
				keysDown[key] = true;
				keysUp[key] = false;
			}

			if (!down && keys[key] == true)
			{
				keysDown[key] = false;
				keysUp[key] = true;
			}

			keys[key] = down;
		}

		void Keyboard::TextCallback(sf::Uint32 c)
		{
			if (c == '\b' && string.getSize() > 0)
				string.erase(string.getSize() - 1);
			else if (c > 31 && c < 128)
				string += c;
		}

		// Simply checks if any key is down
		bool Keyboard::KeyDown()
		{
			for (int i = 0; i < sf::Keyboard::KeyCount; i++)
				if (keysDown[i])
					return true;

			return false;
		}

		bool Keyboard::KeyDown(sf::Keyboard::Key key)
		{
			return keysDown[key];
		}

		bool Keyboard::KeyUp(sf::Keyboard::Key key)
		{
			return keysUp[key];
		}

		bool Keyboard::Key(sf::Keyboard::Key key)
		{
			return keys[key];
		}

		sf::String Keyboard::GetTypedString()
		{
			return string;
		}

		void Keyboard::ClearTypedString()
		{
			string = "";
		}
	}
}