#include "Octomorph/Input/Mouse.h"

namespace Octomorph
{
	namespace Input
	{
		int Mouse::x = 0;
		int Mouse::y = 0;

		bool Mouse::buttons[sf::Mouse::ButtonCount] = { 0 };
		bool Mouse::buttonsDown[sf::Mouse::ButtonCount] = { 0 };
		bool Mouse::buttonsUp[sf::Mouse::ButtonCount] = { 0 };

		void Mouse::Update()
		{
			std::fill(buttonsDown, buttonsDown + sizeof(buttonsDown), false);
			std::fill(buttonsUp, buttonsUp + sizeof(buttonsUp), false);
		}

		void Mouse::MousePosCallback(int _x, int _y)
		{
			x = _x;
			y = _y;
		}

		void Mouse::MouseButtonCallback(sf::Mouse::Button button)
		{
			if (button < 0)
				return;

			bool pressed = sf::Mouse::isButtonPressed(button);
			if (pressed && buttons[button] == false)
			{
				buttonsDown[button] = true;
				buttonsUp[button] = false;
			}

			if (!pressed && buttons[button] == true)
			{
				buttonsDown[button] = false;
				buttonsUp[button] = true;
			}

			buttons[button] = pressed;
		}

		int Mouse::GetMouseX()
		{
			return x;
		}

		int Mouse::GetMouseY()
		{
			return y;
		}

		bool Mouse::ButtonDown(sf::Mouse::Button button)
		{
			return buttonsDown[button];
		}

		bool Mouse::ButtonUp(sf::Mouse::Button button)
		{
			return buttonsUp[button];
		}

		bool Mouse::Button(sf::Mouse::Button button)
		{
			return buttons[button];
		}
	}
}