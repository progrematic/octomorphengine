#include "Octomorph/Input/Joystick.h"

#include <Octomorph/Globals.h>

namespace Octomorph
{
	namespace Input
	{
		float Joystick::deadzone = 50.f; //0.0f;// 15f;
		map<int, array<bool, Joystick::Button::ButtonSize>> Joystick::buttons;
		map<int, array<bool, Joystick::Button::ButtonSize>> Joystick::lastButtons;
		map<int, array<float, Joystick::Axis::AxisSize>> Joystick::axes;
		map<int, array<float, Joystick::Axis::AxisSize>> Joystick::lastAxes;
		map<int, bool> Joystick::availableJoysticks;
		float Joystick::axisButtonThreshold = 50;

		void Joystick::Initialize()
		{
			availableJoysticks.clear();
			buttons.clear();
			lastButtons.clear();
			for (int i = 0; i < sf::Joystick::Count; i++)
			{
				availableJoysticks.insert(pair<int, bool>(i, sf::Joystick::isConnected(i) != 0));
				if (sf::Joystick::isConnected(i))
				{
					buttons.insert(pair<int, array<bool, Joystick::Button::ButtonSize>>(i, { {false } }));
					lastButtons.insert(pair<int, array<bool, Joystick::Button::ButtonSize>>(i, { { false } }));
				}
			}
		}

		void Joystick::Update()
		{
			for (unsigned int i = 0; i < availableJoysticks.size(); i++)
			{
				if (availableJoysticks[i] == NULL)
				{
					//If this is true, the next line will crash. This is kept in so that I can get around to fixing it someday
				}
				availableJoysticks[i] = sf::Joystick::isConnected(i) != 0;
				if (availableJoysticks[i])
				{
					int count = sf::Joystick::getButtonCount(i);

					lastButtons[i] = buttons[i];
					for (int j = 0; j < count; j++)
					{
						buttons[i][j] = sf::Joystick::isButtonPressed(i, j);
					}

					lastAxes[i] = axes[i];
					for (int j = 0; j < Axis::AxisSize; j++)
					{
						axes[i][j] = sf::Joystick::getAxisPosition(i, static_cast<sf::Joystick::Axis>(j));
					}
				}
			}
		}

		bool Joystick::GetButton(int joystickID, Button button)
		{
			if (joystickID < 0)
			{
				for (unsigned int i = 0; i < availableJoysticks.size(); i++)
				{
					if (availableJoysticks[i] && GetButton(i, button))
					{
						return true;
					}
				}
				return false;
			}
			else
				return buttons[joystickID][button];
		}

		bool Joystick::GetButtonDown(int joystickID, Button button)
		{
			if (joystickID < 0)
			{
				for (unsigned int i = 0; i < availableJoysticks.size(); i++)
				{
					if (availableJoysticks[i] && GetButtonDown(i, button))
					{
						return true;
					}
				}
				return false;
			}
			else
				return (lastButtons[joystickID][button] == false && buttons[joystickID][button] == true);
		}

		bool Joystick::GetButtonUp(int joystickID, Button button)
		{
			if (joystickID < 0)
			{
				for (unsigned int i = 0; i < availableJoysticks.size(); i++)
				{
					if (availableJoysticks[i] && GetButtonUp(i, button))
					{
						return true;
					}
				}
				return false;
			}
			else
				return (lastButtons[joystickID][button] == true && buttons[joystickID][button] == false);
		}

		bool Joystick::GetAxisButton(int joystickID, Joystick::AxisButton button)
		{
			if (joystickID < 0)
			{
				for (unsigned int i = 0; i < availableJoysticks.size(); i++)
				{
					if (availableJoysticks[i] && GetAxisButton(i, button))
					{
						return true;
					}
				}
				return false;
			}
			else
			{
				switch (button)
				{
				case AxisButton::LeftStick_Left:
					return axes[joystickID][Axis::LeftStickHorizontal] < -axisButtonThreshold;
					break;
				case AxisButton::LeftStick_Right:
					return axes[joystickID][Axis::LeftStickHorizontal] > axisButtonThreshold;
					break;
				case AxisButton::LeftStick_Down:
					return axes[joystickID][Axis::LeftStickVertical] > axisButtonThreshold;
					break;
				case AxisButton::LeftStick_Up:
					return axes[joystickID][Axis::LeftStickVertical] < -axisButtonThreshold;
					break;
				case AxisButton::LeftTrigger:
					return axes[joystickID][Axis::Triggers] > axisButtonThreshold;
					break;
				case AxisButton::RightStick_Left:
					return axes[joystickID][Axis::RightStickHorizontal] < -axisButtonThreshold;
					break;
				case AxisButton::RightStick_Right:
					return axes[joystickID][Axis::RightStickHorizontal] > axisButtonThreshold;
					break;
				case AxisButton::RightStick_Down:
					return axes[joystickID][Axis::RightStickVertical] > axisButtonThreshold;
					break;
				case AxisButton::RightStick_Up:
					return axes[joystickID][Axis::RightStickVertical] < -axisButtonThreshold;
					break;
				case AxisButton::RightTrigger:
					return axes[joystickID][Axis::Triggers] < -axisButtonThreshold;
					break;
				default:
					return false;
				}
			}
		}

		bool Joystick::GetAxisButtonDown(int joystickID, Joystick::AxisButton button)
		{
			if (joystickID < 0)
			{
				for (unsigned int i = 0; i < availableJoysticks.size(); i++)
				{
					if (availableJoysticks[i] && GetAxisButtonDown(i, button))
					{
						return true;
					}
				}
				return false;
			}
			else
			{
				switch (button)
				{
				case AxisButton::LeftStick_Left:
					return axes[joystickID][Axis::LeftStickHorizontal] < -axisButtonThreshold && lastAxes[joystickID][Axis::LeftStickHorizontal] >= -axisButtonThreshold;
					break;
				case AxisButton::LeftStick_Right:
					return axes[joystickID][Axis::LeftStickHorizontal] > axisButtonThreshold && lastAxes[joystickID][Axis::LeftStickHorizontal] <= axisButtonThreshold;
					break;
				case AxisButton::LeftStick_Down:
					return axes[joystickID][Axis::LeftStickVertical] > axisButtonThreshold && lastAxes[joystickID][Axis::LeftStickVertical] <= axisButtonThreshold;
					break;
				case AxisButton::LeftStick_Up:
					return axes[joystickID][Axis::LeftStickVertical] < -axisButtonThreshold && lastAxes[joystickID][Axis::LeftStickVertical] >= -axisButtonThreshold;
					break;
				case AxisButton::LeftTrigger:
					return axes[joystickID][Axis::Triggers] > axisButtonThreshold && lastAxes[joystickID][Axis::Triggers] <= axisButtonThreshold;
					break;
				case AxisButton::RightStick_Left:
					return axes[joystickID][Axis::RightStickHorizontal] < -axisButtonThreshold && lastAxes[joystickID][Axis::RightStickHorizontal] >= -axisButtonThreshold;
					break;
				case AxisButton::RightStick_Right:
					return axes[joystickID][Axis::RightStickHorizontal] > axisButtonThreshold && lastAxes[joystickID][Axis::RightStickHorizontal] <= axisButtonThreshold;
					break;
				case AxisButton::RightStick_Down:
					return axes[joystickID][Axis::RightStickVertical] > axisButtonThreshold && lastAxes[joystickID][Axis::RightStickVertical] <= axisButtonThreshold;
					break;
				case AxisButton::RightStick_Up:
					return axes[joystickID][Axis::RightStickVertical] < -axisButtonThreshold && lastAxes[joystickID][Axis::RightStickVertical] >= -axisButtonThreshold;
					break;
				case AxisButton::RightTrigger:
					return axes[joystickID][Axis::Triggers] < -axisButtonThreshold && lastAxes[joystickID][Axis::Triggers] >= -axisButtonThreshold;
					break;
				default:
					return false;
				}
			}
		}

		bool Joystick::GetAxisButtonUp(int joystickID, Joystick::AxisButton button)
		{
			if (joystickID < 0)
			{
				for (unsigned int i = 0; i < availableJoysticks.size(); i++)
				{
					if (availableJoysticks[i] && GetAxisButtonUp(i, button))
					{
						return true;
					}
				}
				return false;
			}
			else
			{
				switch (button)
				{
				case AxisButton::LeftStick_Left:
					return axes[joystickID][Axis::LeftStickHorizontal] > -axisButtonThreshold && lastAxes[joystickID][Axis::LeftStickHorizontal] <= -axisButtonThreshold;
					break;
				case AxisButton::LeftStick_Right:
					return axes[joystickID][Axis::LeftStickHorizontal] < axisButtonThreshold && lastAxes[joystickID][Axis::LeftStickHorizontal] >= axisButtonThreshold;
					break;
				case AxisButton::LeftStick_Down:
					return axes[joystickID][Axis::LeftStickVertical] < axisButtonThreshold && lastAxes[joystickID][Axis::LeftStickVertical] >= axisButtonThreshold;
					break;
				case AxisButton::LeftStick_Up:
					return axes[joystickID][Axis::LeftStickVertical] > -axisButtonThreshold && lastAxes[joystickID][Axis::LeftStickVertical] <= -axisButtonThreshold;
					break;
				case AxisButton::LeftTrigger:
					return axes[joystickID][Axis::Triggers] < axisButtonThreshold && lastAxes[joystickID][Axis::Triggers] >= axisButtonThreshold;
					break;
				case AxisButton::RightStick_Left:
					return axes[joystickID][Axis::RightStickHorizontal] > -axisButtonThreshold && lastAxes[joystickID][Axis::RightStickHorizontal] <= -axisButtonThreshold;
					break;
				case AxisButton::RightStick_Right:
					return axes[joystickID][Axis::RightStickHorizontal] < axisButtonThreshold && lastAxes[joystickID][Axis::RightStickHorizontal] >= axisButtonThreshold;
					break;
				case AxisButton::RightStick_Down:
					return axes[joystickID][Axis::RightStickVertical] < axisButtonThreshold && lastAxes[joystickID][Axis::RightStickVertical] >= axisButtonThreshold;
					break;
				case AxisButton::RightStick_Up:
					return axes[joystickID][Axis::RightStickVertical] > -axisButtonThreshold && lastAxes[joystickID][Axis::RightStickVertical] <= -axisButtonThreshold;
					break;
				case AxisButton::RightTrigger:
					return axes[joystickID][Axis::Triggers] > -axisButtonThreshold && lastAxes[joystickID][Axis::Triggers] <= -axisButtonThreshold;
					break;
				default:
					return false;
				}
			}
		}

		float Joystick::GetAxis(int joystickID, Axis axis)
		{
			if (axis == Axis::Triggers)
				printf("WARNING: Left and Right Triggers share an axis - Use Get<Left/Right>Trigger() to split the Triggers axis for each side!");
			return abs(axes[joystickID][axis]) > deadzone ? axes[joystickID][axis] / 100.0f : 0;
		}

		float Joystick::GetLeftAxisForce(int joystickID)
		{
			return sqrt(pow(GetAxis(joystickID, Axis::LeftStickVertical), 2) + pow(GetAxis(joystickID, Axis::LeftStickHorizontal), 2));
		}

		float Joystick::GetRightAxisForce(int joystickID)
		{
			return sqrt(pow(GetAxis(joystickID, Axis::RightStickVertical), 2) + pow(GetAxis(joystickID, Axis::RightStickHorizontal), 2));
		}

		float Joystick::GetLeftAxisAngle(int joystickID)
		{
			float lv = -Joystick::GetAxis(joystickID, Joystick::Axis::LeftStickVertical);
			float lh = Joystick::GetAxis(joystickID, Joystick::Axis::LeftStickHorizontal);
			if (OctomorphMath::IsAround(lh, 0, 0.01f))
				lh = 0.01f;
			float deg = OctomorphMath::ToDegrees(atan(lv / lh));

			if (lh < 0)
				deg += 180;
			else if (lv < 0)
				deg += 360;
			return deg;
		}

		float Joystick::GetRightAxisAngle(int joystickID)
		{
			float rv = -Joystick::GetAxis(joystickID, Joystick::Axis::RightStickVertical);
			float rh = Joystick::GetAxis(joystickID, Joystick::Axis::RightStickHorizontal);
			if (OctomorphMath::IsAround(rh, 0, 0.01f))
				rh = 0.01f;
			float deg = OctomorphMath::ToDegrees(atan(rv / rh));

			if (rh < 0)
				deg += 180;
			else if (rv < 0)
				deg += 360;
			return deg;
		}

		float Joystick::GetLeftTrigger(int joystickID)
		{
			if (joystickID < 0)
			{
				for (unsigned int i = 0; i < availableJoysticks.size(); i++)
				{
					if (availableJoysticks[i] && GetLeftTrigger(i))
					{
						return true;
					}
				}
				return false;
			}
			else
				return axes[joystickID][Axis::Triggers] < 0 ? 0 : axes[joystickID][Axis::Triggers];
		}

		float Joystick::GetRightTrigger(int joystickID)
		{
			if (joystickID < 0)
			{
				for (unsigned int i = 0; i < availableJoysticks.size(); i++)
				{
					if (availableJoysticks[i] && GetRightTrigger(i))
					{
						return true;
					}
				}
				return false;
			}
			else
				return axes[joystickID][Axis::Triggers] > 0 ? 0 : -axes[joystickID][Axis::Triggers];
		}

		void Joystick::PrintDebug(int joystickID)
		{
			if (availableJoysticks.find(joystickID) != availableJoysticks.end())
			{
				cout << "Joystick " << joystickID << endl;
				cout << "A: " << GetButton(joystickID, Button::A);
				cout << "\tB: " << GetButton(joystickID, Button::B);
				cout << "\tX: " << GetButton(joystickID, Button::X);
				cout << "\tY: " << GetButton(joystickID, Button::Y) << endl;
				cout << "LB: " << GetButton(joystickID, Button::LB);
				cout << "\tLS: " << GetButton(joystickID, Button::LeftStick) << endl;
				cout << "RB: " << GetButton(joystickID, Button::RB);
				cout << "\tRS: " << GetButton(joystickID, Button::RightStick) << endl;
				cout << "Start: " << GetButton(joystickID, Button::Start);
				cout << "\tBack: " << GetButton(joystickID, Button::Back) << endl;
				cout << "LS H: " << GetAxis(joystickID, Axis::LeftStickHorizontal);
				cout << "\tLS V: " << GetAxis(joystickID, Axis::LeftStickVertical) << endl;
				cout << "RS H: " << GetAxis(joystickID, Axis::RightStickHorizontal);
				cout << "\tRS V: " << GetAxis(joystickID, Axis::RightStickVertical) << endl;
				cout << "Triggers: " << GetAxis(joystickID, Axis::Triggers) << endl;
				cout << "\tLT: " << GetLeftTrigger(joystickID) << "\tRT: " << GetRightTrigger(joystickID) << endl;
				cout << "DPAD Up: " << GetButton(joystickID, Button::DPAD_Up);
				cout << "\tDPAD Down: " << GetButton(joystickID, Button::DPAD_Down);
				cout << "\tDPAD Left: " << GetButton(joystickID, Button::DPAD_Left);
				cout << "\tDPAD Right: " << GetButton(joystickID, Button::DPAD_Right) << endl;
			}
		}

		void Joystick::PrintDebugSmall(int joystickID)
		{
			if (availableJoysticks.find(joystickID) != availableJoysticks.end())
			{
				cout << "Joystick " << joystickID << "::";
				cout << GetButton(joystickID, Button::A) << "::";
				cout << GetButton(joystickID, Button::B) << "::";
				cout << GetButton(joystickID, Button::X) << "::";
			}
		}

		void Joystick::PrintDebugAll()
		{
			for (unsigned int i = 0; i < availableJoysticks.size(); i++)
			{
				if (availableJoysticks[i])
					PrintDebugSmall(i);
			}
			cout << endl;
		}
	}
}