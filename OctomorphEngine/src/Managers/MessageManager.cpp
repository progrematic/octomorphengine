#include <Octomorph/Managers/MessageManager.h>

namespace Octomorph
{
	namespace Managers
	{
		MessageManager* MessageManager::mInstance = nullptr;

		MessageManager* MessageManager::GetInstance()
		{
			if (mInstance == nullptr)
				mInstance = new MessageManager();
			return mInstance;
		}

		MessageManager::MessageManager() {}

		void MessageManager::SendMessage(std::shared_ptr<Octomorph::Messages::Message> msg)
		{
			for (Interfaces::IMessageHandler* handler : mHandlers)
			{
				if (handler != nullptr && handler->IsSubscribed(msg->GetType()))
				{
					handler->HandleMessage(*msg.get());
				}
			}
		}

		void MessageManager::AddHandler(Interfaces::IMessageHandler* handler)
		{
			std::vector<Interfaces::IMessageHandler*>::iterator it = std::find(mHandlers.begin(), mHandlers.end(), handler);
			if (it == mHandlers.end())
				mHandlers.push_back(handler);
		}

		void MessageManager::RemoveHandler(Interfaces::IMessageHandler* handler)
		{
			std::vector<Interfaces::IMessageHandler*>::iterator it = std::find(mHandlers.begin(), mHandlers.end(), handler);
			if (it != mHandlers.end())
				mHandlers.erase(it);
		}
	}
}
