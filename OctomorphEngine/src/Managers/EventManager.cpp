#include <Octomorph/Managers/EventManager.h>

namespace Octomorph
{
	namespace Managers
	{
		EventManager* EventManager::mInstance = nullptr;

		EventManager* EventManager::GetInstance()
		{
			if (mInstance == nullptr)
				mInstance = new EventManager();
			return mInstance;
		}

		EventManager::EventManager() {}

		void EventManager::Update()
		{
			mHandled = false;
		}

		void EventManager::AddHandler(std::shared_ptr<Interfaces::IEventHandler> handler)
		{
			mHandlers.push_back(handler);
		}

		void EventManager::RemoveHandler(std::shared_ptr<Interfaces::IEventHandler> handler)
		{
			std::deque<std::shared_ptr<Interfaces::IEventHandler>>::iterator it = std::find(mHandlers.begin(), mHandlers.end(), handler);
			if (it != mHandlers.end())
				mHandlers.erase(it);
		}

		void EventManager::HandleEvent(sf::Event e)
		{
			for (std::deque<std::shared_ptr<Interfaces::IEventHandler>>::reverse_iterator it = mHandlers.rbegin(); it < mHandlers.rend(); it++)
			{
				if ((*it)->HandleEvent(e))
				{
					mHandled = true;
					break;
				}
			}
		}

		bool EventManager::ConsumedThisFrame()
		{
			return mHandled;
		}
	}
}