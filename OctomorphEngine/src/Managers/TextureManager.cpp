#include <Octomorph/Managers/TextureManager.h>

namespace Octomorph
{
	namespace Managers
	{
		std::map<std::string, std::pair<int, std::unique_ptr<sf::Texture>>> TextureManager::mTextures;
		int TextureManager::mCurId = 0;

		TextureManager::TextureManager()
		{
		}

		int TextureManager::AddTexture(std::string path)
		{
			// Ensure we haven't already added this texture
			auto it = mTextures.find(path);
			if (it != mTextures.end())
				return it->second.first;

			mCurId++;

			std::unique_ptr<sf::Texture> texture = std::make_unique<sf::Texture>();
			if (!texture->loadFromFile(path))
				return -1;

			mTextures.insert(std::make_pair(path, std::make_pair(mCurId, std::move(texture))));
			return mCurId;
		}

		void TextureManager::RemoveTexture(int id)
		{
			for (auto it = mTextures.begin(); it != mTextures.end(); it++)
			{
				if (it->second.first == id)
				{
					mTextures.erase(it->first);
					break;	// We can break here since we ensure we don't have dupes in TextureManager::AddTexture
				}
			}
		}

		sf::Texture* TextureManager::GetTexture(int id)
		{
			for (auto it = mTextures.begin(); it != mTextures.end(); it++)
			{
				if (it->second.first == id)
					return &*it->second.second;
			}

			return nullptr;
		}
	}
}