#include <Octomorph/Managers/SwarmManager.h>
#include <Octomorph/Managers/ViewManager.h>

#include <Octomorph/Components/DynamicTransformComponent.h>

namespace Octomorph
{
	namespace Managers
	{
		SwarmManager::SwarmManager(sf::Vector2f pos, int agentCount) :
			mPos(pos)
		{
			mSpeed = 5;
			mSeparateSpeed = 100;
			mMaxSteer = 0.8f;
			mArrivalRadius = mSpeed * 20;
			mSeparationRadius = 30;
			mSeekFactor = 1;
			mSeparateFactor = 1.25f;
			mSettleDistance = 50;
			mSettleVelocity = 1;

			for (int i = 0; i < agentCount; i++)
			{
				std::shared_ptr<Components::DynamicTransformComponent> mob = std::make_shared<Components::DynamicTransformComponent>();
				sf::Vector2f offset;
				offset.x = static_cast<float>(OctomorphRandom::GetInt(-100, 100));
				offset.y = static_cast<float>(OctomorphRandom::GetInt(-100, 100));
				mob->SetPos(pos + offset);

				mAgents.push_back(mob);
			}
		}

		void SwarmManager::SetSwarmValues(float speed, float separateSpeed, float maxSteer, float arrivalRadius, float separationRadius,
			float seekFactor, float separateFactor, float settleDistance, float settleVelocity)
		{
			mSpeed = speed;
			mSeparateSpeed = separateSpeed;
			mMaxSteer = maxSteer;
			mArrivalRadius = arrivalRadius;
			mSeparationRadius = separationRadius;
			mSeekFactor = seekFactor;
			mSeparateFactor = separateFactor;
			mSettleDistance = settleDistance;
			mSettleVelocity = settleVelocity;
		}

		void SwarmManager::Update(float dt)
		{
			for (std::shared_ptr<Components::DynamicTransformComponent> agent : mAgents)
			{
				sf::Vector2f separate = Separate(agent);
				sf::Vector2f seek = Seek(agent, mPos);

				separate.x *= mSeparateFactor;
				separate.y *= mSeparateFactor;

				seek.x *= mSeekFactor;
				seek.y *= mSeekFactor;

				agent->AddForce(separate);
				agent->AddForce(seek);

				if (OctomorphMath::DistanceBetweenPoints(agent->GetPos(), mPos) < mSettleDistance &&
					OctomorphMath::Length(agent->GetVel()) < mSettleVelocity)
				{
					agent->SetVel(sf::Vector2f(0, 0));
				}

				agent->Update(dt);
				agent->ApplyVel(sf::Vector2f(1, 1));
			}
		}

		void SwarmManager::Draw(float dt)
		{
			for (std::shared_ptr<Components::DynamicTransformComponent> agent : mAgents)
			{
				sf::RectangleShape rc;
				rc.setFillColor(sf::Color::Red);
				rc.setSize(sf::Vector2f(25, 25));
				rc.setPosition(agent->GetPos() - sf::Vector2f(12.5, 12.5));
				ViewManager::GetInstance()->GetWindow().draw(rc);
			}
		}

		void SwarmManager::SetPos(sf::Vector2f pos)
		{
			mPos = pos;
		}

		sf::Vector2f SwarmManager::GetPos()
		{
			return mPos;
		}

		const std::vector<std::shared_ptr<Components::DynamicTransformComponent>>& SwarmManager::GetAgents()
		{
			return mAgents;
		}

		// PRIVATE

		sf::Vector2f SwarmManager::Seek(std::shared_ptr<Components::DynamicTransformComponent> agent, sf::Vector2f target)
		{
			sf::Vector2f vec = target - agent->GetPos();
			float vecMag = OctomorphMath::Length(vec);

			if (vecMag < mArrivalRadius)
			{
				float mapped = OctomorphMath::Map(vecMag, 0, mArrivalRadius, 0, mSpeed);
				vec = OctomorphMath::Normalize(vec, mapped);
			}
			else
			{
				vec = OctomorphMath::Normalize(vec, mSpeed);
			}

			// Formula for steering - desired velocity minus current velocity
			sf::Vector2f steer = vec - agent->GetVel();
			steer = OctomorphMath::Limit(steer, mMaxSteer);
			return steer;
		}

		sf::Vector2f SwarmManager::Separate(std::shared_ptr<Components::DynamicTransformComponent> agent)
		{
			sf::Vector2f vec = sf::Vector2f(0, 0);
			int neighbours = 0;

			for (std::shared_ptr<Components::DynamicTransformComponent> a : mAgents)
			{
				float dist = OctomorphMath::DistanceBetweenPoints(agent->GetPos(), a->GetPos());
				if (a != agent && dist < mSeparationRadius)
				{
					vec += agent->GetPos() - a->GetPos();
					neighbours++;
				}
			}

			if (neighbours > 0)
			{
				vec.x /= neighbours;
				vec.y /= neighbours;

				vec = OctomorphMath::Normalize(vec, mSeparateSpeed);

				// Formula for steering - desired velocity minus current velocity
				sf::Vector2f steer = vec - agent->GetVel();
				steer = OctomorphMath::Limit(steer, mMaxSteer);
				return steer;
			}

			return sf::Vector2f(0, 0);
		}
	}
}