#include <Octomorph/Managers/ViewManager.h>

namespace Octomorph
{
	namespace Managers
	{
		ViewManager* ViewManager::mInstance = nullptr;

		ViewManager::ViewManager()
		{
			if (mInstance != nullptr)
			{
				std::cout << "ERROR: Attempting to initialize a second ViewManager!" << std::endl;
				return;
			}
		}

		ViewManager* ViewManager::GetInstance()
		{
			if (!mInstance)
				mInstance = new ViewManager();

			return mInstance;
		}

		void ViewManager::Initialize(char* windowTitle, int resX, int resY, int bitDepth, int flags)
		{
			sf::VideoMode vMode(resX, resY, bitDepth);
			mWindow.create(vMode, windowTitle, flags);
			mWindow.setFramerateLimit(60);
			mWindow.setVerticalSyncEnabled(true);

			mResX = resX;
			mResY = resY;
			mPlayerNum = 0;

			for (int i = 0; i < ETOI(VIEW::COUNT); i++)
			{
				mViews[i].view = mWindow.getDefaultView();
				mViews[i].viewSpeed = 500;
				mViews[i].viewpoint = sf::Vector2f(0, 0);
				mViews[i].targetViewpoint = sf::Vector2f(0, 0);
			}

			mWindow.setView(ViewManager::GetInstance()->GetView(VIEW::PLAYER1));
		}

		void ViewManager::UpdatePlayerCount(int playerNum)
		{
			mPlayerNum = playerNum;

			switch (mPlayerNum)
			{
			case 0:
			{
				mViews[ETOI(VIEW::PLAYER1)].view.setViewport(sf::FloatRect(0, 0, 1, 1));
				mViews[ETOI(VIEW::PLAYER1)].view.setSize((float)mResX, (float)mResY);
			}
			break;
			case 1:
			{
				mViews[ETOI(VIEW::PLAYER1)].view.setViewport(sf::FloatRect(0, 0, 1, 1));
				mViews[ETOI(VIEW::PLAYER1)].view.setSize((float)mResX, (float)mResY);
			}
			break;
			case 2:
			{
				mViews[ETOI(VIEW::PLAYER1)].view.setViewport(sf::FloatRect(0, 0, 1, 0.5f));
				mViews[ETOI(VIEW::PLAYER2)].view.setViewport(sf::FloatRect(0, 0.5f, 1, 0.5f));
				mViews[ETOI(VIEW::PLAYER1)].view.setSize((float)mResX, (float)mResY * 0.5f);
				mViews[ETOI(VIEW::PLAYER2)].view.setSize((float)mResX, (float)mResY * 0.5f);
			}
			break;
			case 3:
			{
				mViews[ETOI(VIEW::PLAYER1)].view.setViewport(sf::FloatRect(0, 0, 1, 0.5f));
				mViews[ETOI(VIEW::PLAYER2)].view.setViewport(sf::FloatRect(0, 0.5f, 0.5f, 0.5f));
				mViews[ETOI(VIEW::PLAYER3)].view.setViewport(sf::FloatRect(0.5f, 0.5f, 0.5f, 0.5f));
				mViews[ETOI(VIEW::PLAYER1)].view.setSize((float)mResX, (float)mResY * 0.5f);
				mViews[ETOI(VIEW::PLAYER2)].view.setSize((float)mResX * 0.5f, (float)mResY * 0.5f);
				mViews[ETOI(VIEW::PLAYER3)].view.setSize((float)mResX * 0.5f, (float)mResY * 0.5f);
			}
			break;
			case 4:
			{
				mViews[ETOI(VIEW::PLAYER1)].view.setViewport(sf::FloatRect(0, 0, 0.5f, 0.5f));
				mViews[ETOI(VIEW::PLAYER2)].view.setViewport(sf::FloatRect(0.5f, 0, 0.5f, 0.5f));
				mViews[ETOI(VIEW::PLAYER3)].view.setViewport(sf::FloatRect(0, 0.5f, 0.5f, 0.5f));
				mViews[ETOI(VIEW::PLAYER4)].view.setViewport(sf::FloatRect(0.5f, 0.5f, 0.5f, 0.5f));
				mViews[ETOI(VIEW::PLAYER1)].view.setSize((float)mResX * 0.5f, (float)mResY * 0.5f);
				mViews[ETOI(VIEW::PLAYER2)].view.setSize((float)mResX * 0.5f, (float)mResY * 0.5f);
				mViews[ETOI(VIEW::PLAYER3)].view.setSize((float)mResX * 0.5f, (float)mResY * 0.5f);
				mViews[ETOI(VIEW::PLAYER4)].view.setSize((float)mResX * 0.5f, (float)mResY * 0.5f);
			}
			break;
			}
		}

		sf::RenderWindow& ViewManager::GetWindow()
		{
			return mWindow;
		}

		void ViewManager::SetView(VIEW view)
		{
			mWindow.setView(mViews[ETOI(view)].view);
		}

		sf::View& ViewManager::GetView(VIEW view)
		{
			return mViews[ETOI(view)].view;
		}

		void ViewManager::SetGameViewpointSpeed(VIEW view, float speed)
		{
			mViews[ETOI(view)].viewSpeed = speed;
		}

		void ViewManager::SetGameViewpointTarget(VIEW view, sf::Vector2f target)
		{
			mViews[ETOI(view)].targetViewpoint = target;
		}

		void ViewManager::SetGameViewpoint(VIEW view, sf::Vector2f target)
		{
			mViews[ETOI(view)].targetViewpoint = target;
			mViews[ETOI(view)].viewpoint = target;
		}

		void ViewManager::Update(float dt)
		{
			for (int i = 0; i < ETOI(VIEW::COUNT); i++)
			{
				mViews[i].viewpoint = OctomorphMath::MoveTowards(mViews[i].viewpoint, mViews[i].targetViewpoint, mViews[i].viewSpeed * dt);
				mViews[i].view.setCenter(mViews[i].viewpoint);
			}
		}
	}
}
