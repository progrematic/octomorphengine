#include <Octomorph/Managers/TimeManager.h>

namespace Octomorph
{
	namespace Managers
	{
		TimeManager* TimeManager::mInstance = nullptr;

		TimeManager::TimeManager()
		{
			if (mInstance != nullptr)
			{
				std::cout << "ERROR: Attempting to initialize a second TimeManager!" << std::endl;
				return;
			}
		}

		TimeManager::~TimeManager() {}

		TimeManager* TimeManager::GetInstance()
		{
			if (!mInstance)
				mInstance = new TimeManager();

			return mInstance;
		}

		void TimeManager::StartTimer(std::string name)
		{
			std::map<std::string, TimeData>::iterator it = mTimeData.find(name);
			if (it == mTimeData.end())
			{
				mTimeData.insert(std::pair<std::string, TimeData>(name, TimeData()));
				it = mTimeData.find(name);
			}

			it->second.timed = false;
			it->second.startTime = high_resolution_clock::now();
		}

		float TimeManager::StopTimer(std::string name, bool printDuration)
		{
			TimeData& td = mTimeData[name];
			td.timed = true;
			td.endTime = high_resolution_clock::now();
			td.timeSpan = td.endTime - td.startTime;

			if (printDuration)
				PrintDuration(name);

			return GetDuration(name);
		}

		float TimeManager::GetDuration(std::string name)
		{
			TimeData& td = mTimeData[name];
			if (td.timed)
				return (float)td.timeSpan.count();
			return -1;
		}

		void TimeManager::PrintDuration(std::string name)
		{
			std::cout << "TimeManager: That took " << GetDuration(name) << "ms (" << name << ")\n";
		}
	}
}
