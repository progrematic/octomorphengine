#include <Octomorph/Managers/NetworkManager.h>

namespace Octomorph
{
	namespace Managers
	{
		NetworkManager* NetworkManager::mInstance = nullptr;

		NetworkManager::NetworkManager()
		{
			if (mInstance != nullptr)
			{
				std::cout << "ERROR: Attempting to initialize a second NetworkManager!" << std::endl;
				return;
			}
		}

		NetworkManager* NetworkManager::GetInstance()
		{
			if (!mInstance)
				mInstance = new NetworkManager();

			return mInstance;
		}

		std::string NetworkManager::GetLocalIp()
		{
			return sf::IpAddress::getLocalAddress().toString();
		}

		void NetworkManager::Host(int port)
		{
			mListenPort = port;
			mListenThread = std::thread(&NetworkManager::TryListenAsync, this);
		}

		void NetworkManager::TryListenAsync()
		{
			if (mTCPListener.listen(mListenPort) != sf::Socket::Done)
			{
				std::cout << "ERROR! Could not listen on port: " << mListenPort << std::endl;
				mListen = false;
			}
			mListen = true;
			mListenThread.detach();
			mListenThread = std::thread(&NetworkManager::ListenAsync, this);
		}

		void NetworkManager::ListenAsync()
		{
			while (mListen)
			{
				std::unique_ptr<sf::TcpSocket> client = std::make_unique<sf::TcpSocket>();
				if (mTCPListener.accept(*client) != sf::Socket::Done)
				{
					std::cout << "ERROR! Could not accept client: " << client->getRemoteAddress() << std::endl;
				}

				std::cout << client->getRemoteAddress() << " connected!" << std::endl;
				sf::Packet packet;
				std::string msg = "HELLO from the Server!";
				client->send(packet);
				mTCPClients.push_back(std::move(client));
				mReady = true;
			}
		}

		void NetworkManager::Join(std::string address, int port)
		{
			mServerAddress = address;
			mServerPort = port;
			mConnectedToServer = false;
			mThread = std::thread(&NetworkManager::TryJoinAsync, this);
		}

		void NetworkManager::TryJoinAsync()
		{
			sf::Socket::Status status = mTCPServer.connect(mServerAddress, mServerPort);
			if (status != sf::Socket::Done)
			{
				std::cout << "ERROR! Could not connect to host: " << mServerAddress << ":" << mServerPort << std::endl;
				mConnectedToServer = false;
			}

			std::cout << "Successfully connected to host!" << std::endl;
			mConnectedToServer = true;

			sf::Packet packet;
			mTCPServer.receive(packet);
			std::string msg;
			packet >> msg;

			std::cout << "SERVER: " << msg << std::endl;
			mReady = true;
		}

		bool NetworkManager::IsReady()
		{
			return mReady;
		}

		void NetworkManager::Update()
		{

		}
	}
}