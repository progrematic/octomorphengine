#include <Octomorph/Managers/LightManager.h>
#include <Octomorph/Managers/TextureManager.h>
#include <Octomorph/Globals.h>
#include <Octomorph/Managers/ViewManager.h>

namespace Octomorph
{
	namespace Managers
	{
		LightManager* LightManager::mInstance = nullptr;

		LightManager::LightManager()
		{
			if (mInstance != nullptr)
			{
				std::cout << "ERROR: Attempting to initialize a second LightManager!" << std::endl;
				return;
			}
		}

		LightManager::~LightManager()
		{
			if (mRunning)
			{
				Cleanup();
			}
		}

		LightManager* LightManager::GetInstance()
		{
			if (!mInstance)
				mInstance = new LightManager();

			return mInstance;
		}

		void LightManager::Cleanup()
		{
			ClearLightSources();
			mRunning = false;
			if (thread.joinable())
				thread.join();
		}

		void LightManager::Initialize()
		{
			mFogShader.loadFromFile("Resources/Shaders/Fog.frag", sf::Shader::Fragment);
		}

		void LightManager::Reset(sf::Vector2f worldSize, int tileSize)
		{
			mTileSize = tileSize;
			ClearLightSources();
			mWorldHeight = static_cast<float>(worldSize.y) * mTileSize;
			mFogTexture.create(static_cast<unsigned int>(worldSize.x) * mTileSize, static_cast<unsigned int>(mWorldHeight));

			mRunning = false;
			if (thread.joinable())
				thread.join();

			mRunning = true;
			thread = std::thread(&LightManager::Run, this);
		}

		void LightManager::Run()
		{
			while (mRunning)
			{
				using namespace std::chrono_literals;
				std::this_thread::sleep_for(.01s);

				std::lock_guard<std::mutex> guard(mutex);
				int i = 0;
				for (std::shared_ptr<Components::LightSourceComponent> lightSource : mLightSources)
				{
					sf::Vector2f windowPos = lightSource->GetPos();
					windowPos.y *= -1;
					windowPos.y += mWorldHeight;
					mLightSourceInfo.pos[i] = windowPos;
					mLightSourceInfo.size[i] = lightSource->GetSize();
					mLightSourceInfo.radius[i] = lightSource->GetRadius();
					mLightSourceInfo.waning[i] = lightSource->GetWaning();
					mLightSourceInfo.color[i] = lightSource->GetColor();
					i++;
				}

				mFogShader.setUniform("arrSize", i);
				mFogShader.setUniform("pixelSize", 1);
				mFogShader.setUniformArray("posArr", mLightSourceInfo.pos, MAX_LIGHTS);
				mFogShader.setUniformArray("sizArr", mLightSourceInfo.size, MAX_LIGHTS);
				mFogShader.setUniformArray("radArr", mLightSourceInfo.radius, MAX_LIGHTS);
				mFogShader.setUniformArray("wanArr", mLightSourceInfo.waning, MAX_LIGHTS);
				mFogShader.setUniformArray("colArr", mLightSourceInfo.color, MAX_LIGHTS);
			}
		}

		void LightManager::Draw(float dt)
		{
			mFogTexture.clear(sf::Color::Transparent);
			mFogTexture.draw(sf::Sprite(mFogTexture.getTexture()), &mFogShader);
			mFogTexture.display();

			ViewManager::GetInstance()->GetWindow().draw(sf::Sprite(mFogTexture.getTexture()));
		}

		void LightManager::AddLightSource(std::shared_ptr<Components::LightSourceComponent> lightSource)
		{
			std::lock_guard<std::mutex> guard(mutex);
			mLightSources.push_back(lightSource);
		}

		void LightManager::ClearLightSources()
		{
			std::lock_guard<std::mutex> guard(mutex);
			mLightSources.clear();
		}

		void LightManager::LightRect(sf::IntRect rect, float discoverRange)
		{
			float rectLeft = static_cast<float>(rect.left - 1) * mTileSize;
			float rectRight = static_cast<float>(rect.left + rect.width) * mTileSize + mTileSize;
			float rectTop = static_cast<float>(rect.top - 1) * mTileSize;
			float rectBot = static_cast<float>(rect.top + rect.height) * mTileSize + mTileSize;

			std::shared_ptr<Components::LightSourceComponent> ls = std::make_shared<Components::LightSourceComponent>();
			ls->SetPos(sf::Vector2f((rectLeft + rectRight) / 2, (rectTop + rectBot) / 2));
			ls->SetSize(sf::Vector2f(rectRight - rectLeft, rectBot - rectTop));
			ls->SetWaning(discoverRange * mTileSize);
			AddLightSource(ls);
		}
	}
}