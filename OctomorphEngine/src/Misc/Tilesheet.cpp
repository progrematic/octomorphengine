#include <Octomorph/Misc//Tilesheet.h>

#include <Octomorph/Managers/TextureManager.h>
#include <Octomorph/IO/json.h>

namespace Octomorph
{
	namespace Misc
	{
		Tilesheet::Tilesheet()
		{
			int spriteId = Managers::TextureManager::AddTexture("Resources/notfound.png");
			if (spriteId >= 0)
				mSpritesheet.setTexture(*Managers::TextureManager::GetTexture(spriteId));
		}

		void Tilesheet::Load(std::string spritePath, std::string jsonPath)
		{
			int spriteId = Managers::TextureManager::AddTexture(spritePath);
			if (spriteId >= 0)
				mSpritesheet.setTexture(*Managers::TextureManager::GetTexture(spriteId));

			Json::Value data;
			std::ifstream file;
			file.open(jsonPath);

			if (file.is_open())
			{
				file >> data;
				mHorTiles = data["horTiles"].asInt();
				mVerTiles = data["verTiles"].asInt();
				mTileWidth = data["frameWidth"].asInt();
				mTileHeight = data["frameHeight"].asInt();
			}
			else
			{
				std::cout << "ERROR! Could not load tilesheet data: " << jsonPath << std::endl;
			}
		}

		void Tilesheet::GetTile(int index, sf::Sprite& sprite)
		{
			if (index >= 0 && index <= mHorTiles * mVerTiles)
			{
				sf::Vector2i frame = GetFrameFromIndex(index);
				sprite.setTexture(*mSpritesheet.getTexture());
				sprite.setTextureRect(sf::IntRect(frame.x * mTileWidth,
					frame.y * mTileHeight,
					mTileWidth, mTileHeight));
			}
			else
			{
				std::cout << "ERROR! Could not get tilesheet tile (out of bounds): " << index << std::endl;
			}
		}

		// PRIVATE

		sf::Vector2i Tilesheet::GetFrameFromIndex(int index)
		{
			sf::Vector2i ret;

			ret.x = index % mHorTiles;
			ret.y = index / mHorTiles;

			return ret;
		}
	}
}
