#include <Octomorph/Misc//IntervalTimer.h>

namespace Octomorph
{
	namespace Misc
	{
		IntervalTimer::IntervalTimer()
		{
			mInterval = 0;
			mCallback = nullptr;
			mEnabled = false;
		}

		void IntervalTimer::Initialize(float interval, std::function<void(void)> callback)
		{
			mInterval = interval;
			mCallback = callback;
		}

		void IntervalTimer::Start()
		{
			mElapsed = 0;
			mEnabled = true;
		}

		void IntervalTimer::Stop()
		{
			mEnabled = false;
		}

		void IntervalTimer::Update(float dt)
		{
			if (mEnabled)
			{
				mElapsed += dt;
				if (mElapsed >= mInterval)
				{
					mElapsed = 0;
					mCallback();
				}
			}
		}
	}
}