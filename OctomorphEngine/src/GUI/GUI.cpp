#include <Octomorph/GUI/GUI.h>
#include <Octomorph/Managers/EventManager.h>
#include <Octomorph/Managers/ViewManager.h>
#include <Octomorph/Interfaces/IEventHandler.h>

namespace Octomorph
{
	namespace GUI
	{
		GUI::GUI() : mPos({ 0, 0 }) { Reset(); }
		GUI::GUI(sf::Vector2f pos) : mPos(pos) { Reset(); }

		void GUI::Reset()
		{
			Disable();
			mBorderSize = sf::Vector2f(0, 0);
			mTabbables.clear();
			mRegisters.clear();
			mWidgets.clear();
			mShape.setFillColor(sf::Color::Magenta);
		}

		void GUI::SetColor(sf::Color color)
		{
			mShape.setFillColor(color);
		}

		void GUI::SetImage(sf::Texture& texture)
		{
			std::shared_ptr<Widgets::Image> image = std::make_shared<Widgets::Image>();
			image->SetImage(texture);
			mBg = image;
		}

		void GUI::Enable()
		{
			mEnabled = true;
			if (mTabbables.size() > 0)
			{
				mCurActiveHandler = (*mTabbables.begin());
				mCurActiveHandler->SetActive(true);
			}

			for (std::shared_ptr<Interfaces::IEventHandler> handler : mRegisters)
			{
				Managers::EventManager::GetInstance()->AddHandler(handler);
			}
		}

		void GUI::Disable()
		{
			mEnabled = false;
			if (mCurActiveHandler != nullptr && mTabbables.size() > 0)
				mCurActiveHandler->SetActive(false);

			for (std::shared_ptr<Interfaces::IEventHandler> handler : mRegisters)
			{
				Managers::EventManager::GetInstance()->RemoveHandler(handler);
			}
		}

		void GUI::ToggleEnabled()
		{
			if (mEnabled)
				Disable();
			else
				Enable();
		}

		bool GUI::IsEnabled()
		{
			return mEnabled;
		}

		void GUI::NextTabbable()
		{
			if (mTabbables.size() > 0)
			{
				mCurActiveHandler->SetActive(false);
				auto it = std::find(mTabbables.begin(), mTabbables.end(), mCurActiveHandler);
				if (it == mTabbables.end() - 1)
					it = mTabbables.begin();
				else
					it++;
				mCurActiveHandler = (*it);
				mCurActiveHandler->SetActive(true);
			}
		}

		void GUI::PrevTabbable()
		{
			if (mTabbables.size() > 0)
			{
				mCurActiveHandler->SetActive(false);
				auto it = std::find(mTabbables.begin(), mTabbables.end(), mCurActiveHandler);
				if (it == mTabbables.begin())
					it = mTabbables.end() - 1;
				else
					it--;
				mCurActiveHandler = (*it);
				mCurActiveHandler->SetActive(true);
			}
		}

		void GUI::AddWidget(std::shared_ptr<Widgets::Widget> widget, bool registerEventHandler, bool tabbable)
		{
			mWidgets.push_back(widget);
			std::shared_ptr<Interfaces::IEventHandler> handler = std::dynamic_pointer_cast<Interfaces::IEventHandler>(widget);
			if (handler)
			{
				if (registerEventHandler)
					mRegisters.push_back(handler);
				if (tabbable)
					mTabbables.push_back(handler);
			}
		}

		sf::Vector2f GUI::GetPos()
		{
			return mPos;
		}

		void GUI::SetPos(sf::Vector2f pos)
		{
			mPos = pos;
		}

		void GUI::SetBorderSize(sf::Vector2f border)
		{
			mBorderSize = border;
		}

		sf::FloatRect GUI::GetRect()
		{
			sf::FloatRect ret;
			ret.left = 99999;
			ret.top = 99999;
			ret.width = -99999;
			ret.height = -99999;

			for (std::shared_ptr<Widgets::Widget> w : mWidgets)
			{
				ret.left = fmin(ret.left, w->GetPos().x);
				ret.top = fmin(ret.top, w->GetPos().y);
				ret.width = fmax(ret.width, w->GetPos().x + w->GetSize().x);
				ret.height = fmax(ret.height, w->GetPos().y + w->GetSize().y);
			}
			// We calculated the right and bot above, so let's convert that to actual width and height
			ret.width = ret.width - ret.left;
			ret.height = ret.height - ret.top;

			// Widget positions are relative to the parent (i.e., us)
			ret.left += mPos.x;
			ret.top += mPos.y;

			// Take Border Size into account
			ret.width += mBorderSize.x;
			ret.height += mBorderSize.y;
			ret.left -= mBorderSize.x / 2.f;
			ret.top -= mBorderSize.y / 2.f;

			return ret;
		}

		void LoadGUIFromFile(std::string path)
		{

		}

		void GUI::Update(float dt)
		{
			if (mEnabled)
			{
				sf::FloatRect rc = GetRect();

				if (mBg != nullptr)
				{
					mBg->SetPos(sf::Vector2f(rc.left, rc.top));
					mBg->SetSize(sf::Vector2f(rc.width, rc.height));
					mBg->Update(dt, sf::Vector2f(0, 0));
				}
				else
				{
					mShape.setPosition(sf::Vector2f(rc.left, rc.top));
					mShape.setSize(sf::Vector2f(rc.width, rc.height));
				}

				for (std::shared_ptr<Widgets::Widget> widget : mWidgets)
				{
					widget->Update(dt, mPos);
				}
			}
		}

		void GUI::Draw(float dt)
		{
			if (mEnabled)
			{
				if (mBg != nullptr)
				{
					mBg->Draw(dt, sf::Vector2f(0, 0));
				}
				else
				{
					Managers::ViewManager::GetInstance()->GetWindow().draw(mShape);
				}

				for (std::shared_ptr<Widgets::Widget> widget : mWidgets)
				{
					widget->Draw(dt, mPos);
				}
			}
		}
	}
}