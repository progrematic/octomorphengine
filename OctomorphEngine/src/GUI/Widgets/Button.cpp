#include <Octomorph/GUI/Widgets/Button.h>

#include <Octomorph/Managers/EventManager.h>
#include <Octomorph/Managers/ViewManager.h>
#include <Octomorph/Input/Mouse.h>

using namespace Octomorph::Input;

namespace Octomorph
{
	namespace GUI
	{
		namespace Widgets
		{
			Button::Button()
			{
				mStateColors.insert(std::pair<BUTTON_STATE, sf::Color>(BUTTON_STATE::NORMAL, sf::Color::White));
				mStateColors.insert(std::pair<BUTTON_STATE, sf::Color>(BUTTON_STATE::HOVER, sf::Color(200, 200, 200, 255)));
				mStateColors.insert(std::pair<BUTTON_STATE, sf::Color>(BUTTON_STATE::PRESSED, sf::Color(127, 127, 127, 255)));
				mStateImages.insert(std::pair<BUTTON_STATE, std::shared_ptr<Image>>(BUTTON_STATE::NORMAL, nullptr));
				mStateImages.insert(std::pair<BUTTON_STATE, std::shared_ptr<Image>>(BUTTON_STATE::HOVER, nullptr));
				mStateImages.insert(std::pair<BUTTON_STATE, std::shared_ptr<Image>>(BUTTON_STATE::PRESSED, nullptr));

				mCallback = nullptr;
				mCallbackWithArg = nullptr;
				mArg = nullptr;
				mSelected = false;
				mLabel.SetAnchor(sf::Vector2f(0.5f, 0.5f));
				mLabel.SetColor(sf::Color::Black);
				SetState(BUTTON_STATE::NORMAL);
			}

			void Button::SetColorForState(sf::Color color, BUTTON_STATE state)
			{
				mStateColors.at(state) = color;
			}

			void Button::SetImageForState(sf::Texture& image, BUTTON_STATE state)
			{
				std::shared_ptr<Image> ptrImage = std::make_shared<Image>();
				ptrImage->SetImage(image);
				mStateImages.at(state) = ptrImage;
			}

			void Button::SetCallback(std::function<void(void)> callback)
			{
				mCallback = callback;
			}

			void Button::SetCallback(std::function<void(void*)> callback, void* arg)
			{
				mCallbackWithArg = callback;
				mArg = arg;
			}

			void Button::SetState(BUTTON_STATE state)
			{
				mState = state;
				mShape.setFillColor(mStateColors.at(mState));
			}

			TextLabel& Button::GetLabel()
			{
				return mLabel;
			}

			bool Button::HandleEvent(sf::Event e)
			{
				bool handled = false;

				int x = Mouse::GetMouseX();
				int y = Mouse::GetMouseY();

				if (x > mRealPos.x && x < mRealPos.x + mSize.x &&
					y > mRealPos.y && y < mRealPos.y + mSize.y)
				{
					if (Mouse::Button(sf::Mouse::Left))
						SetState(BUTTON_STATE::PRESSED);

					if (Mouse::ButtonDown(sf::Mouse::Left))
					{
						handled = true;
						mSelected = true;
						if (mCallback != nullptr)
							mCallback();
						else if (mCallbackWithArg != nullptr)
							mCallbackWithArg(mArg);
					}
				}

				return handled;
			}

			void Button::Update(float dt, sf::Vector2f guiPos)
			{
				mRealPos = mPos + guiPos;
				mLabel.Update(dt, guiPos);
				int x = Mouse::GetMouseX();
				int y = Mouse::GetMouseY();

				if (Mouse::ButtonUp(sf::Mouse::Left))
				{
					mSelected = false;
				}

				SetState(BUTTON_STATE::NORMAL);
				if (x > mRealPos.x && x < mRealPos.x + mSize.x &&
					y > mRealPos.y && y < mRealPos.y + mSize.y)
				{
					SetState(BUTTON_STATE::HOVER);
				}

				if (mSelected)
				{
					SetState(BUTTON_STATE::PRESSED);
				}

				mLabel.SetPos(GetPosAtAnchor(sf::Vector2f(0.5f, 0.5f)));
				std::shared_ptr<Image> stateImage = mStateImages.at(mState);
				if (stateImage != nullptr)
				{
					stateImage->SetPos(GetPosAtAnchor(sf::Vector2f(0.5f, 0.5f)));
					stateImage->SetSize(mSize);
					stateImage->Update(dt, guiPos);
				}

				mShape.setSize(mSize);
			}

			void Button::Draw(float dt, sf::Vector2f guiPos)
			{
				mShape.setPosition(mPos + guiPos);

				std::shared_ptr<Image> stateImage = mStateImages.at(mState);
				if (stateImage != nullptr)
					stateImage->Draw(dt, guiPos);
				else
					Managers::ViewManager::GetInstance()->GetWindow().draw(mShape);
				mShape.setPosition(mPos - guiPos);

				mLabel.Draw(dt, guiPos);
			}

			void Button::Draw(float dt, sf::RenderTexture& texture)
			{
				std::shared_ptr<Image> stateImage = mStateImages.at(mState);
				if (stateImage != nullptr)
					stateImage->Draw(dt, texture);
				else
					texture.draw(mShape);

				mLabel.Draw(dt, texture);
			}
		}
	}
}