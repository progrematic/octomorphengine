#include <Octomorph/GUI/Widgets/Scroller.h>
#include <Octomorph/Managers/ViewManager.h>

namespace Octomorph
{
	namespace GUI
	{
		namespace Widgets
		{
			Scroller::Scroller()
			{
				mOrientation = Enums::ORIENTATION::HORIZONTAL;
				mInsideBG.setFillColor(sf::Color::White);
				mBorder.setFillColor(sf::Color::Black);
				mBorderSize = 5;
				mSpacing = 5;
				mMaskShader.loadFromFile("Resources/Shaders/Mask.frag", sf::Shader::Fragment);
			}

			void Scroller::SetOrientation(Enums::ORIENTATION orientation)
			{
				mOrientation = orientation;
			}

			void Scroller::SetMaskSize(sf::Vector2f size)
			{
				mMaskSize = size;
			}

			void Scroller::SetInsideColor(sf::Color color)
			{
				mInsideBG.setFillColor(color);
			}

			void Scroller::SetBorderColor(sf::Color color)
			{
				mBorder.setFillColor(color);
			}

			void Scroller::SetBorderSize(float size)
			{
				mBorder.setSize(mSize + sf::Vector2f(size * 2, size * 2));
			}

			void Scroller::SetSpacing(float spacing)
			{
				mSpacing = spacing;
			}

			void Scroller::AddWidget(std::shared_ptr<Widget> widget)
			{
				mWidgets.push_back(widget);
				RepositionWidgets();
			}

			void Scroller::Update(float dt, sf::Vector2f guiPos)
			{
				for (std::shared_ptr<Widget> widget : mWidgets)
				{
					widget->Update(dt, guiPos);
				}

				mMaskShader.setUniform("mask", mMaskSize);
				mInsideBG.setPosition(mPos);
				mBorder.setPosition(mPos - sf::Vector2f(mBorderSize * 0.5f, mBorderSize * 0.5f));

				DrawToTexture(dt);	// Must draw to texture before clearing window
			}

			void Scroller::Draw(float dt, sf::Vector2f guiPos)
			{
				sf::Sprite textureSprite(mTexture.getTexture());
				textureSprite.setPosition(mPos + guiPos);
				Octomorph::Managers::ViewManager::GetInstance()->GetWindow().draw(textureSprite);
			}

			void Scroller::Draw(float dt, sf::RenderTexture& texture)
			{
				sf::Sprite textureSprite(mTexture.getTexture());
				textureSprite.setPosition(mPos -sf::Vector2f(mSize.x / 2, mSize.y / 2));
				texture.draw(textureSprite);
			}

			// PRIVATE

			void Scroller::DrawToTexture(float dt)
			{
				mTexture.clear(sf::Color::Black);
				//mTexture.draw(mBorder);
				//mTexture.draw(mInsideBG);
				for (std::shared_ptr<Widget> widget : mWidgets)
				{
					widget->Draw(dt, mTexture);
					//widget->DebugDraw(dt, mTexture);
				}
				//mTexture.draw(sf::Sprite(mTexture.getTexture()), &mMaskShader);

				mTexture.display();
			}

			void Scroller::RepositionWidgets()
			{
				mWidgetCummulativeSize = sf::Vector2f(0, 0);
				float* extraSpacing = &mWidgetCummulativeSize.x;
				for (std::shared_ptr<Widget> widget : mWidgets)
				{
					switch (mOrientation)
					{
					case Enums::ORIENTATION::HORIZONTAL:
					{
						extraSpacing = &mWidgetCummulativeSize.x;
						widget->SetPos(sf::Vector2f(mWidgetCummulativeSize.x + mSpacing, mPos.y));
						widget->SetAnchor(sf::Vector2f(-0.5f, 0));
						mWidgetCummulativeSize.x += widget->GetSize().x + mSpacing;
						mWidgetCummulativeSize.y = OctomorphMath::Max(mWidgetCummulativeSize.y, widget->GetSize().y);
					}
					break;
					case Enums::ORIENTATION::VERTICAL:
					{
						extraSpacing = &mWidgetCummulativeSize.y;
						widget->SetPos(sf::Vector2f(mPos.x, mWidgetCummulativeSize.y + mSpacing));
						widget->SetAnchor(sf::Vector2f(0, -0.5f));
						mWidgetCummulativeSize.x = OctomorphMath::Max(mWidgetCummulativeSize.x, widget->GetSize().x);
						mWidgetCummulativeSize.y += widget->GetSize().y + mSpacing;
					}
					break;
					}
				}
				
				// Now that we know what our cummulative size is in the non-oriented direction, we can position
				//  our widgets right in the middle on that axis
				for (std::shared_ptr<Widget> widget : mWidgets)
				{
					switch (mOrientation)
					{
					case Enums::ORIENTATION::HORIZONTAL:
					{
						widget->SetPos(sf::Vector2f(widget->GetRealPos().x, mWidgetCummulativeSize.y * 0.5f));
					}
					break;
					case Enums::ORIENTATION::VERTICAL:
					{
						widget->SetPos(sf::Vector2f(mWidgetCummulativeSize.x * 0.5f, widget->GetRealPos().y));
					}
					break;
					}
				}
				
				(*extraSpacing) += mSpacing;
				mTexture.create(mWidgetCummulativeSize.x * 10, mWidgetCummulativeSize.y * 10);
				Widget::SetSize(mWidgetCummulativeSize);
			}
		}
	}
}