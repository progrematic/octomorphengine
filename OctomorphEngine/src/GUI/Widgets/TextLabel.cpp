#include <Octomorph/GUI/Widgets/TextLabel.h>

#include <Octomorph/Managers/ViewManager.h>

namespace Octomorph
{
	namespace GUI
	{
		namespace Widgets
		{
			TextLabel::TextLabel()
			{
				if (!mFont.loadFromFile("resources/fonts/arial.ttf"))
				{
					std::cout << "Error loading font" << std::endl;
				}

				mText.setFont(mFont);
				mText.setCharacterSize(18);
				mText.setFillColor(sf::Color::White);
				mText.setString("undefined");
				mText.setPosition(mPos);
			}

			void TextLabel::Update(float dt, sf::Vector2f guiPos)
			{
				mText.setPosition(mPos);
			}

			void TextLabel::Draw(float dt, sf::Vector2f guiPos)
			{
				mText.setPosition(mPos + guiPos);
				Managers::ViewManager::GetInstance()->GetWindow().draw(mText);
				mText.setPosition(mPos - guiPos);
			}

			void TextLabel::Draw(float dt, sf::RenderTexture& texture)
			{
				// We need this offset because sf::Texts are drawn from top-left, not center
				sf::Vector2f offset = sf::Vector2f(mSize.x / 2, mSize.y / 2);
				mText.setPosition(mPos - offset);
				texture.draw(mText);
				mText.setPosition(mPos + offset);
			}

			void TextLabel::SetFont(sf::Font font)
			{
				mFont = font;
			}

			void TextLabel::SetString(std::string s)
			{
				mText.setString(s);
				UpdateSize();
			}

			void TextLabel::SetColor(sf::Color c)
			{
				mText.setFillColor(c);
			}

			void TextLabel::SetFontSize(int size)
			{
				mText.setCharacterSize(size);
				UpdateSize();
			}

			int TextLabel::GetFontSize()
			{
				return mText.getCharacterSize();
			}

			// PRIVATE

			void TextLabel::UpdateSize()
			{
				// We add the charsize / 2 to the localbound height to account for letters like g and j that drop below the bounds
				SetSize(sf::Vector2f(mText.getLocalBounds().width, mText.getLocalBounds().height + static_cast<float>(mText.getCharacterSize() / 2)));
			}
		}
	}
}