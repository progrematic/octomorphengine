#include <Octomorph/GUI/Widgets/Input.h>

#include <Octomorph/Managers/ViewManager.h>
#include <Octomorph/Managers/EventManager.h>

namespace Octomorph
{
	namespace GUI
	{
		namespace Widgets
		{
			Input::Input()
			{
				mOutlineThickness = 2;
				mShape.setFillColor(sf::Color::White);
				mShape.setOutlineColor(sf::Color(100, 100, 100, 255));
				mShape.setOutlineThickness(mOutlineThickness);
				mCursor.setFillColor(sf::Color::Black);
				mCursor.setSize(sf::Vector2f(2, 40));

				mLabel.SetString("");
				mLabel.SetColor(sf::Color::Black);
				mLabel.SetFontSize(20);
				mLabel.SetAnchor(sf::Vector2f(0, 0.5f));

				mDrawCursor = false;
				mIntervalTimer.Initialize(0.5f, std::bind(&Input::ToggleCursor, this));

				mCallback = nullptr;
			}

			void Input::Update(float dt, sf::Vector2f guiPos)
			{
				mIntervalTimer.Update(dt);
				mLabel.Update(dt, guiPos);
				mShape.setSize(mSize);

				mLabel.SetString(mString);
				mLabel.SetPos(GetPosAtAnchor(sf::Vector2f(0, 0.5f)) + sf::Vector2f(2, 0));
			}

			void Input::Draw(float dt, sf::Vector2f guiPos)
			{
				mShape.setPosition(mPos + guiPos);
				mCursor.setPosition(mShape.getPosition() + sf::Vector2f(mLabel.GetSize().x + 4, 2.5f));
				Managers::ViewManager::GetInstance()->GetWindow().draw(mShape);
				mLabel.Draw(dt, guiPos);
				if (mDrawCursor)
					Managers::ViewManager::GetInstance()->GetWindow().draw(mCursor);
				mShape.setPosition(mPos - guiPos);
			}

			void Input::Draw(float dt, sf::RenderTexture& texture)
			{
				mCursor.setPosition(mShape.getPosition() + sf::Vector2f(mLabel.GetSize().x + 4, 2.5f));
				texture.draw(mShape);
				mLabel.Draw(dt, texture);
				if (mDrawCursor)
					texture.draw(mCursor);
			}

			void Input::SetActive(bool x)
			{
				IEventHandler::SetActive(x);

				std::shared_ptr<IEventHandler> handler = this->shared_from_this();

				if (x)
				{
					Managers::EventManager::GetInstance()->AddHandler(handler);
					mDrawCursor = true;
					mIntervalTimer.Start();
				}
				else
				{
					Managers::EventManager::GetInstance()->RemoveHandler(handler);
					mDrawCursor = false;
					mIntervalTimer.Stop();
				}
			}

			void Input::SetCallback(std::function<void(const std::string&)> callback)
			{
				mCallback = callback;
			}

			bool Input::HandleEvent(sf::Event e)
			{
				bool ret = false;
				if (e.type == sf::Event::TextEntered)
				{
					sf::Uint32 c = static_cast<sf::Uint32>(e.text.unicode);
					if (c == '\b' && mString.getSize() > 0)
						mString.erase(mString.getSize() - 1);
					else if (c > 31 && c < 128)
						mString += c;
					ret = true;
				}
				else if (e.type == sf::Event::KeyPressed)
				{
					if (e.key.code == sf::Keyboard::Return)
					{
						mCallback(mString.toAnsiString());
						mString.clear();
						ret = true;
					}
				}

				return ret;
			}

			// PRIVATE

			void Input::ToggleCursor()
			{
				mDrawCursor = !mDrawCursor;
			}
		}
	}
}