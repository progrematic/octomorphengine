#include <Octomorph/GUI/Widgets/Image.h>

#include <Octomorph/Managers/ViewManager.h>

namespace Octomorph
{
	namespace GUI
	{
		namespace Widgets
		{
			Image::Image()
			{

			}

			void Image::SetImage(sf::Texture& texture)
			{
				mSprite.setTexture(texture);
				mSprite.setTextureRect(sf::IntRect(0, 0, mSprite.getTexture()->getSize().x, mSprite.getTexture()->getSize().y));
				mSprite.setOrigin((float)texture.getSize().x / 2, (float)texture.getSize().y / 2);
			}

			void Image::Update(float dt, sf::Vector2f guiPos)
			{
				mSprite.setPosition(mPos);
				sf::Vector2f size = sf::Vector2f(mSprite.getLocalBounds().width, mSprite.getLocalBounds().height);
				sf::Vector2f scale;
				scale.x = mSize.x / size.x;
				scale.y = mSize.y / size.y;
				mSprite.setScale(scale);
			}

			void Image::Draw(float dt, sf::Vector2f guiPos)
			{
				mSprite.setPosition(mPos + guiPos);
				Managers::ViewManager::GetInstance()->GetWindow().draw(mSprite);
				mSprite.setPosition(mPos - guiPos);
			}

			void Image::Draw(float dt, sf::RenderTexture& texture)
			{
				texture.draw(mSprite);
			}
		}
	}
}