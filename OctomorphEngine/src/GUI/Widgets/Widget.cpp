#include <Octomorph/GUI/Widgets/Widget.h>
#include <Octomorph/Managers/ViewManager.h>

namespace Octomorph
{
	namespace GUI
	{
		namespace Widgets
		{
			Widget::Widget() : mPos({ 0, 0 }), mSize({ 100, 100 }) {}

			void Widget::SetPos(sf::Vector2f pos)
			{
				mRealPos = pos;
				UpdatePos();
			}

			void Widget::SetSize(sf::Vector2f size)
			{
				mSize = size;
				UpdatePos();
			}

			sf::Vector2f Widget::GetRealPos()
			{
				return mRealPos;
			}

			sf::Vector2f Widget::GetPosAtAnchor(sf::Vector2f anchor)
			{
				sf::Vector2f offset = sf::Vector2f(mSize.x * anchor.x, mSize.y * anchor.y);
				return mPos + offset;
			}

			void Widget::SetAnchor(sf::Vector2f anchor)
			{
				mAnchor = anchor;
				UpdatePos();
			}

			sf::Vector2f Widget::GetPos()
			{
				return mPos;
			}

			sf::Vector2f Widget::GetSize()
			{
				return mSize;
			}

			void Widget::DebugDraw(float dt, sf::Vector2f guiPos)
			{
				sf::RectangleShape bg;
				bg.setSize(mSize);
				bg.setPosition(GetPosAtAnchor(sf::Vector2f(-0.5f, -0.5f)));
				bg.setFillColor(sf::Color::Cyan);
				Octomorph::Managers::ViewManager::GetInstance()->GetWindow().draw(bg);

				sf::RectangleShape anchor;
				int anchorSize = 10;
				anchor.setSize(sf::Vector2f(anchorSize, anchorSize));
				anchor.setFillColor(sf::Color::Magenta);
				anchor.setPosition(guiPos + GetPosAtAnchor(mAnchor) - sf::Vector2f(anchorSize / 2, anchorSize / 2));
				Octomorph::Managers::ViewManager::GetInstance()->GetWindow().draw(anchor);
			}

			void Widget::DebugDraw(float dt, sf::RenderTexture& texture)
			{
				sf::RectangleShape bg;
				bg.setSize(mSize);
				bg.setPosition(GetPosAtAnchor(sf::Vector2f(-0.5f, -0.5f)));
				bg.setFillColor(sf::Color::Cyan);
				texture.draw(bg);

				sf::RectangleShape anchor;
				int anchorSize = 10;
				anchor.setSize(sf::Vector2f(anchorSize, anchorSize));
				anchor.setFillColor(sf::Color::Magenta);
				anchor.setPosition(GetPosAtAnchor(mAnchor) - sf::Vector2f(anchorSize / 2, anchorSize / 2));
				texture.draw(anchor);
			}

			// PRIVATE

			void Widget::UpdatePos()
			{
				sf::Vector2f offset = sf::Vector2f(mSize.x * mAnchor.x, mSize.y * mAnchor.y);
				mPos = mRealPos - offset;
			}
		}
	}
}