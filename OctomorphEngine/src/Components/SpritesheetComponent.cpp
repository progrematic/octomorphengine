#include "Octomorph/Components/SpritesheetComponent.h"
#include <Octomorph/Managers/ViewManager.h>
#include <Octomorph/Managers/TextureManager.h>

#include <Octomorph/IO/json.h>

namespace Octomorph
{
	namespace Components
	{
		SpritesheetComponent::SpritesheetComponent() :
			mLayerMax(1),
			mCurFrame(0),
			mDt(0),
			mError(true)
		{
			ClearSprites();
			LoadError();
		}

		void SpritesheetComponent::SetLayerMax(int drawOrderMax)
		{
			mLayerMax = drawOrderMax;
		}

		void SpritesheetComponent::LoadSpritesheet(std::string path)
		{
			Json::Value data;
			std::ifstream file;
			file.open(path);

			if (file.is_open())
			{
				mError = false;
				file >> data;
				mHorTiles = data["horTiles"].asInt();
				mVerTiles = data["verTiles"].asInt();
				mColWidth = data["colWidth"].asInt();
				mColHeight = data["colHeight"].asInt();
				mFrameWidth = data["frameWidth"].asInt();
				mFrameHeight = data["frameHeight"].asInt();

				mAnimations.clear();
				for (Json::Value::iterator it = data["animations"].begin(); it != data["animations"].end(); it++)
				{
					Animation anim;
					anim.speed = (*it)["speed"].asInt();
					for (Json::Value::iterator index = (*it)["spriteIndeces"].begin(); index != (*it)["spriteIndeces"].end(); index++)
					{
						anim.frames.push_back(index->asInt());
					}
					mAnimations.insert(std::pair<int, Animation>((*it)["id"].asInt(), anim));
				}
				SetAnimation(0);
				ClearSprites();
			}
			else
			{
				std::cout << "ERROR! Could not load spritesheet data: " << path << std::endl;
				LoadError();
			}
		}

		void SpritesheetComponent::SetSprite(sf::Texture& texture, int order)
		{
			if (!mError)
			{
				std::shared_ptr<sf::Sprite> sprite = mSprites.at(order);
				if (sprite == nullptr)
					sprite = std::make_shared<sf::Sprite>();
				sprite->setTexture(texture);
				sprite->setOrigin(sprite->getTextureRect().width / mHorTiles / 2.f, sprite->getTextureRect().height / mVerTiles / 2.f);
				mSprites.at(order) = sprite;
			}
			else
			{
				std::cout << "Could not set sprite to SpritesheetComponent in ERROR state" << std::endl;
			}
		}

		void SpritesheetComponent::ClearSprite(int order)
		{
			if (!mError)
			{
				std::shared_ptr<sf::Sprite> sprite = mSprites.at(order);
				if (sprite != nullptr)
				{
					mSprites.erase(order);
				}
			}
			else
			{
				std::cout << "Could not clear sprite of SpritesheetComponent in ERROR state" << std::endl;
			}
		}

		void SpritesheetComponent::SetAnimation(int state)
		{
			if (!mError)
			{
				if (mAnimations.size() > 0)
				{
					mCurState = state;
					mCurFrame = -1;
					NextFrame();
					mDt = 0;
				}
			}
			else
			{
				std::cout << "Could not set animation to SpritesheetComponent in ERROR state" << std::endl;
			}
		}

		void SpritesheetComponent::Draw(float dt)
		{
			if (mAnimations.size() > 0)
			{
				mDt += dt;
				if (mDt >= (1.f / mAnimations[mCurState].speed))
				{
					NextFrame();
					mDt = 0;
				}
			}

			for (int i = 0; i < mSprites.size(); i++)
			{
				std::shared_ptr<sf::Sprite> sprite = mSprites.at(i);
				if (sprite != nullptr)
				{
					Managers::ViewManager::GetInstance()->GetWindow().draw(*sprite);
				}
			}
		}

		void SpritesheetComponent::SetPos(sf::Vector2f pos)
		{
			mPos = pos;

			for (int i = 0; i < mSprites.size(); i++)
			{
				std::shared_ptr<sf::Sprite> sprite = mSprites.at(i);
				if (sprite != nullptr)
				{
					sprite->setPosition(mPos);
				}
			}
		}

		sf::Vector2f SpritesheetComponent::GetPos()
		{
			return mPos;
		}

		int SpritesheetComponent::GetCurrentAnimation()
		{
			return mCurState;
		}

		sf::Vector2f SpritesheetComponent::GetFrameDimensions()
		{
			return sf::Vector2f((float)mFrameWidth, (float)mFrameHeight);
		}

		sf::Vector2f SpritesheetComponent::GetCollisionDimensions()
		{
			return sf::Vector2f((float)mColWidth, (float)mColHeight);
		}

		// PRIVATE

		void SpritesheetComponent::ClearSprites()
		{
			mSprites.clear();

			for (int i = 0; i < mLayerMax; i++)
			{
				mSprites.insert(std::pair<int, std::shared_ptr<sf::Sprite>>(i, nullptr));
			}
		}

		void SpritesheetComponent::LoadError()
		{
			int spriteId;
			spriteId = Managers::TextureManager::AddTexture("Resources/notfound.png");
			LoadSpritesheet("Resources/notfound.json");
			SetSprite(*Managers::TextureManager::GetTexture(spriteId));
			SetAnimation(0);	// default to 0 for errors
			mError = true;
		}

		void SpritesheetComponent::NextFrame()
		{
			Animation& animation = mAnimations[mCurState];

			if (mCurFrame == animation.frames.size() - 1)
				mCurFrame = 0;
			else
				mCurFrame++;

			for (int i = 0; i < mSprites.size(); i++)
			{
				std::shared_ptr<sf::Sprite> sprite = mSprites.at(i);
				if (sprite != nullptr)
				{
					sf::Vector2i frame = GetFrameFromIndex(animation.frames[mCurFrame]);
					sprite->setTextureRect(sf::IntRect(frame.x * mFrameWidth,
						frame.y * mFrameHeight,
						mFrameWidth, mFrameHeight));
				}
			}
		}

		sf::Vector2i SpritesheetComponent::GetFrameFromIndex(int index)
		{
			sf::Vector2i ret;

			ret.x = index % mHorTiles;
			ret.y = index / mHorTiles;

			return ret;
		}
	}
}