#include "Octomorph/Components/SpriteComponent.h"
#include <Octomorph/Managers/ViewManager.h>

namespace Octomorph
{
	namespace Components
	{
		SpriteComponent::SpriteComponent() {}

		void SpriteComponent::Draw(float dt)
		{
			Managers::ViewManager::GetInstance()->GetWindow().draw(mSprite);
		}

		void SpriteComponent::SetSprite(sf::Texture& texture)
		{
			mSprite.setTexture(texture);
			mSprite.setTextureRect(sf::IntRect(0, 0, mSprite.getTexture()->getSize().x, mSprite.getTexture()->getSize().y));
			mSprite.setOrigin((float)texture.getSize().x / 2, (float)texture.getSize().y / 2);
		}

		sf::Sprite& SpriteComponent::GetSprite()
		{
			return mSprite;
		}
	}
}