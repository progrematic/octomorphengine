#include <Octomorph/Components/StaticTransformComponent.h>

namespace Octomorph
{
	namespace Components
	{
		StaticTransformComponent::StaticTransformComponent() :
			mPos({ 0, 0 })
		{}

		void StaticTransformComponent::SetPos(sf::Vector2f pos)
		{
			mPos = pos;
		}

		sf::Vector2f& StaticTransformComponent::GetPos()
		{
			return mPos;
		}
	}
}