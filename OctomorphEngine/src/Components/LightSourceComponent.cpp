#include "Octomorph/Components/LightSourceComponent.h"

namespace Octomorph
{
	namespace Components
	{
		LightSourceComponent::LightSourceComponent() :
			mPos({ 0, 0 }),
			mSize({ 0, 0 }),
			mColor(0, 0, 0, 0),
			mRadius(0),
			mWaning(0),
			mFlicker(false),
			mFlickerRate(15),
			mDt(0),
			mFlickerDirection(1),
			mFlickerIntensity(10)
		{}

		void LightSourceComponent::Update(float dt)
		{
			Component::Update(dt);

			if (mFlicker)
			{
				mDt += dt;
				if (mDt >= 1.f / mFlickerRate)
				{
					mWaning += mFlickerIntensity * mFlickerDirection;
					mFlickerDirection *= -1;
					mDt = 0;
				}
			}
		}

		sf::Vector2f LightSourceComponent::GetPos()
		{
			return mPos;
		}

		void LightSourceComponent::SetPos(sf::Vector2f pos)
		{
			mPos = pos;
		}

		sf::Vector2f LightSourceComponent::GetSize()
		{
			return mSize;
		}

		void LightSourceComponent::SetSize(sf::Vector2f size)
		{
			mSize = size;
		}

		sf::Color LightSourceComponent::GetColor()
		{
			return mColor;
		}

		void LightSourceComponent::SetColor(sf::Color color)
		{
			mColor = color;
		}

		float LightSourceComponent::GetRadius()
		{
			return mRadius;
		}

		void LightSourceComponent::SetRadius(float radius)
		{
			mRadius = radius;
		}

		float LightSourceComponent::GetWaning()
		{
			return mWaning;
		}

		void LightSourceComponent::SetWaning(float waning)
		{
			mWaning = waning;
		}

		bool LightSourceComponent::GetFlicker()
		{
			return mFlicker;
		}

		void LightSourceComponent::SetFlicker(bool flicker, float flickerRate, float flickerIntensity)
		{
			mFlicker = flicker;
			mFlickerRate = flickerRate;
			mFlickerIntensity = flickerIntensity;
		}
	}
}