#include <Octomorph/Components/InputComponent.h>

namespace Octomorph
{
	namespace Components
	{
		InputComponent::InputComponent()
		{
			// Defaults
			mJoystickId = 1;
			mInputType = INPUT_TYPE::KEYBOARD;
		};

		void InputComponent::SetInputType(INPUT_TYPE type)
		{
			mInputType = type;
		}

		INPUT_TYPE InputComponent::GetInputType()
		{
			return mInputType;
		}

		void InputComponent::SetJoystickId(int joystickId)
		{
			mJoystickId = joystickId;
		}

		int InputComponent::GetJoystickId()
		{
			return mJoystickId;
		}

		void InputComponent::SetKeyboardAnalogInput(int input, bool high, sf::Keyboard::Key key)
		{
			if (mKeyboardAnalogInputs.find(input) == mKeyboardAnalogInputs.end())
				mKeyboardAnalogInputs.insert(std::pair<int, KeyboardAxis>(input, { sf::Keyboard::KeyCount, sf::Keyboard::KeyCount }));	// set to invalid for now.. will get set below
			
			if (high)
				mKeyboardAnalogInputs.at(input).high = key;
			else
				mKeyboardAnalogInputs.at(input).low = key;
		}

		void InputComponent::SetKeyboardDigitalInput(int input, sf::Keyboard::Key key)
		{
			if (mKeyboardDigitalInputs.find(input) == mKeyboardDigitalInputs.end())
				mKeyboardDigitalInputs.insert(std::pair<int, sf::Keyboard::Key>(input, sf::Keyboard::KeyCount));	// set to invalid for now.. will get set below

			mKeyboardDigitalInputs.at(input) = key;
		}

		void InputComponent::SetJoystickAnalogInput(int input, Joystick::Axis key)
		{
			if (mJoystickAnalogInputs.find(input) == mJoystickAnalogInputs.end())
				mJoystickAnalogInputs.insert(std::pair<int, Joystick::Axis>(input, Joystick::Axis::AxisSize));	// set to invalid for now.. will get set below

			mJoystickAnalogInputs.at(input) = key;
		}

		void InputComponent::SetJoystickDigitalInput(int input, Joystick::Button key)
		{
			if (mJoystickDigitalInputs.find(input) == mJoystickDigitalInputs.end())
				mJoystickDigitalInputs.insert(std::pair<int, Joystick::Button>(input, Joystick::Button::ButtonSize));	// set to invalid for now.. will get set below

			mJoystickDigitalInputs.at(input) = key;
		}

		float InputComponent::GetAnalog(int input)
		{
			float ret = 0.f;

			switch (mInputType)
			{
			case INPUT_TYPE::KEYBOARD:
			{
				if (Keyboard::Key(mKeyboardAnalogInputs.at(input).high))
					ret += 1.f;
				if (Keyboard::Key(mKeyboardAnalogInputs.at(input).low))
					ret -= 1.f;
			}
			break;
			case INPUT_TYPE::CONTROLLER:
			{
				ret = Joystick::GetAxis(mJoystickId, mJoystickAnalogInputs.at(input));
			}
			break;
			}

			return ret;
		}

		bool InputComponent::GetDigital(int input)
		{
			bool ret = false;

			switch (mInputType)
			{
			case INPUT_TYPE::KEYBOARD:
			{
				if (Keyboard::Key(mKeyboardDigitalInputs.at(input)))
					ret = true;
			}
			break;
			case INPUT_TYPE::CONTROLLER:
			{
				if (Joystick::GetButton(mJoystickId, mJoystickDigitalInputs.at(input)))
					ret = true;
			}
			break;
			}

			return ret;
		}

		bool InputComponent::GetDigitalDown(int input)
		{
			bool ret = false;

			switch (mInputType)
			{
			case INPUT_TYPE::KEYBOARD:
			{
				if (Keyboard::KeyDown(mKeyboardDigitalInputs.at(input)))
					ret = true;
			}
			break;
			case INPUT_TYPE::CONTROLLER:
			{
				if (Joystick::GetButtonDown(mJoystickId, mJoystickDigitalInputs.at(input)))
					ret = true;
			}
			break;
			}

			return ret;
		}

		bool InputComponent::GetDigitalUp(int input)
		{
			bool ret = false;

			switch (mInputType)
			{
			case INPUT_TYPE::KEYBOARD:
			{
				if (Keyboard::KeyUp(mKeyboardDigitalInputs.at(input)))
					ret = true;
			}
			break;
			case INPUT_TYPE::CONTROLLER:
			{
				if (Joystick::GetButtonUp(mJoystickId, mJoystickDigitalInputs.at(input)))
					ret = true;
			}
			break;
			}

			return ret;
		}
	}
}

// Need to add GetAnalogDown, GetAnalogUp.. maybe..