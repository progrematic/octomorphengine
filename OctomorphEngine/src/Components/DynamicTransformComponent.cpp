#include <Octomorph/Components/DynamicTransformComponent.h>

namespace Octomorph
{
	namespace Components
	{
		DynamicTransformComponent::DynamicTransformComponent() :
			mVel({ 0, 0 })
		{}

		sf::Vector2f DynamicTransformComponent::ApplyVel(bool resetVel)
		{
			mPos += mVel;
			if (resetVel)
				mVel = sf::Vector2f(0, 0);
			return mPos;
		}

		sf::Vector2f DynamicTransformComponent::ApplyVel(sf::Vector2f decay)
		{
			mPos += mVel;
			mVel.x *= decay.x;
			mVel.y *= decay.y;
			return mPos;
		}

		void DynamicTransformComponent::AddForce(sf::Vector2f force)
		{
			mVel += force;
		}

		sf::Vector2f DynamicTransformComponent::GetFuturePos()
		{
			return mPos + mVel;
		}

		void DynamicTransformComponent::SetVel(sf::Vector2f vel)
		{
			mVel = vel;
		}

		sf::Vector2f& DynamicTransformComponent::GetVel()
		{
			return mVel;
		}
	}
}