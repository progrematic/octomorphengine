var class_octomorph_1_1_managers_1_1_light_manager =
[
    [ "~LightManager", "class_octomorph_1_1_managers_1_1_light_manager.html#a09a6295862b3fefe652fd2d7da5ec9d3", null ],
    [ "AddLightSource", "class_octomorph_1_1_managers_1_1_light_manager.html#a0dd78c4716f5cffb770aead5b6dc92bf", null ],
    [ "Cleanup", "class_octomorph_1_1_managers_1_1_light_manager.html#aa7273be753ddaec2e4ad2532e124aa08", null ],
    [ "ClearLightSources", "class_octomorph_1_1_managers_1_1_light_manager.html#a0d2eed34c84212e94d3cd424e80bcad5", null ],
    [ "Draw", "class_octomorph_1_1_managers_1_1_light_manager.html#a6f4ada4f26cb9f47b18c3955431c379d", null ],
    [ "GetInstance", "class_octomorph_1_1_managers_1_1_light_manager.html#a9a9a425d037f375d1dc31c4672f074b3", null ],
    [ "Initialize", "class_octomorph_1_1_managers_1_1_light_manager.html#a07c484b874a583d6ec0df5946b11b5df", null ],
    [ "LightRect", "class_octomorph_1_1_managers_1_1_light_manager.html#af00498d3d8b0734a70dacc76c283f787", null ],
    [ "Reset", "class_octomorph_1_1_managers_1_1_light_manager.html#a8f08cea20b2021127729f64bdddbfcf5", null ],
    [ "Run", "class_octomorph_1_1_managers_1_1_light_manager.html#aeed0a699569a854e68712bd515a1fc19", null ]
];