var class_octomorph_1_1_managers_1_1_view_manager =
[
    [ "GetInstance", "class_octomorph_1_1_managers_1_1_view_manager.html#ab78f45db674ded4726f57e8beeec6143", null ],
    [ "GetView", "class_octomorph_1_1_managers_1_1_view_manager.html#a5b37b0364f6598fcbfd3367e7ac046b8", null ],
    [ "GetWindow", "class_octomorph_1_1_managers_1_1_view_manager.html#aaaec60bd3dd46b198d74ff41e1435cfc", null ],
    [ "Initialize", "class_octomorph_1_1_managers_1_1_view_manager.html#acf0102a388185abde8d6dc3aa525044f", null ],
    [ "SetGameViewpoint", "class_octomorph_1_1_managers_1_1_view_manager.html#a6648103ce471f5094fbedcc0472d0cf7", null ],
    [ "SetGameViewpointSpeed", "class_octomorph_1_1_managers_1_1_view_manager.html#a1714e7c0570ac6aed99aa9f2be175e9a", null ],
    [ "SetGameViewpointTarget", "class_octomorph_1_1_managers_1_1_view_manager.html#affd7d57ea95d44e6a86310e45d519408", null ],
    [ "SetView", "class_octomorph_1_1_managers_1_1_view_manager.html#a05a3e8b16af96c27da11a7f471d20a60", null ],
    [ "Update", "class_octomorph_1_1_managers_1_1_view_manager.html#a8b1ae46dbb816c2dc5174685c28ee955", null ],
    [ "UpdatePlayerCount", "class_octomorph_1_1_managers_1_1_view_manager.html#a37cb51a03ad7005842c4f7b6ec6fac57", null ]
];