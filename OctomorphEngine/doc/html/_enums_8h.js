var _enums_8h =
[
    [ "INPUT_TYPE", "_enums_8h.html#ae689450d25656c848835ee8e098675bf", [
      [ "KEYBOARD", "_enums_8h.html#ae689450d25656c848835ee8e098675bfaedda266cdb6345b9f6914cc2e7577475", null ],
      [ "CONTROLLER", "_enums_8h.html#ae689450d25656c848835ee8e098675bfa42e074e8564dad9034ef033c6a6cf8b6", null ],
      [ "COUNT", "_enums_8h.html#ae689450d25656c848835ee8e098675bfa4905ac9d6a22bdfc1ae096094ce6248d", null ]
    ] ],
    [ "VIEW", "_enums_8h.html#a441fa5a3e810c22a4cf20b913f0bbee2", [
      [ "MAIN", "_enums_8h.html#a441fa5a3e810c22a4cf20b913f0bbee2a186495f7da296bf880df3a22a492b378", null ],
      [ "PLAYER1", "_enums_8h.html#a441fa5a3e810c22a4cf20b913f0bbee2a8e8478fd786f76ac4c83d5d3e1f267f2", null ],
      [ "PLAYER2", "_enums_8h.html#a441fa5a3e810c22a4cf20b913f0bbee2a843a04bf323577f88418d120b31233d1", null ],
      [ "PLAYER3", "_enums_8h.html#a441fa5a3e810c22a4cf20b913f0bbee2af9fa28d54d2acafbde32c4c2c045309c", null ],
      [ "PLAYER4", "_enums_8h.html#a441fa5a3e810c22a4cf20b913f0bbee2a1a645d4daaea7ff32ede80651bf493dd", null ],
      [ "UI", "_enums_8h.html#a441fa5a3e810c22a4cf20b913f0bbee2a71ff71526d15db86eb50fcac245d183b", null ],
      [ "COUNT", "_enums_8h.html#a441fa5a3e810c22a4cf20b913f0bbee2a4905ac9d6a22bdfc1ae096094ce6248d", null ]
    ] ]
];