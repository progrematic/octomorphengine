var class_octomorph_1_1_g_u_i_1_1_widgets_1_1_text_label =
[
    [ "TextLabel", "class_octomorph_1_1_g_u_i_1_1_widgets_1_1_text_label.html#a492d8e475f40a2fcf4e28d057a6ef255", null ],
    [ "Draw", "class_octomorph_1_1_g_u_i_1_1_widgets_1_1_text_label.html#acc8627ba209921b3e4d02d512e65faeb", null ],
    [ "GetFontSize", "class_octomorph_1_1_g_u_i_1_1_widgets_1_1_text_label.html#a94ee39871aae69b6fb4fbf07364ab723", null ],
    [ "SetColor", "class_octomorph_1_1_g_u_i_1_1_widgets_1_1_text_label.html#afbe6f4d4af49595019eca1491eead115", null ],
    [ "SetFont", "class_octomorph_1_1_g_u_i_1_1_widgets_1_1_text_label.html#ac5764d36ed71620799c731c8faaac0c2", null ],
    [ "SetFontSize", "class_octomorph_1_1_g_u_i_1_1_widgets_1_1_text_label.html#ad657f20ada4d240df7d6dd9aba6980ba", null ],
    [ "SetString", "class_octomorph_1_1_g_u_i_1_1_widgets_1_1_text_label.html#a41750f5ae85345130da55a2884352fd9", null ],
    [ "Update", "class_octomorph_1_1_g_u_i_1_1_widgets_1_1_text_label.html#aa7c14f29aecb7a838cdec64ae0a67551", null ]
];