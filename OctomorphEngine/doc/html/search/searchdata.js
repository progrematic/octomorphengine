var indexSectionsWithContent =
{
  0: "abcdeghijklmnoprstuvwxy~",
  1: "bcdegijklmnpstvw",
  2: "o",
  3: "bcdeghijklmnpstvw",
  4: "abcdeghijklmnprstuw~",
  5: "lmpr",
  6: "abiv",
  7: "abcdhklmnprstuxy",
  8: "eimp"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "enums",
  7: "enumvalues",
  8: "defines"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Enumerations",
  7: "Enumerator",
  8: "Macros"
};

