var class_octomorph_1_1_managers_1_1_message_manager =
[
    [ "AddHandler", "class_octomorph_1_1_managers_1_1_message_manager.html#a0d592cd6d050fc7e5879d2ba137875d7", null ],
    [ "GetInstance", "class_octomorph_1_1_managers_1_1_message_manager.html#ae59f998ca5763d2882b44ac6ebae0fb4", null ],
    [ "RemoveHandler", "class_octomorph_1_1_managers_1_1_message_manager.html#a7f224897f73d1d0cd3f108f92e43cb42", null ],
    [ "SendMessage", "class_octomorph_1_1_managers_1_1_message_manager.html#a4bfe201b87339afc2c533e43b00c0906", null ]
];