var struct_octomorph_1_1_managers_1_1_light_manager_1_1_light_source_info =
[
    [ "color", "struct_octomorph_1_1_managers_1_1_light_manager_1_1_light_source_info.html#a26edc557b81de356bedb384abe165cdf", null ],
    [ "lightNum", "struct_octomorph_1_1_managers_1_1_light_manager_1_1_light_source_info.html#ae3f9ab8b161bb44f333bad94cf29b531", null ],
    [ "pos", "struct_octomorph_1_1_managers_1_1_light_manager_1_1_light_source_info.html#a0c3294d66b7854c7d70633f0aec4a423", null ],
    [ "radius", "struct_octomorph_1_1_managers_1_1_light_manager_1_1_light_source_info.html#a541784ce4a534787d2cbe5f227205b27", null ],
    [ "size", "struct_octomorph_1_1_managers_1_1_light_manager_1_1_light_source_info.html#addebdb17c3184fbaee76585cabc7d82a", null ],
    [ "waning", "struct_octomorph_1_1_managers_1_1_light_manager_1_1_light_source_info.html#a0c5ed0b51b3ce8bf7934a27ea173616e", null ]
];