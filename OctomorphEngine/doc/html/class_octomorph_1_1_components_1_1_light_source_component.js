var class_octomorph_1_1_components_1_1_light_source_component =
[
    [ "LightSourceComponent", "class_octomorph_1_1_components_1_1_light_source_component.html#a880e7358fced93d71784d976667f0315", null ],
    [ "GetColor", "class_octomorph_1_1_components_1_1_light_source_component.html#af6f5b9551240d83f27fac70ded3a1031", null ],
    [ "GetFlicker", "class_octomorph_1_1_components_1_1_light_source_component.html#a6cabf94e62e7c4f4916afd95509bd1cd", null ],
    [ "GetPos", "class_octomorph_1_1_components_1_1_light_source_component.html#a806b297e921438bf578787c269cd7f4e", null ],
    [ "GetRadius", "class_octomorph_1_1_components_1_1_light_source_component.html#a975a8bd50ca0b1a5cfb03261863e7231", null ],
    [ "GetSize", "class_octomorph_1_1_components_1_1_light_source_component.html#a93fced1fffec838a58fd7c21e21a39af", null ],
    [ "GetWaning", "class_octomorph_1_1_components_1_1_light_source_component.html#aaf0ef3c054ec63a9ce15aec93550b042", null ],
    [ "SetColor", "class_octomorph_1_1_components_1_1_light_source_component.html#a056cc1d81c29b2092141e11908999f4b", null ],
    [ "SetFlicker", "class_octomorph_1_1_components_1_1_light_source_component.html#a0b3c8aedee252b48115c1acb10348487", null ],
    [ "SetPos", "class_octomorph_1_1_components_1_1_light_source_component.html#a022b3d584b15eceae30961c997248ad1", null ],
    [ "SetRadius", "class_octomorph_1_1_components_1_1_light_source_component.html#a49e0666464da7300de083b31fc8d0074", null ],
    [ "SetSize", "class_octomorph_1_1_components_1_1_light_source_component.html#ab71b9b11cf39c6bde74527281d87d308", null ],
    [ "SetWaning", "class_octomorph_1_1_components_1_1_light_source_component.html#afa94e0361bcc06f77bb9cf3abc336284", null ],
    [ "Update", "class_octomorph_1_1_components_1_1_light_source_component.html#a69568f58efdfdb5b2b618bf3e2648a0c", null ]
];